﻿namespace DongleManagment {
    partial class ContractReport {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContractReport));
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.txtNumber = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.txtSecondPerson = new Telerik.Reporting.TextBox();
            this.txtPrice = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.txtDate = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.txtSecondPerson2 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.txtAddress = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.txtPhoneNumber = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(2.0866141319274902D);
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(8.21504020690918D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.txtNumber,
            this.textBox6,
            this.textBox7,
            this.textBox9,
            this.txtSecondPerson,
            this.txtPrice,
            this.textBox14,
            this.txtDate,
            this.textBox15,
            this.textBox26,
            this.textBox28,
            this.textBox27,
            this.textBox30,
            this.textBox29,
            this.textBox32,
            this.textBox31,
            this.textBox34,
            this.textBox33,
            this.textBox36,
            this.textBox35,
            this.textBox38,
            this.textBox37,
            this.textBox41,
            this.textBox40,
            this.textBox17,
            this.textBox16,
            this.txtSecondPerson2,
            this.textBox19,
            this.txtAddress,
            this.textBox20,
            this.txtPhoneNumber,
            this.textBox22,
            this.textBox24,
            this.textBox25,
            this.textBox8,
            this.textBox5});
            this.detail.Name = "detail";
            // 
            // textBox1
            // 
            this.textBox1.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3D), Telerik.Reporting.Drawing.Unit.Inch(0.10000022500753403D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.29583358764648438D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Arial";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Value = "( بسمه تعالى )";
            // 
            // textBox2
            // 
            this.textBox2.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9685039520263672D), Telerik.Reporting.Drawing.Unit.Inch(2.297637939453125D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39992094039916992D), Telerik.Reporting.Drawing.Unit.Inch(0.29992133378982544D));
            this.textBox2.Style.Font.Name = "Arial";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.Value = "1 - ";
            // 
            // textBox3
            // 
            this.textBox3.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.8740158081054688D), Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.411023736000061D), Telerik.Reporting.Drawing.Unit.Inch(0.30000019073486328D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Arial";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.Value = "عقد تجهيز برنامج";
            // 
            // textBox4
            // 
            this.textBox4.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.285118579864502D), Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49992150068283081D), Telerik.Reporting.Drawing.Unit.Inch(0.30000019073486328D));
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Arial";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Value = "م / ";
            // 
            // txtNumber
            // 
            this.txtNumber.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.txtNumber.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.2124226838350296D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.19583381712436676D));
            this.txtNumber.Style.Font.Name = "Arial";
            this.txtNumber.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtNumber.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtNumber.Value = "888888";
            // 
            // textBox6
            // 
            this.textBox6.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.4999995231628418D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0000003576278687D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox6.Style.Font.Name = "Arial";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox6.Value = "الطرف الأول : ";
            // 
            // textBox7
            // 
            this.textBox7.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.6999990940093994D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7999212741851807D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox7.Style.Font.Name = "Arial";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox7.Value = "شركة المربع للحلول البرمجية";
            // 
            // textBox9
            // 
            this.textBox9.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.4999995231628418D), Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0000003576278687D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox9.Style.Font.Name = "Arial";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox9.Value = "الطرف الثاني : ";
            // 
            // txtSecondPerson
            // 
            this.txtSecondPerson.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.txtSecondPerson.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.6999988555908203D), Telerik.Reporting.Drawing.Unit.Inch(1.3000001907348633D));
            this.txtSecondPerson.Name = "txtSecondPerson";
            this.txtSecondPerson.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7999212741851807D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.txtSecondPerson.Style.Font.Name = "Arial";
            this.txtSecondPerson.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtSecondPerson.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtSecondPerson.Value = "الجابري مول";
            // 
            // txtPrice
            // 
            this.txtPrice.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.txtPrice.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.21242265403270721D), Telerik.Reporting.Drawing.Unit.Inch(1.7000001668930054D));
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.2875776290893555D), Telerik.Reporting.Drawing.Unit.Inch(0.47952747344970703D));
            this.txtPrice.Style.Font.Bold = false;
            this.txtPrice.Style.Font.Name = "Arial";
            this.txtPrice.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtPrice.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtPrice.Value = "500 $";
            // 
            // textBox14
            // 
            this.textBox14.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.4021621942520142D), Telerik.Reporting.Drawing.Unit.Inch(0.50000029802322388D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79992121458053589D), Telerik.Reporting.Drawing.Unit.Inch(0.19583381712436676D));
            this.textBox14.Style.Font.Name = "Arial";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox14.Value = "التاريخ : ";
            // 
            // txtDate
            // 
            this.txtDate.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.txtDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D), Telerik.Reporting.Drawing.Unit.Inch(0.50000029802322388D));
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.19583381712436676D));
            this.txtDate.Style.Font.Name = "Arial";
            this.txtDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtDate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtDate.Value = "2020/01/01";
            // 
            // textBox15
            // 
            this.textBox15.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19685037434101105D), Telerik.Reporting.Drawing.Unit.Inch(2.297637939453125D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.7715744972229D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox15.Style.Font.Name = "Arial";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.Value = "يقوم الطرف الأول بتوفير التدريب والدعم الفني على البرنامج مجاناً للطرف الثاني .";
            // 
            // textBox26
            // 
            this.textBox26.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.4125016927719116D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79992121458053589D), Telerik.Reporting.Drawing.Unit.Inch(0.19583381712436676D));
            this.textBox26.Style.Font.Name = "Arial";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox26.Value = "الرقم : ";
            // 
            // textBox28
            // 
            this.textBox28.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9685039520263672D), Telerik.Reporting.Drawing.Unit.Inch(2.5976383686065674D));
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39992094039916992D), Telerik.Reporting.Drawing.Unit.Inch(0.299999862909317D));
            this.textBox28.Style.Font.Name = "Arial";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox28.Value = "2 - ";
            // 
            // textBox27
            // 
            this.textBox27.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19685046374797821D), Telerik.Reporting.Drawing.Unit.Inch(2.5976383686065674D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.7715744972229D), Telerik.Reporting.Drawing.Unit.Inch(0.29999971389770508D));
            this.textBox27.Style.Font.Name = "Arial";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox27.Value = "يقوم الطرف الأول بمساعدة الطرف الثاني على ترتيب العمل داخل البرنامج وتوفير خدمات " +
    "ما بعد البيع فيما يتعلق بالبرنامج .";
            // 
            // textBox30
            // 
            this.textBox30.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9685039520263672D), Telerik.Reporting.Drawing.Unit.Inch(2.9000785350799561D));
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39992094039916992D), Telerik.Reporting.Drawing.Unit.Inch(0.38950410485267639D));
            this.textBox30.Style.Font.Name = "Arial";
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox30.Value = "3 - ";
            // 
            // textBox29
            // 
            this.textBox29.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19685037434101105D), Telerik.Reporting.Drawing.Unit.Inch(2.9000787734985352D));
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.7715744972229D), Telerik.Reporting.Drawing.Unit.Inch(0.38950395584106445D));
            this.textBox29.Style.Font.Name = "Arial";
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox29.Value = "يكون التحديث مجانياً ضمن الإصدار نفسه المرخص بدنكل ويتم عن طريق تواصل الطرف الثان" +
    "ي مع الطرف الأول لغرض التحديث.";
            // 
            // textBox32
            // 
            this.textBox32.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9685039520263672D), Telerik.Reporting.Drawing.Unit.Inch(3.2896606922149658D));
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39992094039916992D), Telerik.Reporting.Drawing.Unit.Inch(0.29992133378982544D));
            this.textBox32.Style.Font.Name = "Arial";
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox32.Value = "4 -";
            // 
            // textBox31
            // 
            this.textBox31.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19685046374797821D), Telerik.Reporting.Drawing.Unit.Inch(3.2896614074707031D));
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.7715744972229D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox31.Style.Font.Name = "Arial";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox31.Value = "على الطرف الثاني توفير حاسبة خاصة بالعمل وصالحة للاستخدام لضمان سلاسة العمل . ";
            // 
            // textBox34
            // 
            this.textBox34.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9685039520263672D), Telerik.Reporting.Drawing.Unit.Inch(3.5896613597869873D));
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39992094039916992D), Telerik.Reporting.Drawing.Unit.Inch(0.61033821105957031D));
            this.textBox34.Style.Font.Name = "Arial";
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox34.Value = "5 -";
            // 
            // textBox33
            // 
            this.textBox33.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19685037434101105D), Telerik.Reporting.Drawing.Unit.Inch(3.5896613597869873D));
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.7715744972229D), Telerik.Reporting.Drawing.Unit.Inch(0.610338032245636D));
            this.textBox33.Style.Font.Name = "Arial";
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox33.Value = resources.GetString("textBox33.Value");
            // 
            // textBox36
            // 
            this.textBox36.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9685039520263672D), Telerik.Reporting.Drawing.Unit.Inch(4.2000775337219238D));
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39992094039916992D), Telerik.Reporting.Drawing.Unit.Inch(0.38950410485267639D));
            this.textBox36.Style.Font.Name = "Arial";
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox36.Value = "6 -";
            // 
            // textBox35
            // 
            this.textBox35.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19685046374797821D), Telerik.Reporting.Drawing.Unit.Inch(4.20007848739624D));
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.7715744972229D), Telerik.Reporting.Drawing.Unit.Inch(0.38950395584106445D));
            this.textBox35.Style.Font.Name = "Arial";
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox35.Value = "في حال كسر أو عطل (دنكل الحماية) يتم استبدالها بتكلفة 15 دولار وذلك بشرط جلب (الد" +
    "نكل) العاطل أو المكسور إلى الشركة .";
            // 
            // textBox38
            // 
            this.textBox38.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9685039520263672D), Telerik.Reporting.Drawing.Unit.Inch(4.58966064453125D));
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39992094039916992D), Telerik.Reporting.Drawing.Unit.Inch(0.29992133378982544D));
            this.textBox38.Style.Font.Name = "Arial";
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox38.Value = "7 -";
            // 
            // textBox37
            // 
            this.textBox37.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19685037434101105D), Telerik.Reporting.Drawing.Unit.Inch(4.5896611213684082D));
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.7715744972229D), Telerik.Reporting.Drawing.Unit.Inch(0.29992121458053589D));
            this.textBox37.Style.Font.Name = "Arial";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox37.Value = "على الطرف الثاني توفير كادر مؤهل للعمل على البرنامج .";
            // 
            // textBox41
            // 
            this.textBox41.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9685039520263672D), Telerik.Reporting.Drawing.Unit.Inch(4.8896603584289551D));
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39992094039916992D), Telerik.Reporting.Drawing.Unit.Inch(0.38950410485267639D));
            this.textBox41.Style.Font.Name = "Arial";
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox41.Value = "8 -";
            // 
            // textBox40
            // 
            this.textBox40.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19685037434101105D), Telerik.Reporting.Drawing.Unit.Inch(4.8896613121032715D));
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.7715744972229D), Telerik.Reporting.Drawing.Unit.Inch(0.38950395584106445D));
            this.textBox40.Style.Font.Name = "Arial";
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox40.Value = "لا يحق للطرف الثاني إعادة بيع البرنامج  أو نقل ملكيته أو إعارته بأي شكل من الأشكا" +
    "ل إلا بإذن خطي من قبل الطرف الأول .";
            // 
            // textBox17
            // 
            this.textBox17.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.59055119752883911D), Telerik.Reporting.Drawing.Unit.Inch(6.205298900604248D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.19583381712436676D));
            this.textBox17.Style.Font.Name = "Arial";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox17.Value = "الطرف الأول";
            // 
            // textBox16
            // 
            this.textBox16.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.3937010765075684D), Telerik.Reporting.Drawing.Unit.Inch(6.205298900604248D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.19583381712436676D));
            this.textBox16.Style.Font.Name = "Arial";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox16.Value = "الطرف الثاني";
            // 
            // txtSecondPerson2
            // 
            this.txtSecondPerson2.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.txtSecondPerson2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.7348225116729736D), Telerik.Reporting.Drawing.Unit.Inch(6.5052990913391113D));
            this.txtSecondPerson2.Name = "txtSecondPerson2";
            this.txtSecondPerson2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.3211314678192139D), Telerik.Reporting.Drawing.Unit.Inch(0.19583381712436676D));
            this.txtSecondPerson2.Style.Font.Bold = true;
            this.txtSecondPerson2.Style.Font.Name = "Arial";
            this.txtSecondPerson2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtSecondPerson2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtSecondPerson2.Value = "الجابري مول";
            // 
            // textBox19
            // 
            this.textBox19.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.0601997375488281D), Telerik.Reporting.Drawing.Unit.Inch(6.5005745887756348D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.443918377161026D), Telerik.Reporting.Drawing.Unit.Inch(0.19583381712436676D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Arial";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox19.Value = "الإسم : ";
            // 
            // txtAddress
            // 
            this.txtAddress.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.txtAddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.7348225116729736D), Telerik.Reporting.Drawing.Unit.Inch(6.8052997589111328D));
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.16365122795105D), Telerik.Reporting.Drawing.Unit.Inch(0.19583381712436676D));
            this.txtAddress.Style.Font.Bold = true;
            this.txtAddress.Style.Font.Name = "Arial";
            this.txtAddress.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtAddress.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtAddress.Value = "النجف - شارع المطار - قرب متوسطة الفارس العربي";
            // 
            // textBox20
            // 
            this.textBox20.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.898552417755127D), Telerik.Reporting.Drawing.Unit.Inch(6.8005752563476562D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.60348206758499146D), Telerik.Reporting.Drawing.Unit.Inch(0.19583381712436676D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.Font.Name = "Arial";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox20.Value = "العنوان : ";
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.txtPhoneNumber.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.7348225116729736D), Telerik.Reporting.Drawing.Unit.Inch(7.1053004264831543D));
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0061712265014648D), Telerik.Reporting.Drawing.Unit.Inch(0.19583381712436676D));
            this.txtPhoneNumber.Style.Font.Bold = true;
            this.txtPhoneNumber.Style.Font.Name = "Arial";
            this.txtPhoneNumber.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtPhoneNumber.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtPhoneNumber.Value = "النجف - شارع المطار - قرب متوسطة الفارس العربي";
            // 
            // textBox22
            // 
            this.textBox22.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7410721778869629D), Telerik.Reporting.Drawing.Unit.Inch(7.1005759239196777D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7630457878112793D), Telerik.Reporting.Drawing.Unit.Inch(0.19583381712436676D));
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.Font.Name = "Arial";
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox22.Value = "رقم الهاتف : ";
            // 
            // textBox24
            // 
            this.textBox24.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.3937010765075684D), Telerik.Reporting.Drawing.Unit.Inch(7.6053004264831543D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.19583381712436676D));
            this.textBox24.Style.Font.Name = "Arial";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox24.Value = "التوقيع :";
            // 
            // textBox25
            // 
            this.textBox25.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.59055119752883911D), Telerik.Reporting.Drawing.Unit.Inch(7.6053004264831543D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.19583381712436676D));
            this.textBox25.Style.Font.Name = "Arial";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox25.Value = "التوقيع : ";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.052083373069763184D);
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // textBox5
            // 
            this.textBox5.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.19685041904449463D), Telerik.Reporting.Drawing.Unit.Inch(5.2792434692382812D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.7715744972229D), Telerik.Reporting.Drawing.Unit.Inch(0.38950395584106445D));
            this.textBox5.Style.Font.Name = "Arial";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox5.Value = "على الطرف الثاني توفير جهاز خزن خارجي او وسيلة خزن لغرض النسخ الاحتياطي اليومي لب" +
    "يانات البرنامج حيث ان الطرف الاول لا يتحمل مسؤولية ضياع البيانات";
            // 
            // textBox8
            // 
            this.textBox8.Culture = new System.Globalization.CultureInfo("ar-IQ");
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9685039520263672D), Telerik.Reporting.Drawing.Unit.Inch(5.2792434692382812D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39992094039916992D), Telerik.Reporting.Drawing.Unit.Inch(0.38950410485267639D));
            this.textBox8.Style.Font.Name = "Arial";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Value = "9 -";
            // 
            // ContractReport
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "ContractReport";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(0.20000000298023224D), Telerik.Reporting.Drawing.Unit.Cm(0.20000000298023224D), Telerik.Reporting.Drawing.Unit.Cm(0.20000000298023224D), Telerik.Reporting.Drawing.Unit.Cm(0.20000000298023224D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8.1102361679077148D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox40;
        public Telerik.Reporting.TextBox txtNumber;
        public Telerik.Reporting.TextBox txtSecondPerson;
        public Telerik.Reporting.TextBox txtPrice;
        public Telerik.Reporting.TextBox txtDate;
        public Telerik.Reporting.TextBox txtSecondPerson2;
        public Telerik.Reporting.TextBox txtAddress;
        public Telerik.Reporting.TextBox txtPhoneNumber;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox5;
    }
}