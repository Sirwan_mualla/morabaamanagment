﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DongleManagment.Models;

namespace DongleManagment.Forms
{
    public partial class FrmDongleMovment : Form
    {
        public string DongleKey { get; set; }

        public FrmDongleMovment()
        {
            InitializeComponent();
        }

        private async Task GetData()
        {
            var data = new List<ActiveDongleViewModel>();
            await Task.Run(() =>
           {
               try
               {

                   var client = new HttpClient { BaseAddress = Pv.BaseAddress };

                   client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                   client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                   var response = client.GetAsync("api/Dongle/GetActiveDongleMovment?dongleKey=" + DongleKey).Result;

                   if (response.IsSuccessStatusCode)
                   {
                       var result = response.Content.ReadAsAsync<List<ActiveDongle>>().Result;
                       if (result == null)
                       {
                           MessageBox.Show("لا توجد بيانات !");
                           return;
                       }


                       foreach (var activeDongle in result)
                       {
                           data.Add(new ActiveDongleViewModel
                           {
                               VersionId = activeDongle.VersionId,
                               Id = activeDongle.Id,
                               CompanyAddress = activeDongle.CompanyAddress,
                               IsMain = activeDongle.IsMain,
                               ApplicationId = activeDongle.ApplicationId,
                               CompanyGovernorate = activeDongle.CompanyGovernorate,
                               CompanyJop = activeDongle.CompanyJop,
                               CompanyName = activeDongle.CompanyName,
                               CompanyNote = activeDongle.CompanyNote,
                               CompanyPhones = activeDongle.CompanyPhones,
                               CostPrice = activeDongle.CostPrice,
                               Date = activeDongle.Date,
                               MachineDate = activeDongle.MachineDate,
                               Deleted = activeDongle.Deleted,
                               Discount = activeDongle.Discount,
                               DongleKey = activeDongle.DongleKey,
                               Explained = Pv.DongleActivitionString(new DongleHoldData
                               {
                                   IsMain = activeDongle.IsMain,
                                   VersionId = activeDongle.VersionId,
                                   ApplicationId = activeDongle.ApplicationId,
                                   Features = activeDongle.FeatureList.Select(c => c.FeatureId).ToList(),
                               }, activeDongle.StrUpgraded),
                               FeatureList = activeDongle.FeatureList,
                               OfferId = activeDongle.OfferId,
                               SalePrice = activeDongle.SalePrice,
                               UserId = activeDongle.UserId,
                               UserName = activeDongle.UserName,
                               IsDeleted = activeDongle.IsDeleted
                           }) ;
                       }


                   }
                   else
                   {
                       MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                   }
               }
               catch (Exception ex)
               {
                   MessageBox.Show(ex.Message);
               }
           });
            dg3.AutoGenerateColumns = false;
            dg3.DataSource = data;
        }

        private async void FrmDongleMovment_Load(object sender, EventArgs e)
        {
            await GetData();
        }
    }
}
