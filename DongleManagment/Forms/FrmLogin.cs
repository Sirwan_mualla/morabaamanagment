﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DongleManagment.Forms;
using DongleManagment.Models;
using Newtonsoft.Json.Linq;
using Application = System.Windows.Forms.Application;
using Version = DongleManagment.Models.Version;


namespace DongleManagment
{
    public partial class FrmLogin : Form
    {
        private bool flagRequest = false;
        BackgroundWorker _backgroundWorker = new BackgroundWorker();
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void GetData()
        {
            try
            {

                var client = new HttpClient { BaseAddress = Pv.BaseAddress };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);
                var response = client.GetAsync("api/Dongle/Versions").Result;

                if (response.IsSuccessStatusCode)
                {
                    Pv.Versions = response.Content.ReadAsAsync<List<Version>>().Result;

                }

                client = new HttpClient { BaseAddress = Pv.BaseAddress };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);
                response = client.GetAsync("api/Dongle/Features").Result;

                if (response.IsSuccessStatusCode)
                {
                    Pv.Featureses = response.Content.ReadAsAsync<List<Features>>().Result;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private async Task login() {
            try {
                if (flagRequest) return;
                flagRequest = true;
                this.Cursor = Cursors.WaitCursor;

                var myToken = new Token();
                var strToken = await myToken.GetToken(txtUserName.Text.Trim(), txtUserPassword.Text.Trim());

                if (strToken == null) return;

                using (var httpClient = new HttpClient()) {
                    httpClient.BaseAddress = Pv.BaseAddress;
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", strToken);
                    httpClient.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("Application/JSON"));

                    using (var response = await httpClient.GetAsync("Api/Account/Login")) {

                        var result = await response.Content.ReadAsAsync<UserViewModel>();
                        Pv.User = result;
                        Pv.User.Token = strToken;
                        if (Pv.User.Id == null)
                            return;
                        this.Hide();
                        GetData();
                        if (result.Type == UserViewModel.UserType.Admin || result.Type == UserViewModel.UserType.Accountant) {
                            var f = new FrmMain();
                            f.ShowDialog();
                            this.Close();
                        } else if (result.Type == UserViewModel.UserType.Agent) {
                            var f = new FrmMain(true);
                            f.ShowDialog();
                            this.Close();

                            //var f = new FrmMainAgent();
                            //f.ShowDialog();
                            //this.Close();
                        }

                    }
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            } finally {
                flagRequest = false;
                this.Cursor = Cursors.Default;
            }
        }
        private async void btnLogin_Click(object sender, EventArgs e)
        {
            await login();

        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            //morabaa2007@gmail.com
            //Morabaa123@a
            lblversion.Text = $"V {Pv.CurrentVersionNo}.0"; 
            _backgroundWorker.DoWork += BackgroundWorkerOnDoWork;
            CheckVersionApp();

         
      }

        private void BackgroundWorkerOnDoWork(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            Thread.Sleep(5000);
            Application.Exit();

        }

        private async void CheckVersionApp()
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = Pv.BaseAddress;
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("Application/JSON"));
                try {
                    using (var response = await httpClient.GetAsync("api/Dongle/GetCurrentVersion")) {

                        var result = await response.Content.ReadAsAsync<CurrentVersion>();
                        if (result is null) return;
                        if (result.No != Pv.CurrentVersionNo) {
                            _backgroundWorker.RunWorkerAsync();
                            MessageBox.Show("النسخة قديمة يرجى الاتصال بشركة المربع!");
                        }


                    }
                } catch (Exception ex) {
                    MessageBox.Show("فشلت محاولة الاتصال بالسيرفر");
                }
                

            }

        }

        private async void txtUserPassword_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter)
                await login();
        }
    }
}
