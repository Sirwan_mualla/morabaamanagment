﻿namespace DongleManagment.Forms
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.btnAddAdmin = new System.Windows.Forms.Button();
            this.btnAddAgent = new System.Windows.Forms.Button();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnAccountant = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtConfirme = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEMail = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnChangePassword = new System.Windows.Forms.Button();
            this.txtNewConfirme = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNewPassword = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtOldPassword = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnNewVersion = new System.Windows.Forms.Button();
            this.btnDeleteVersion = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtMainVersion = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSubVersion = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAgentMainVersion = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtAgentSubVersion = new System.Windows.Forms.TextBox();
            this.btnSaveVersion = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.txtVersionName = new System.Windows.Forms.TextBox();
            this.btnVersionRefrish = new System.Windows.Forms.Button();
            this.dgVersions = new System.Windows.Forms.DataGridView();
            this.clmVersionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.btnRestPassword = new System.Windows.Forms.Button();
            this.txtNewConfirme2 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtNewPassword2 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtEmail2 = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnNewFeatures = new System.Windows.Forms.Button();
            this.btnDeleteFeatures = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.txtMainFeatures = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSubFeatures = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtAgentMainFeatures = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtAgentSubFeatures = new System.Windows.Forms.TextBox();
            this.btnSaveFeatures = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.txtFeaturesName = new System.Windows.Forms.TextBox();
            this.btnFeaturesRefrish = new System.Windows.Forms.Button();
            this.dgFeathures = new System.Windows.Forms.DataGridView();
            this.clmFeaturesName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.txtMasterPassword = new System.Windows.Forms.TextBox();
            this.cmbcurrency = new System.Windows.Forms.ComboBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.chkMaster = new System.Windows.Forms.CheckBox();
            this.chkAutoDeActivate = new System.Windows.Forms.CheckBox();
            this.dtpDeActivationDate = new System.Windows.Forms.DateTimePicker();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.txtOldVersionDetails = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.lblOldVersionKey = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.lblOldVersionState = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.btnRefrish = new System.Windows.Forms.Button();
            this.checkUpgrad = new System.Windows.Forms.CheckBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.btnReActiveDongle = new System.Windows.Forms.Button();
            this.lblMatch = new System.Windows.Forms.Label();
            this.lblDongleActivationInfo = new System.Windows.Forms.Label();
            this.txtActivitionInfo = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.txtCompanyGovernorate = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtCompanyNote = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtCompanyJop = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtCompanyPhones = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtCompanyAddress = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.btnSaveActiveDongle = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.rdoServer = new System.Windows.Forms.RadioButton();
            this.rdoSub = new System.Windows.Forms.RadioButton();
            this.DgActiveDongleFeatures = new System.Windows.Forms.DataGridView();
            this.clmId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmFeatureName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmChoose = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cmbVersion = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.btnReadData = new System.Windows.Forms.Button();
            this.btnAddDongle = new System.Windows.Forms.Button();
            this.btnCheckDongle = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.pnlUser = new System.Windows.Forms.Panel();
            this.btnFetchDataAccounts = new System.Windows.Forms.Button();
            this.cmbUser = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.btnExport = new System.Windows.Forms.Button();
            this.dg1 = new System.Windows.Forms.DataGridView();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.pnlUser2 = new System.Windows.Forms.Panel();
            this.btnFetchDataAccounts2 = new System.Windows.Forms.Button();
            this.cmbUser2 = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.btnExport2 = new System.Windows.Forms.Button();
            this.dg2 = new System.Windows.Forms.DataGridView();
            this.clmDg2Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg2MachineDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg2CostPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg2CompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg2Explained = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmCompanyDetails3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.btnRefresh2 = new System.Windows.Forms.Button();
            this.dtTo2 = new System.Windows.Forms.DateTimePicker();
            this.dtFrom2 = new System.Windows.Forms.DateTimePicker();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.pnlUser3 = new System.Windows.Forms.Panel();
            this.btnFetchDataAccounts3 = new System.Windows.Forms.Button();
            this.cmbUser3 = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.btnExport3 = new System.Windows.Forms.Button();
            this.dg3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDeleted = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmCompanyDetails2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDongleMovment = new System.Windows.Forms.DataGridViewImageColumn();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.btnRefresh3 = new System.Windows.Forms.Button();
            this.dtTo3 = new System.Windows.Forms.DateTimePicker();
            this.dtFrom3 = new System.Windows.Forms.DateTimePicker();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.label45 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnExport4 = new System.Windows.Forms.Button();
            this.dg4 = new System.Windows.Forms.DataGridView();
            this.clmUserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmCompanyDetails = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDongleMovment2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.clmKey2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.btnRefresh4 = new System.Windows.Forms.Button();
            this.dtTo4 = new System.Windows.Forms.DateTimePicker();
            this.dtFrom4 = new System.Windows.Forms.DateTimePicker();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.clmDg1Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg1MachineDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg1CostPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg1CompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg1Explained = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmCompanyDetails4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDongleMovments = new System.Windows.Forms.DataGridViewImageColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVersions)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFeathures)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgActiveDongleFeatures)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.pnlUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg1)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.pnlUser2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg2)).BeginInit();
            this.tabPage7.SuspendLayout();
            this.pnlUser3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg3)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg4)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddAdmin
            // 
            this.btnAddAdmin.Location = new System.Drawing.Point(120, 183);
            this.btnAddAdmin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddAdmin.Name = "btnAddAdmin";
            this.btnAddAdmin.Size = new System.Drawing.Size(107, 28);
            this.btnAddAdmin.TabIndex = 1;
            this.btnAddAdmin.Text = "اضافة ادمن";
            this.btnAddAdmin.UseVisualStyleBackColor = true;
            this.btnAddAdmin.Click += new System.EventHandler(this.btnAddAdmin_Click);
            // 
            // btnAddAgent
            // 
            this.btnAddAgent.Location = new System.Drawing.Point(233, 183);
            this.btnAddAgent.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddAgent.Name = "btnAddAgent";
            this.btnAddAgent.Size = new System.Drawing.Size(107, 28);
            this.btnAddAgent.TabIndex = 2;
            this.btnAddAgent.Text = "اضافة وكيل";
            this.btnAddAgent.UseVisualStyleBackColor = true;
            this.btnAddAgent.Click += new System.EventHandler(this.btnAddAgent_Click);
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(7, 23);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(287, 24);
            this.txtUserName.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnAccountant);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btnAddAdmin);
            this.groupBox1.Controls.Add(this.btnAddAgent);
            this.groupBox1.Controls.Add(this.txtPhone);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtConfirme);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtEMail);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtUserName);
            this.groupBox1.Location = new System.Drawing.Point(751, 7);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(406, 224);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "بيانات المستخدم";
            // 
            // btnAccountant
            // 
            this.btnAccountant.Location = new System.Drawing.Point(7, 183);
            this.btnAccountant.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAccountant.Name = "btnAccountant";
            this.btnAccountant.Size = new System.Drawing.Size(107, 28);
            this.btnAccountant.TabIndex = 13;
            this.btnAccountant.Text = "اضافة محاسب";
            this.btnAccountant.UseVisualStyleBackColor = true;
            this.btnAccountant.Click += new System.EventHandler(this.btnAccountant_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(302, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "رقم الهاتف";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(7, 151);
            this.txtPhone.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(287, 24);
            this.txtPhone.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(302, 123);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "تاكيد كلمة المرور";
            // 
            // txtConfirme
            // 
            this.txtConfirme.Location = new System.Drawing.Point(7, 119);
            this.txtConfirme.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtConfirme.Name = "txtConfirme";
            this.txtConfirme.PasswordChar = '*';
            this.txtConfirme.Size = new System.Drawing.Size(287, 24);
            this.txtConfirme.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(302, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "كلمة المرور";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(7, 87);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(287, 24);
            this.txtPassword.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(302, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "البريد الالكتروني";
            // 
            // txtEMail
            // 
            this.txtEMail.Location = new System.Drawing.Point(7, 55);
            this.txtEMail.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtEMail.Name = "txtEMail";
            this.txtEMail.Size = new System.Drawing.Size(287, 24);
            this.txtEMail.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(302, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "أسم المستخدم";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.btnChangePassword);
            this.groupBox2.Controls.Add(this.txtNewConfirme);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtNewPassword);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtOldPassword);
            this.groupBox2.Location = new System.Drawing.Point(751, 239);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(406, 158);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "تغيير كلمة المرور";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(262, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "تأكيد كلمة المرور الجديدة";
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.Location = new System.Drawing.Point(168, 119);
            this.btnChangePassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.Size = new System.Drawing.Size(87, 28);
            this.btnChangePassword.TabIndex = 2;
            this.btnChangePassword.Text = "حفظ";
            this.btnChangePassword.UseVisualStyleBackColor = true;
            this.btnChangePassword.Click += new System.EventHandler(this.btnChangePassword_Click);
            // 
            // txtNewConfirme
            // 
            this.txtNewConfirme.Location = new System.Drawing.Point(7, 87);
            this.txtNewConfirme.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNewConfirme.Name = "txtNewConfirme";
            this.txtNewConfirme.PasswordChar = '*';
            this.txtNewConfirme.Size = new System.Drawing.Size(248, 24);
            this.txtNewConfirme.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(289, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 17);
            this.label7.TabIndex = 10;
            this.label7.Text = "كلمة المرور الجديدة";
            // 
            // txtNewPassword
            // 
            this.txtNewPassword.Location = new System.Drawing.Point(7, 55);
            this.txtNewPassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNewPassword.Name = "txtNewPassword";
            this.txtNewPassword.PasswordChar = '*';
            this.txtNewPassword.Size = new System.Drawing.Size(248, 24);
            this.txtNewPassword.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(289, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 17);
            this.label8.TabIndex = 8;
            this.label8.Text = "كلمة المرور القديمة";
            // 
            // txtOldPassword
            // 
            this.txtOldPassword.Location = new System.Drawing.Point(7, 23);
            this.txtOldPassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtOldPassword.Name = "txtOldPassword";
            this.txtOldPassword.PasswordChar = '*';
            this.txtOldPassword.Size = new System.Drawing.Size(248, 24);
            this.txtOldPassword.TabIndex = 7;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.btnVersionRefrish);
            this.groupBox3.Controls.Add(this.dgVersions);
            this.groupBox3.Location = new System.Drawing.Point(279, 4);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox3.Size = new System.Drawing.Size(880, 513);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "انواع النسخ";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnNewVersion);
            this.groupBox4.Controls.Add(this.btnDeleteVersion);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.txtMainVersion);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.txtSubVersion);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.txtAgentMainVersion);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.txtAgentSubVersion);
            this.groupBox4.Controls.Add(this.btnSaveVersion);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.txtVersionName);
            this.groupBox4.Location = new System.Drawing.Point(14, 23);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox4.Size = new System.Drawing.Size(392, 346);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "بيانات النسخ";
            // 
            // btnNewVersion
            // 
            this.btnNewVersion.Location = new System.Drawing.Point(246, 183);
            this.btnNewVersion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNewVersion.Name = "btnNewVersion";
            this.btnNewVersion.Size = new System.Drawing.Size(87, 28);
            this.btnNewVersion.TabIndex = 18;
            this.btnNewVersion.Text = "جديد";
            this.btnNewVersion.UseVisualStyleBackColor = true;
            this.btnNewVersion.Click += new System.EventHandler(this.btnNewVersion_Click);
            // 
            // btnDeleteVersion
            // 
            this.btnDeleteVersion.Location = new System.Drawing.Point(57, 183);
            this.btnDeleteVersion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDeleteVersion.Name = "btnDeleteVersion";
            this.btnDeleteVersion.Size = new System.Drawing.Size(87, 28);
            this.btnDeleteVersion.TabIndex = 17;
            this.btnDeleteVersion.Text = "حذف";
            this.btnDeleteVersion.UseVisualStyleBackColor = true;
            this.btnDeleteVersion.Click += new System.EventHandler(this.btnDeleteVersion_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(279, 59);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 17);
            this.label13.TabIndex = 16;
            this.label13.Text = "سعر الرئيسية";
            // 
            // txtMainVersion
            // 
            this.txtMainVersion.Location = new System.Drawing.Point(23, 55);
            this.txtMainVersion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMainVersion.Name = "txtMainVersion";
            this.txtMainVersion.Size = new System.Drawing.Size(248, 24);
            this.txtMainVersion.TabIndex = 15;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(279, 123);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(121, 17);
            this.label12.TabIndex = 14;
            this.label12.Text = "سعر الرئيسية وكيل";
            // 
            // txtSubVersion
            // 
            this.txtSubVersion.Location = new System.Drawing.Point(23, 87);
            this.txtSubVersion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSubVersion.Name = "txtSubVersion";
            this.txtSubVersion.Size = new System.Drawing.Size(248, 24);
            this.txtSubVersion.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(279, 155);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(112, 17);
            this.label10.TabIndex = 12;
            this.label10.Text = "سعر الفرعية وكيل";
            // 
            // txtAgentMainVersion
            // 
            this.txtAgentMainVersion.Location = new System.Drawing.Point(23, 119);
            this.txtAgentMainVersion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAgentMainVersion.Name = "txtAgentMainVersion";
            this.txtAgentMainVersion.Size = new System.Drawing.Size(248, 24);
            this.txtAgentMainVersion.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(279, 91);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 17);
            this.label9.TabIndex = 10;
            this.label9.Text = "سعر الشبكية";
            // 
            // txtAgentSubVersion
            // 
            this.txtAgentSubVersion.Location = new System.Drawing.Point(23, 151);
            this.txtAgentSubVersion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAgentSubVersion.Name = "txtAgentSubVersion";
            this.txtAgentSubVersion.Size = new System.Drawing.Size(248, 24);
            this.txtAgentSubVersion.TabIndex = 9;
            // 
            // btnSaveVersion
            // 
            this.btnSaveVersion.Location = new System.Drawing.Point(152, 183);
            this.btnSaveVersion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSaveVersion.Name = "btnSaveVersion";
            this.btnSaveVersion.Size = new System.Drawing.Size(87, 28);
            this.btnSaveVersion.TabIndex = 2;
            this.btnSaveVersion.Text = "حفظ";
            this.btnSaveVersion.UseVisualStyleBackColor = true;
            this.btnSaveVersion.Click += new System.EventHandler(this.btnSaveVersion_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(279, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(84, 17);
            this.label11.TabIndex = 8;
            this.label11.Text = "أسم النسخة";
            // 
            // txtVersionName
            // 
            this.txtVersionName.Location = new System.Drawing.Point(23, 23);
            this.txtVersionName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVersionName.Name = "txtVersionName";
            this.txtVersionName.Size = new System.Drawing.Size(248, 24);
            this.txtVersionName.TabIndex = 7;
            // 
            // btnVersionRefrish
            // 
            this.btnVersionRefrish.Location = new System.Drawing.Point(717, 377);
            this.btnVersionRefrish.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnVersionRefrish.Name = "btnVersionRefrish";
            this.btnVersionRefrish.Size = new System.Drawing.Size(87, 28);
            this.btnVersionRefrish.TabIndex = 2;
            this.btnVersionRefrish.Text = "تحديث";
            this.btnVersionRefrish.UseVisualStyleBackColor = true;
            this.btnVersionRefrish.Click += new System.EventHandler(this.btnVersionRefrish_Click);
            // 
            // dgVersions
            // 
            this.dgVersions.AllowUserToAddRows = false;
            this.dgVersions.AllowUserToDeleteRows = false;
            this.dgVersions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVersions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmVersionName});
            this.dgVersions.Location = new System.Drawing.Point(413, 23);
            this.dgVersions.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgVersions.Name = "dgVersions";
            this.dgVersions.ReadOnly = true;
            this.dgVersions.RowHeadersWidth = 51;
            this.dgVersions.Size = new System.Drawing.Size(392, 346);
            this.dgVersions.TabIndex = 0;
            this.dgVersions.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgVersions_CellClick);
            // 
            // clmVersionName
            // 
            this.clmVersionName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmVersionName.DataPropertyName = "Name";
            this.clmVersionName.HeaderText = "الاسم";
            this.clmVersionName.MinimumWidth = 6;
            this.clmVersionName.Name = "clmVersionName";
            this.clmVersionName.ReadOnly = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1176, 783);
            this.tabControl1.TabIndex = 7;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage2.Controls.Add(this.tableLayoutPanel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage2.Size = new System.Drawing.Size(1168, 754);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "النسخ";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1132F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 4);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 618F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1162, 746);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage1.Controls.Add(this.tableLayoutPanel2);
            this.tabPage1.Controls.Add(this.groupBox12);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage1.Size = new System.Drawing.Size(1168, 754);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "المستخدم";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 486F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(149, 20);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 178F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(334, 241);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label35);
            this.groupBox12.Controls.Add(this.btnRestPassword);
            this.groupBox12.Controls.Add(this.txtNewConfirme2);
            this.groupBox12.Controls.Add(this.label36);
            this.groupBox12.Controls.Add(this.txtNewPassword2);
            this.groupBox12.Controls.Add(this.label37);
            this.groupBox12.Controls.Add(this.txtEmail2);
            this.groupBox12.Location = new System.Drawing.Point(751, 404);
            this.groupBox12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox12.Size = new System.Drawing.Size(406, 158);
            this.groupBox12.TabIndex = 6;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "أعادة تعيين كلمة المرور";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(262, 91);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(154, 17);
            this.label35.TabIndex = 12;
            this.label35.Text = "تأكيد كلمة المرور الجديدة";
            // 
            // btnRestPassword
            // 
            this.btnRestPassword.Location = new System.Drawing.Point(168, 119);
            this.btnRestPassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRestPassword.Name = "btnRestPassword";
            this.btnRestPassword.Size = new System.Drawing.Size(87, 28);
            this.btnRestPassword.TabIndex = 2;
            this.btnRestPassword.Text = "حفظ";
            this.btnRestPassword.UseVisualStyleBackColor = true;
            this.btnRestPassword.Click += new System.EventHandler(this.btnRestPassword_Click);
            // 
            // txtNewConfirme2
            // 
            this.txtNewConfirme2.Location = new System.Drawing.Point(7, 87);
            this.txtNewConfirme2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNewConfirme2.Name = "txtNewConfirme2";
            this.txtNewConfirme2.PasswordChar = '*';
            this.txtNewConfirme2.Size = new System.Drawing.Size(248, 24);
            this.txtNewConfirme2.TabIndex = 11;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(289, 59);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(122, 17);
            this.label36.TabIndex = 10;
            this.label36.Text = "كلمة المرور الجديدة";
            // 
            // txtNewPassword2
            // 
            this.txtNewPassword2.Location = new System.Drawing.Point(7, 55);
            this.txtNewPassword2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNewPassword2.Name = "txtNewPassword2";
            this.txtNewPassword2.PasswordChar = '*';
            this.txtNewPassword2.Size = new System.Drawing.Size(248, 24);
            this.txtNewPassword2.TabIndex = 9;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(289, 27);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(100, 17);
            this.label37.TabIndex = 8;
            this.label37.Text = "البريد الالكتروني";
            // 
            // txtEmail2
            // 
            this.txtEmail2.Location = new System.Drawing.Point(7, 23);
            this.txtEmail2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtEmail2.Name = "txtEmail2";
            this.txtEmail2.Size = new System.Drawing.Size(248, 24);
            this.txtEmail2.TabIndex = 7;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage3.Controls.Add(this.tableLayoutPanel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage3.Size = new System.Drawing.Size(1168, 754);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "الاضافات";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 964F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.groupBox5, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 4);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 599F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1162, 746);
            this.tableLayoutPanel3.TabIndex = 8;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Controls.Add(this.btnFeaturesRefrish);
            this.groupBox5.Controls.Add(this.dgFeathures);
            this.groupBox5.Location = new System.Drawing.Point(320, 4);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox5.Size = new System.Drawing.Size(839, 422);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "انواع الاضافات";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnNewFeatures);
            this.groupBox6.Controls.Add(this.btnDeleteFeatures);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.txtMainFeatures);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.txtSubFeatures);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.txtAgentMainFeatures);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Controls.Add(this.txtAgentSubFeatures);
            this.groupBox6.Controls.Add(this.btnSaveFeatures);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.txtFeaturesName);
            this.groupBox6.Location = new System.Drawing.Point(7, 23);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox6.Size = new System.Drawing.Size(392, 346);
            this.groupBox6.TabIndex = 6;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "بيانات الاضافات";
            // 
            // btnNewFeatures
            // 
            this.btnNewFeatures.Location = new System.Drawing.Point(246, 183);
            this.btnNewFeatures.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNewFeatures.Name = "btnNewFeatures";
            this.btnNewFeatures.Size = new System.Drawing.Size(87, 28);
            this.btnNewFeatures.TabIndex = 18;
            this.btnNewFeatures.Text = "جديد";
            this.btnNewFeatures.UseVisualStyleBackColor = true;
            this.btnNewFeatures.Click += new System.EventHandler(this.btnNewFeatures_Click);
            // 
            // btnDeleteFeatures
            // 
            this.btnDeleteFeatures.Location = new System.Drawing.Point(57, 183);
            this.btnDeleteFeatures.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDeleteFeatures.Name = "btnDeleteFeatures";
            this.btnDeleteFeatures.Size = new System.Drawing.Size(87, 28);
            this.btnDeleteFeatures.TabIndex = 17;
            this.btnDeleteFeatures.Text = "حذف";
            this.btnDeleteFeatures.UseVisualStyleBackColor = true;
            this.btnDeleteFeatures.Click += new System.EventHandler(this.btnDeleteFeatures_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(279, 59);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(89, 17);
            this.label14.TabIndex = 16;
            this.label14.Text = "سعر الرئيسية";
            // 
            // txtMainFeatures
            // 
            this.txtMainFeatures.Location = new System.Drawing.Point(23, 55);
            this.txtMainFeatures.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMainFeatures.Name = "txtMainFeatures";
            this.txtMainFeatures.Size = new System.Drawing.Size(248, 24);
            this.txtMainFeatures.TabIndex = 15;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(279, 123);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(121, 17);
            this.label15.TabIndex = 14;
            this.label15.Text = "سعر الرئيسية وكيل";
            // 
            // txtSubFeatures
            // 
            this.txtSubFeatures.Location = new System.Drawing.Point(23, 87);
            this.txtSubFeatures.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSubFeatures.Name = "txtSubFeatures";
            this.txtSubFeatures.Size = new System.Drawing.Size(248, 24);
            this.txtSubFeatures.TabIndex = 13;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(279, 155);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(112, 17);
            this.label16.TabIndex = 12;
            this.label16.Text = "سعر الفرعية وكيل";
            // 
            // txtAgentMainFeatures
            // 
            this.txtAgentMainFeatures.Location = new System.Drawing.Point(23, 119);
            this.txtAgentMainFeatures.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAgentMainFeatures.Name = "txtAgentMainFeatures";
            this.txtAgentMainFeatures.Size = new System.Drawing.Size(248, 24);
            this.txtAgentMainFeatures.TabIndex = 11;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(279, 91);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(86, 17);
            this.label17.TabIndex = 10;
            this.label17.Text = "سعر الشبكية";
            // 
            // txtAgentSubFeatures
            // 
            this.txtAgentSubFeatures.Location = new System.Drawing.Point(23, 151);
            this.txtAgentSubFeatures.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtAgentSubFeatures.Name = "txtAgentSubFeatures";
            this.txtAgentSubFeatures.Size = new System.Drawing.Size(248, 24);
            this.txtAgentSubFeatures.TabIndex = 9;
            // 
            // btnSaveFeatures
            // 
            this.btnSaveFeatures.Location = new System.Drawing.Point(152, 183);
            this.btnSaveFeatures.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSaveFeatures.Name = "btnSaveFeatures";
            this.btnSaveFeatures.Size = new System.Drawing.Size(87, 28);
            this.btnSaveFeatures.TabIndex = 2;
            this.btnSaveFeatures.Text = "حفظ";
            this.btnSaveFeatures.UseVisualStyleBackColor = true;
            this.btnSaveFeatures.Click += new System.EventHandler(this.btnSaveFeatures_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(279, 27);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 17);
            this.label18.TabIndex = 8;
            this.label18.Text = "اسم الاضافة";
            // 
            // txtFeaturesName
            // 
            this.txtFeaturesName.Location = new System.Drawing.Point(23, 23);
            this.txtFeaturesName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtFeaturesName.Name = "txtFeaturesName";
            this.txtFeaturesName.Size = new System.Drawing.Size(248, 24);
            this.txtFeaturesName.TabIndex = 7;
            // 
            // btnFeaturesRefrish
            // 
            this.btnFeaturesRefrish.Location = new System.Drawing.Point(710, 377);
            this.btnFeaturesRefrish.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFeaturesRefrish.Name = "btnFeaturesRefrish";
            this.btnFeaturesRefrish.Size = new System.Drawing.Size(87, 28);
            this.btnFeaturesRefrish.TabIndex = 2;
            this.btnFeaturesRefrish.Text = "تحديث";
            this.btnFeaturesRefrish.UseVisualStyleBackColor = true;
            this.btnFeaturesRefrish.Click += new System.EventHandler(this.btnFeaturesRefrish_Click);
            // 
            // dgFeathures
            // 
            this.dgFeathures.AllowUserToAddRows = false;
            this.dgFeathures.AllowUserToDeleteRows = false;
            this.dgFeathures.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFeathures.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmFeaturesName});
            this.dgFeathures.Location = new System.Drawing.Point(406, 23);
            this.dgFeathures.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgFeathures.Name = "dgFeathures";
            this.dgFeathures.ReadOnly = true;
            this.dgFeathures.RowHeadersWidth = 51;
            this.dgFeathures.Size = new System.Drawing.Size(392, 346);
            this.dgFeathures.TabIndex = 0;
            this.dgFeathures.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFeathures_CellClick);
            // 
            // clmFeaturesName
            // 
            this.clmFeaturesName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmFeaturesName.DataPropertyName = "Name";
            this.clmFeaturesName.HeaderText = "الاسم";
            this.clmFeaturesName.MinimumWidth = 6;
            this.clmFeaturesName.Name = "clmFeaturesName";
            this.clmFeaturesName.ReadOnly = true;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage4.Controls.Add(this.groupBox7);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage4.Size = new System.Drawing.Size(1168, 754);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "الدنكل";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.button1);
            this.groupBox7.Controls.Add(this.groupBox8);
            this.groupBox7.Controls.Add(this.btnReadData);
            this.groupBox7.Controls.Add(this.btnAddDongle);
            this.groupBox7.Controls.Add(this.btnCheckDongle);
            this.groupBox7.Location = new System.Drawing.Point(7, 21);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox7.Size = new System.Drawing.Size(1249, 720);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "بيانات الدنكل";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1072, 204);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 69);
            this.button1.TabIndex = 7;
            this.button1.Text = "قراءة البيانات / سيرفر";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.txtMasterPassword);
            this.groupBox8.Controls.Add(this.cmbcurrency);
            this.groupBox8.Controls.Add(this.txtPrice);
            this.groupBox8.Controls.Add(this.chkMaster);
            this.groupBox8.Controls.Add(this.chkAutoDeActivate);
            this.groupBox8.Controls.Add(this.dtpDeActivationDate);
            this.groupBox8.Controls.Add(this.groupBox13);
            this.groupBox8.Controls.Add(this.groupBox11);
            this.groupBox8.Controls.Add(this.groupBox10);
            this.groupBox8.Controls.Add(this.lblAmount);
            this.groupBox8.Controls.Add(this.label21);
            this.groupBox8.Controls.Add(this.btnSaveActiveDongle);
            this.groupBox8.Controls.Add(this.groupBox9);
            this.groupBox8.Controls.Add(this.DgActiveDongleFeatures);
            this.groupBox8.Controls.Add(this.cmbVersion);
            this.groupBox8.Controls.Add(this.label19);
            this.groupBox8.Location = new System.Drawing.Point(7, 23);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox8.Size = new System.Drawing.Size(1058, 682);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "تنشيط الدنكل";
            // 
            // txtMasterPassword
            // 
            this.txtMasterPassword.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtMasterPassword.Location = new System.Drawing.Point(331, 224);
            this.txtMasterPassword.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMasterPassword.Name = "txtMasterPassword";
            this.txtMasterPassword.Size = new System.Drawing.Size(216, 28);
            this.txtMasterPassword.TabIndex = 24;
            this.txtMasterPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMasterPassword.UseSystemPasswordChar = true;
            this.txtMasterPassword.Visible = false;
            this.txtMasterPassword.TextChanged += new System.EventHandler(this.txtMasterPassword_TextChanged);
            // 
            // cmbcurrency
            // 
            this.cmbcurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbcurrency.FormattingEnabled = true;
            this.cmbcurrency.Items.AddRange(new object[] {
            "د.ع",
            "$"});
            this.cmbcurrency.Location = new System.Drawing.Point(574, 228);
            this.cmbcurrency.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbcurrency.Name = "cmbcurrency";
            this.cmbcurrency.Size = new System.Drawing.Size(84, 24);
            this.cmbcurrency.TabIndex = 23;
            // 
            // txtPrice
            // 
            this.txtPrice.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPrice.Location = new System.Drawing.Point(666, 225);
            this.txtPrice.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(154, 28);
            this.txtPrice.TabIndex = 22;
            this.txtPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            this.txtPrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPrice_KeyDown);
            this.txtPrice.Leave += new System.EventHandler(this.txtPrice_Leave);
            // 
            // chkMaster
            // 
            this.chkMaster.AutoSize = true;
            this.chkMaster.Font = new System.Drawing.Font("Tahoma", 12F);
            this.chkMaster.Location = new System.Drawing.Point(393, 228);
            this.chkMaster.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkMaster.Name = "chkMaster";
            this.chkMaster.Size = new System.Drawing.Size(161, 28);
            this.chkMaster.TabIndex = 20;
            this.chkMaster.Text = "Master Dongle";
            this.chkMaster.UseVisualStyleBackColor = true;
            this.chkMaster.Visible = false;
            // 
            // chkAutoDeActivate
            // 
            this.chkAutoDeActivate.AutoSize = true;
            this.chkAutoDeActivate.Location = new System.Drawing.Point(666, 646);
            this.chkAutoDeActivate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkAutoDeActivate.Name = "chkAutoDeActivate";
            this.chkAutoDeActivate.Size = new System.Drawing.Size(182, 21);
            this.chkAutoDeActivate.TabIndex = 19;
            this.chkAutoDeActivate.Text = "الغاء تفعيل تلقائي في يوم ";
            this.chkAutoDeActivate.UseVisualStyleBackColor = true;
            this.chkAutoDeActivate.CheckedChanged += new System.EventHandler(this.chkAutoDeActivate_CheckedChanged);
            // 
            // dtpDeActivationDate
            // 
            this.dtpDeActivationDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDeActivationDate.Location = new System.Drawing.Point(558, 642);
            this.dtpDeActivationDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtpDeActivationDate.Name = "dtpDeActivationDate";
            this.dtpDeActivationDate.Size = new System.Drawing.Size(101, 24);
            this.dtpDeActivationDate.TabIndex = 18;
            this.dtpDeActivationDate.Visible = false;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.txtOldVersionDetails);
            this.groupBox13.Controls.Add(this.label48);
            this.groupBox13.Controls.Add(this.lblOldVersionKey);
            this.groupBox13.Controls.Add(this.label46);
            this.groupBox13.Controls.Add(this.lblOldVersionState);
            this.groupBox13.Controls.Add(this.label43);
            this.groupBox13.Controls.Add(this.btnRefrish);
            this.groupBox13.Controls.Add(this.checkUpgrad);
            this.groupBox13.Location = new System.Drawing.Point(551, 89);
            this.groupBox13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox13.Size = new System.Drawing.Size(495, 129);
            this.groupBox13.TabIndex = 17;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "بيانات الترقية";
            // 
            // txtOldVersionDetails
            // 
            this.txtOldVersionDetails.Enabled = false;
            this.txtOldVersionDetails.Location = new System.Drawing.Point(7, 64);
            this.txtOldVersionDetails.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtOldVersionDetails.Multiline = true;
            this.txtOldVersionDetails.Name = "txtOldVersionDetails";
            this.txtOldVersionDetails.Size = new System.Drawing.Size(157, 56);
            this.txtOldVersionDetails.TabIndex = 23;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(171, 64);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(89, 17);
            this.label48.TabIndex = 22;
            this.label48.Text = "بيانات النسخة";
            this.label48.DoubleClick += new System.EventHandler(this.label48_DoubleClick);
            // 
            // lblOldVersionKey
            // 
            this.lblOldVersionKey.Location = new System.Drawing.Point(37, 31);
            this.lblOldVersionKey.Name = "lblOldVersionKey";
            this.lblOldVersionKey.Size = new System.Drawing.Size(129, 16);
            this.lblOldVersionKey.TabIndex = 21;
            this.lblOldVersionKey.Text = "-----------------------";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(171, 31);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(73, 17);
            this.label46.TabIndex = 20;
            this.label46.Text = "كود التفعيل";
            // 
            // lblOldVersionState
            // 
            this.lblOldVersionState.Location = new System.Drawing.Point(278, 64);
            this.lblOldVersionState.Name = "lblOldVersionState";
            this.lblOldVersionState.Size = new System.Drawing.Size(129, 16);
            this.lblOldVersionState.TabIndex = 19;
            this.lblOldVersionState.Text = "-----------------------";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(413, 64);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(82, 17);
            this.label43.TabIndex = 18;
            this.label43.Text = "حالة النسخة";
            // 
            // btnRefrish
            // 
            this.btnRefrish.Location = new System.Drawing.Point(401, 92);
            this.btnRefrish.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRefrish.Name = "btnRefrish";
            this.btnRefrish.Size = new System.Drawing.Size(87, 28);
            this.btnRefrish.TabIndex = 17;
            this.btnRefrish.Text = "تحديث";
            this.btnRefrish.UseVisualStyleBackColor = true;
            this.btnRefrish.Click += new System.EventHandler(this.btnRefrish_Click);
            // 
            // checkUpgrad
            // 
            this.checkUpgrad.AutoSize = true;
            this.checkUpgrad.Location = new System.Drawing.Point(427, 31);
            this.checkUpgrad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.checkUpgrad.Name = "checkUpgrad";
            this.checkUpgrad.Size = new System.Drawing.Size(59, 21);
            this.checkUpgrad.TabIndex = 0;
            this.checkUpgrad.Text = "ترقية";
            this.checkUpgrad.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.btnReActiveDongle);
            this.groupBox11.Controls.Add(this.lblMatch);
            this.groupBox11.Controls.Add(this.lblDongleActivationInfo);
            this.groupBox11.Controls.Add(this.txtActivitionInfo);
            this.groupBox11.Controls.Add(this.label44);
            this.groupBox11.Controls.Add(this.label28);
            this.groupBox11.Location = new System.Drawing.Point(7, 23);
            this.groupBox11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox11.Size = new System.Drawing.Size(537, 194);
            this.groupBox11.TabIndex = 15;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "بيانات الدنكل";
            // 
            // btnReActiveDongle
            // 
            this.btnReActiveDongle.BackColor = System.Drawing.Color.White;
            this.btnReActiveDongle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReActiveDongle.Location = new System.Drawing.Point(7, 151);
            this.btnReActiveDongle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnReActiveDongle.Name = "btnReActiveDongle";
            this.btnReActiveDongle.Size = new System.Drawing.Size(117, 28);
            this.btnReActiveDongle.TabIndex = 3;
            this.btnReActiveDongle.Text = "مطابقة البيانات";
            this.btnReActiveDongle.UseVisualStyleBackColor = false;
            this.btnReActiveDongle.Visible = false;
            this.btnReActiveDongle.Click += new System.EventHandler(this.btnReActiveDongle_Click);
            // 
            // lblMatch
            // 
            this.lblMatch.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatch.Location = new System.Drawing.Point(171, 151);
            this.lblMatch.Name = "lblMatch";
            this.lblMatch.Size = new System.Drawing.Size(358, 28);
            this.lblMatch.TabIndex = 16;
            this.lblMatch.Text = " ";
            this.lblMatch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDongleActivationInfo
            // 
            this.lblDongleActivationInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(172)))), ((int)(((byte)(243)))));
            this.lblDongleActivationInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDongleActivationInfo.Location = new System.Drawing.Point(7, 87);
            this.lblDongleActivationInfo.Name = "lblDongleActivationInfo";
            this.lblDongleActivationInfo.Size = new System.Drawing.Size(429, 60);
            this.lblDongleActivationInfo.TabIndex = 15;
            this.lblDongleActivationInfo.Text = " ";
            // 
            // txtActivitionInfo
            // 
            this.txtActivitionInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(172)))), ((int)(((byte)(243)))));
            this.txtActivitionInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActivitionInfo.Location = new System.Drawing.Point(7, 20);
            this.txtActivitionInfo.Name = "txtActivitionInfo";
            this.txtActivitionInfo.Size = new System.Drawing.Size(429, 60);
            this.txtActivitionInfo.TabIndex = 14;
            this.txtActivitionInfo.Text = " ";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(443, 106);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(80, 17);
            this.label44.TabIndex = 12;
            this.label44.Text = "بيانات الدنكل";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(443, 33);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(89, 17);
            this.label28.TabIndex = 10;
            this.label28.Text = "بيانات السيرفر";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.txtCompanyGovernorate);
            this.groupBox10.Controls.Add(this.label27);
            this.groupBox10.Controls.Add(this.txtCompanyNote);
            this.groupBox10.Controls.Add(this.label26);
            this.groupBox10.Controls.Add(this.txtCompanyJop);
            this.groupBox10.Controls.Add(this.label25);
            this.groupBox10.Controls.Add(this.txtCompanyPhones);
            this.groupBox10.Controls.Add(this.label24);
            this.groupBox10.Controls.Add(this.txtCompanyAddress);
            this.groupBox10.Controls.Add(this.label23);
            this.groupBox10.Controls.Add(this.txtCompanyName);
            this.groupBox10.Controls.Add(this.label22);
            this.groupBox10.Location = new System.Drawing.Point(7, 256);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox10.Size = new System.Drawing.Size(537, 383);
            this.groupBox10.TabIndex = 14;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "بيانات الشركة";
            // 
            // txtCompanyGovernorate
            // 
            this.txtCompanyGovernorate.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCompanyGovernorate.Location = new System.Drawing.Point(7, 119);
            this.txtCompanyGovernorate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCompanyGovernorate.Name = "txtCompanyGovernorate";
            this.txtCompanyGovernorate.Size = new System.Drawing.Size(401, 28);
            this.txtCompanyGovernorate.TabIndex = 13;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label27.Location = new System.Drawing.Point(415, 124);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(75, 21);
            this.label27.TabIndex = 12;
            this.label27.Text = "المحافظة";
            // 
            // txtCompanyNote
            // 
            this.txtCompanyNote.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCompanyNote.Location = new System.Drawing.Point(7, 255);
            this.txtCompanyNote.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCompanyNote.Multiline = true;
            this.txtCompanyNote.Name = "txtCompanyNote";
            this.txtCompanyNote.Size = new System.Drawing.Size(401, 120);
            this.txtCompanyNote.TabIndex = 9;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label26.Location = new System.Drawing.Point(415, 265);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(83, 21);
            this.label26.TabIndex = 8;
            this.label26.Text = "الملاحظات";
            // 
            // txtCompanyJop
            // 
            this.txtCompanyJop.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCompanyJop.Location = new System.Drawing.Point(7, 183);
            this.txtCompanyJop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCompanyJop.Multiline = true;
            this.txtCompanyJop.Name = "txtCompanyJop";
            this.txtCompanyJop.Size = new System.Drawing.Size(401, 68);
            this.txtCompanyJop.TabIndex = 7;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label25.Location = new System.Drawing.Point(415, 188);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(99, 21);
            this.label25.TabIndex = 6;
            this.label25.Text = "عمل الشركة";
            // 
            // txtCompanyPhones
            // 
            this.txtCompanyPhones.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCompanyPhones.Location = new System.Drawing.Point(7, 151);
            this.txtCompanyPhones.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCompanyPhones.Name = "txtCompanyPhones";
            this.txtCompanyPhones.Size = new System.Drawing.Size(401, 28);
            this.txtCompanyPhones.TabIndex = 5;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label24.Location = new System.Drawing.Point(415, 155);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 21);
            this.label24.TabIndex = 4;
            this.label24.Text = "رقم الهاتف";
            // 
            // txtCompanyAddress
            // 
            this.txtCompanyAddress.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCompanyAddress.Location = new System.Drawing.Point(7, 54);
            this.txtCompanyAddress.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCompanyAddress.Multiline = true;
            this.txtCompanyAddress.Name = "txtCompanyAddress";
            this.txtCompanyAddress.Size = new System.Drawing.Size(401, 62);
            this.txtCompanyAddress.TabIndex = 3;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label23.Location = new System.Drawing.Point(415, 58);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 21);
            this.label23.TabIndex = 2;
            this.label23.Text = "العنوان";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCompanyName.Location = new System.Drawing.Point(7, 22);
            this.txtCompanyName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(401, 28);
            this.txtCompanyName.TabIndex = 1;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label22.Location = new System.Drawing.Point(415, 26);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(100, 21);
            this.label22.TabIndex = 0;
            this.label22.Text = "أسم الشركة";
            // 
            // lblAmount
            // 
            this.lblAmount.Location = new System.Drawing.Point(884, 681);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(98, 22);
            this.lblAmount.TabIndex = 13;
            this.lblAmount.Text = "0";
            this.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(989, 686);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(57, 17);
            this.label21.TabIndex = 12;
            this.label21.Text = "المجموع";
            // 
            // btnSaveActiveDongle
            // 
            this.btnSaveActiveDongle.Location = new System.Drawing.Point(952, 642);
            this.btnSaveActiveDongle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSaveActiveDongle.Name = "btnSaveActiveDongle";
            this.btnSaveActiveDongle.Size = new System.Drawing.Size(87, 28);
            this.btnSaveActiveDongle.TabIndex = 11;
            this.btnSaveActiveDongle.Text = "حفظ";
            this.btnSaveActiveDongle.UseVisualStyleBackColor = true;
            this.btnSaveActiveDongle.Click += new System.EventHandler(this.btnSaveActiveDongle_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.rdoServer);
            this.groupBox9.Controls.Add(this.rdoSub);
            this.groupBox9.Location = new System.Drawing.Point(878, 23);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox9.Size = new System.Drawing.Size(167, 58);
            this.groupBox9.TabIndex = 8;
            this.groupBox9.TabStop = false;
            // 
            // rdoServer
            // 
            this.rdoServer.AccessibleName = "";
            this.rdoServer.AutoSize = true;
            this.rdoServer.Checked = true;
            this.rdoServer.Location = new System.Drawing.Point(85, 23);
            this.rdoServer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdoServer.Name = "rdoServer";
            this.rdoServer.Size = new System.Drawing.Size(66, 21);
            this.rdoServer.TabIndex = 4;
            this.rdoServer.TabStop = true;
            this.rdoServer.Text = "سيرفر";
            this.rdoServer.UseVisualStyleBackColor = true;
            this.rdoServer.CheckedChanged += new System.EventHandler(this.rdoServer_CheckedChanged);
            // 
            // rdoSub
            // 
            this.rdoSub.AccessibleName = "";
            this.rdoSub.AutoSize = true;
            this.rdoSub.Location = new System.Drawing.Point(10, 23);
            this.rdoSub.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rdoSub.Name = "rdoSub";
            this.rdoSub.Size = new System.Drawing.Size(68, 21);
            this.rdoSub.TabIndex = 5;
            this.rdoSub.Text = "شبكية";
            this.rdoSub.UseVisualStyleBackColor = true;
            // 
            // DgActiveDongleFeatures
            // 
            this.DgActiveDongleFeatures.AllowUserToAddRows = false;
            this.DgActiveDongleFeatures.AllowUserToDeleteRows = false;
            this.DgActiveDongleFeatures.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgActiveDongleFeatures.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmId,
            this.clmFeatureName,
            this.clmPrice,
            this.clmChoose});
            this.DgActiveDongleFeatures.Location = new System.Drawing.Point(558, 263);
            this.DgActiveDongleFeatures.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DgActiveDongleFeatures.Name = "DgActiveDongleFeatures";
            this.DgActiveDongleFeatures.ReadOnly = true;
            this.DgActiveDongleFeatures.RowHeadersWidth = 51;
            this.DgActiveDongleFeatures.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgActiveDongleFeatures.Size = new System.Drawing.Size(488, 375);
            this.DgActiveDongleFeatures.TabIndex = 3;
            this.DgActiveDongleFeatures.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgActiveDongleFeatures_CellClick);
            // 
            // clmId
            // 
            this.clmId.DataPropertyName = "Id";
            this.clmId.HeaderText = "id";
            this.clmId.MinimumWidth = 6;
            this.clmId.Name = "clmId";
            this.clmId.ReadOnly = true;
            this.clmId.Visible = false;
            this.clmId.Width = 125;
            // 
            // clmFeatureName
            // 
            this.clmFeatureName.DataPropertyName = "Name";
            this.clmFeatureName.HeaderText = "الاسم";
            this.clmFeatureName.MinimumWidth = 6;
            this.clmFeatureName.Name = "clmFeatureName";
            this.clmFeatureName.ReadOnly = true;
            this.clmFeatureName.Width = 260;
            // 
            // clmPrice
            // 
            this.clmPrice.DataPropertyName = "Price";
            this.clmPrice.HeaderText = "السعر";
            this.clmPrice.MinimumWidth = 6;
            this.clmPrice.Name = "clmPrice";
            this.clmPrice.ReadOnly = true;
            this.clmPrice.Visible = false;
            this.clmPrice.Width = 50;
            // 
            // clmChoose
            // 
            this.clmChoose.DataPropertyName = "Value";
            this.clmChoose.HeaderText = "أختيار";
            this.clmChoose.MinimumWidth = 6;
            this.clmChoose.Name = "clmChoose";
            this.clmChoose.ReadOnly = true;
            this.clmChoose.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clmChoose.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.clmChoose.Width = 40;
            // 
            // cmbVersion
            // 
            this.cmbVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVersion.FormattingEnabled = true;
            this.cmbVersion.Location = new System.Drawing.Point(825, 228);
            this.cmbVersion.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbVersion.Name = "cmbVersion";
            this.cmbVersion.Size = new System.Drawing.Size(140, 24);
            this.cmbVersion.TabIndex = 1;
            this.cmbVersion.SelectedIndexChanged += new System.EventHandler(this.cmbVersion_SelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(973, 233);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 17);
            this.label19.TabIndex = 0;
            this.label19.Text = "نوع النسخة";
            this.label19.DoubleClick += new System.EventHandler(this.label19_DoubleClick);
            // 
            // btnReadData
            // 
            this.btnReadData.Location = new System.Drawing.Point(1072, 130);
            this.btnReadData.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnReadData.Name = "btnReadData";
            this.btnReadData.Size = new System.Drawing.Size(87, 66);
            this.btnReadData.TabIndex = 5;
            this.btnReadData.Text = "قراءة البيانات / دنكل";
            this.btnReadData.UseVisualStyleBackColor = true;
            this.btnReadData.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnAddDongle
            // 
            this.btnAddDongle.Location = new System.Drawing.Point(1072, 23);
            this.btnAddDongle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddDongle.Name = "btnAddDongle";
            this.btnAddDongle.Size = new System.Drawing.Size(87, 28);
            this.btnAddDongle.TabIndex = 1;
            this.btnAddDongle.Text = "اضافة دنكل";
            this.btnAddDongle.UseVisualStyleBackColor = true;
            this.btnAddDongle.Click += new System.EventHandler(this.btnAddDongle_Click_1);
            // 
            // btnCheckDongle
            // 
            this.btnCheckDongle.Location = new System.Drawing.Point(1072, 59);
            this.btnCheckDongle.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnCheckDongle.Name = "btnCheckDongle";
            this.btnCheckDongle.Size = new System.Drawing.Size(87, 28);
            this.btnCheckDongle.TabIndex = 2;
            this.btnCheckDongle.Text = "فحص الدنكل";
            this.btnCheckDongle.UseVisualStyleBackColor = true;
            this.btnCheckDongle.Click += new System.EventHandler(this.btnCheckDongle_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage5.Controls.Add(this.pnlUser);
            this.tabPage5.Controls.Add(this.btnExport);
            this.tabPage5.Controls.Add(this.dg1);
            this.tabPage5.Controls.Add(this.label31);
            this.tabPage5.Controls.Add(this.label30);
            this.tabPage5.Controls.Add(this.btnRefresh);
            this.tabPage5.Controls.Add(this.dtTo);
            this.tabPage5.Controls.Add(this.dtFrom);
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage5.Size = new System.Drawing.Size(1168, 754);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "تقرير المبيعات";
            // 
            // pnlUser
            // 
            this.pnlUser.Controls.Add(this.btnFetchDataAccounts);
            this.pnlUser.Controls.Add(this.cmbUser);
            this.pnlUser.Controls.Add(this.label29);
            this.pnlUser.Location = new System.Drawing.Point(798, 7);
            this.pnlUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlUser.Name = "pnlUser";
            this.pnlUser.Size = new System.Drawing.Size(365, 60);
            this.pnlUser.TabIndex = 10;
            // 
            // btnFetchDataAccounts
            // 
            this.btnFetchDataAccounts.Location = new System.Drawing.Point(8, 14);
            this.btnFetchDataAccounts.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFetchDataAccounts.Name = "btnFetchDataAccounts";
            this.btnFetchDataAccounts.Size = new System.Drawing.Size(87, 28);
            this.btnFetchDataAccounts.TabIndex = 0;
            this.btnFetchDataAccounts.Text = "جلب البيانات";
            this.btnFetchDataAccounts.UseVisualStyleBackColor = true;
            this.btnFetchDataAccounts.Click += new System.EventHandler(this.btnFetchDataAccounts_Click);
            // 
            // cmbUser
            // 
            this.cmbUser.FormattingEnabled = true;
            this.cmbUser.Location = new System.Drawing.Point(103, 16);
            this.cmbUser.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbUser.Name = "cmbUser";
            this.cmbUser.Size = new System.Drawing.Size(140, 24);
            this.cmbUser.TabIndex = 1;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(251, 20);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(101, 17);
            this.label29.TabIndex = 2;
            this.label29.Text = "أسم المستخدم";
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(1088, 78);
            this.btnExport.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 30);
            this.btnExport.TabIndex = 9;
            this.btnExport.Text = "تصدير";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // dg1
            // 
            this.dg1.AllowUserToAddRows = false;
            this.dg1.AllowUserToDeleteRows = false;
            this.dg1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmDg1Id,
            this.clmDg1MachineDate,
            this.clmDg1CostPrice,
            this.clmDg1CompanyName,
            this.clmDg1Explained,
            this.clmCompanyDetails4,
            this.clmDongleMovments});
            this.dg1.Location = new System.Drawing.Point(7, 113);
            this.dg1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dg1.Name = "dg1";
            this.dg1.ReadOnly = true;
            this.dg1.RowHeadersWidth = 51;
            this.dg1.Size = new System.Drawing.Size(1150, 628);
            this.dg1.TabIndex = 8;
            this.dg1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg1_CellClick);
            this.dg1.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dg1_RowPrePaint);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(133, 43);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(60, 17);
            this.label31.TabIndex = 7;
            this.label31.Text = "الى تاريخ";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(133, 11);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(57, 17);
            this.label30.TabIndex = 6;
            this.label30.Text = "من تاريخ";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(7, 71);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(119, 28);
            this.btnRefresh.TabIndex = 5;
            this.btnRefresh.Text = "تحديث البيانات";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTo.Location = new System.Drawing.Point(7, 39);
            this.dtTo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(118, 24);
            this.dtTo.TabIndex = 4;
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFrom.Location = new System.Drawing.Point(7, 7);
            this.dtFrom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(118, 24);
            this.dtFrom.TabIndex = 3;
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage6.Controls.Add(this.pnlUser2);
            this.tabPage6.Controls.Add(this.btnExport2);
            this.tabPage6.Controls.Add(this.dg2);
            this.tabPage6.Controls.Add(this.label32);
            this.tabPage6.Controls.Add(this.label33);
            this.tabPage6.Controls.Add(this.btnRefresh2);
            this.tabPage6.Controls.Add(this.dtTo2);
            this.tabPage6.Controls.Add(this.dtFrom2);
            this.tabPage6.Location = new System.Drawing.Point(4, 25);
            this.tabPage6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage6.Size = new System.Drawing.Size(1168, 754);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "تقرير المحذوفات";
            // 
            // pnlUser2
            // 
            this.pnlUser2.Controls.Add(this.btnFetchDataAccounts2);
            this.pnlUser2.Controls.Add(this.cmbUser2);
            this.pnlUser2.Controls.Add(this.label34);
            this.pnlUser2.Location = new System.Drawing.Point(761, 7);
            this.pnlUser2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlUser2.Name = "pnlUser2";
            this.pnlUser2.Size = new System.Drawing.Size(402, 53);
            this.pnlUser2.TabIndex = 19;
            // 
            // btnFetchDataAccounts2
            // 
            this.btnFetchDataAccounts2.Location = new System.Drawing.Point(54, 12);
            this.btnFetchDataAccounts2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFetchDataAccounts2.Name = "btnFetchDataAccounts2";
            this.btnFetchDataAccounts2.Size = new System.Drawing.Size(87, 28);
            this.btnFetchDataAccounts2.TabIndex = 9;
            this.btnFetchDataAccounts2.Text = "جلب البيانات";
            this.btnFetchDataAccounts2.UseVisualStyleBackColor = true;
            this.btnFetchDataAccounts2.Click += new System.EventHandler(this.btnFetchDataAccounts2_Click);
            // 
            // cmbUser2
            // 
            this.cmbUser2.FormattingEnabled = true;
            this.cmbUser2.Location = new System.Drawing.Point(148, 15);
            this.cmbUser2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbUser2.Name = "cmbUser2";
            this.cmbUser2.Size = new System.Drawing.Size(140, 24);
            this.cmbUser2.TabIndex = 10;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(296, 18);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(101, 17);
            this.label34.TabIndex = 11;
            this.label34.Text = "أسم المستخدم";
            // 
            // btnExport2
            // 
            this.btnExport2.Location = new System.Drawing.Point(1086, 73);
            this.btnExport2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExport2.Name = "btnExport2";
            this.btnExport2.Size = new System.Drawing.Size(75, 30);
            this.btnExport2.TabIndex = 18;
            this.btnExport2.Text = "تصدير";
            this.btnExport2.UseVisualStyleBackColor = true;
            this.btnExport2.Click += new System.EventHandler(this.btnExport2_Click);
            // 
            // dg2
            // 
            this.dg2.AllowUserToAddRows = false;
            this.dg2.AllowUserToDeleteRows = false;
            this.dg2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmDg2Id,
            this.clmDg2MachineDate,
            this.clmDg2CostPrice,
            this.clmDg2CompanyName,
            this.clmDg2Explained,
            this.clmCompanyDetails3});
            this.dg2.Location = new System.Drawing.Point(8, 113);
            this.dg2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dg2.Name = "dg2";
            this.dg2.ReadOnly = true;
            this.dg2.RowHeadersWidth = 51;
            this.dg2.Size = new System.Drawing.Size(1155, 628);
            this.dg2.TabIndex = 17;
            // 
            // clmDg2Id
            // 
            this.clmDg2Id.DataPropertyName = "Id";
            this.clmDg2Id.HeaderText = "رقم العملية";
            this.clmDg2Id.MinimumWidth = 6;
            this.clmDg2Id.Name = "clmDg2Id";
            this.clmDg2Id.ReadOnly = true;
            this.clmDg2Id.Width = 125;
            // 
            // clmDg2MachineDate
            // 
            this.clmDg2MachineDate.DataPropertyName = "MachineDate";
            this.clmDg2MachineDate.HeaderText = "التاريخ";
            this.clmDg2MachineDate.MinimumWidth = 6;
            this.clmDg2MachineDate.Name = "clmDg2MachineDate";
            this.clmDg2MachineDate.ReadOnly = true;
            this.clmDg2MachineDate.Width = 125;
            // 
            // clmDg2CostPrice
            // 
            this.clmDg2CostPrice.DataPropertyName = "CostPrice";
            this.clmDg2CostPrice.HeaderText = "السعر";
            this.clmDg2CostPrice.MinimumWidth = 6;
            this.clmDg2CostPrice.Name = "clmDg2CostPrice";
            this.clmDg2CostPrice.ReadOnly = true;
            this.clmDg2CostPrice.Width = 125;
            // 
            // clmDg2CompanyName
            // 
            this.clmDg2CompanyName.DataPropertyName = "CompanyName";
            this.clmDg2CompanyName.HeaderText = "اسم الشركة";
            this.clmDg2CompanyName.MinimumWidth = 6;
            this.clmDg2CompanyName.Name = "clmDg2CompanyName";
            this.clmDg2CompanyName.ReadOnly = true;
            this.clmDg2CompanyName.Width = 125;
            // 
            // clmDg2Explained
            // 
            this.clmDg2Explained.DataPropertyName = "Explained";
            this.clmDg2Explained.HeaderText = "التفاصيل";
            this.clmDg2Explained.MinimumWidth = 6;
            this.clmDg2Explained.Name = "clmDg2Explained";
            this.clmDg2Explained.ReadOnly = true;
            this.clmDg2Explained.Width = 125;
            // 
            // clmCompanyDetails3
            // 
            this.clmCompanyDetails3.DataPropertyName = "StrCompanyDetails";
            this.clmCompanyDetails3.HeaderText = "معلومات الشركة";
            this.clmCompanyDetails3.MinimumWidth = 6;
            this.clmCompanyDetails3.Name = "clmCompanyDetails3";
            this.clmCompanyDetails3.ReadOnly = true;
            this.clmCompanyDetails3.Width = 125;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(134, 43);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 17);
            this.label32.TabIndex = 16;
            this.label32.Text = "الى تاريخ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(134, 11);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(57, 17);
            this.label33.TabIndex = 15;
            this.label33.Text = "من تاريخ";
            // 
            // btnRefresh2
            // 
            this.btnRefresh2.Location = new System.Drawing.Point(8, 71);
            this.btnRefresh2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRefresh2.Name = "btnRefresh2";
            this.btnRefresh2.Size = new System.Drawing.Size(119, 28);
            this.btnRefresh2.TabIndex = 14;
            this.btnRefresh2.Text = "تحديث البيانات";
            this.btnRefresh2.UseVisualStyleBackColor = true;
            this.btnRefresh2.Click += new System.EventHandler(this.btnRefresh2_Click);
            // 
            // dtTo2
            // 
            this.dtTo2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTo2.Location = new System.Drawing.Point(8, 39);
            this.dtTo2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtTo2.Name = "dtTo2";
            this.dtTo2.Size = new System.Drawing.Size(118, 24);
            this.dtTo2.TabIndex = 13;
            // 
            // dtFrom2
            // 
            this.dtFrom2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFrom2.Location = new System.Drawing.Point(8, 7);
            this.dtFrom2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtFrom2.Name = "dtFrom2";
            this.dtFrom2.Size = new System.Drawing.Size(118, 24);
            this.dtFrom2.TabIndex = 12;
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage7.Controls.Add(this.pnlUser3);
            this.tabPage7.Controls.Add(this.btnExport3);
            this.tabPage7.Controls.Add(this.dg3);
            this.tabPage7.Controls.Add(this.label38);
            this.tabPage7.Controls.Add(this.label39);
            this.tabPage7.Controls.Add(this.btnRefresh3);
            this.tabPage7.Controls.Add(this.dtTo3);
            this.tabPage7.Controls.Add(this.dtFrom3);
            this.tabPage7.Location = new System.Drawing.Point(4, 25);
            this.tabPage7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage7.Size = new System.Drawing.Size(1168, 754);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "تقرير مختلط";
            // 
            // pnlUser3
            // 
            this.pnlUser3.Controls.Add(this.btnFetchDataAccounts3);
            this.pnlUser3.Controls.Add(this.cmbUser3);
            this.pnlUser3.Controls.Add(this.label40);
            this.pnlUser3.Location = new System.Drawing.Point(793, 14);
            this.pnlUser3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pnlUser3.Name = "pnlUser3";
            this.pnlUser3.Size = new System.Drawing.Size(364, 57);
            this.pnlUser3.TabIndex = 29;
            // 
            // btnFetchDataAccounts3
            // 
            this.btnFetchDataAccounts3.Location = new System.Drawing.Point(16, 5);
            this.btnFetchDataAccounts3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnFetchDataAccounts3.Name = "btnFetchDataAccounts3";
            this.btnFetchDataAccounts3.Size = new System.Drawing.Size(87, 28);
            this.btnFetchDataAccounts3.TabIndex = 19;
            this.btnFetchDataAccounts3.Text = "جلب البيانات";
            this.btnFetchDataAccounts3.UseVisualStyleBackColor = true;
            this.btnFetchDataAccounts3.Click += new System.EventHandler(this.btnFetchDataAccounts3_Click);
            // 
            // cmbUser3
            // 
            this.cmbUser3.FormattingEnabled = true;
            this.cmbUser3.Location = new System.Drawing.Point(111, 6);
            this.cmbUser3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbUser3.Name = "cmbUser3";
            this.cmbUser3.Size = new System.Drawing.Size(140, 24);
            this.cmbUser3.TabIndex = 20;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(260, 11);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(101, 17);
            this.label40.TabIndex = 21;
            this.label40.Text = "أسم المستخدم";
            // 
            // btnExport3
            // 
            this.btnExport3.Location = new System.Drawing.Point(1083, 78);
            this.btnExport3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExport3.Name = "btnExport3";
            this.btnExport3.Size = new System.Drawing.Size(75, 30);
            this.btnExport3.TabIndex = 28;
            this.btnExport3.Text = "تصدير";
            this.btnExport3.UseVisualStyleBackColor = true;
            this.btnExport3.Click += new System.EventHandler(this.btnExport3_Click);
            // 
            // dg3
            // 
            this.dg3.AllowUserToAddRows = false;
            this.dg3.AllowUserToDeleteRows = false;
            this.dg3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.clmKey,
            this.dataGridViewTextBoxColumn2,
            this.clmDeleted,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.clmCompanyDetails2,
            this.clmDongleMovment});
            this.dg3.Location = new System.Drawing.Point(5, 114);
            this.dg3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dg3.Name = "dg3";
            this.dg3.ReadOnly = true;
            this.dg3.RowHeadersWidth = 51;
            this.dg3.Size = new System.Drawing.Size(1158, 626);
            this.dg3.TabIndex = 27;
            this.dg3.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg3_CellClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.HeaderText = "رقم العملية";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 125;
            // 
            // clmKey
            // 
            this.clmKey.DataPropertyName = "DongleKey";
            this.clmKey.HeaderText = "رقم الدنكل";
            this.clmKey.MinimumWidth = 6;
            this.clmKey.Name = "clmKey";
            this.clmKey.ReadOnly = true;
            this.clmKey.Width = 125;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "MachineDate";
            this.dataGridViewTextBoxColumn2.HeaderText = "التاريخ";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 125;
            // 
            // clmDeleted
            // 
            this.clmDeleted.DataPropertyName = "IsDeleted";
            this.clmDeleted.HeaderText = "محذوف/غير محذوف";
            this.clmDeleted.MinimumWidth = 6;
            this.clmDeleted.Name = "clmDeleted";
            this.clmDeleted.ReadOnly = true;
            this.clmDeleted.Width = 125;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CostPrice";
            this.dataGridViewTextBoxColumn3.HeaderText = "السعر";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 125;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "CompanyName";
            this.dataGridViewTextBoxColumn4.HeaderText = "اسم الشركة";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 125;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Explained";
            this.dataGridViewTextBoxColumn5.HeaderText = "التفاصيل";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 125;
            // 
            // clmCompanyDetails2
            // 
            this.clmCompanyDetails2.DataPropertyName = "StrCompanyDetails";
            this.clmCompanyDetails2.HeaderText = "معلومات الشركة";
            this.clmCompanyDetails2.MinimumWidth = 6;
            this.clmCompanyDetails2.Name = "clmCompanyDetails2";
            this.clmCompanyDetails2.ReadOnly = true;
            this.clmCompanyDetails2.Width = 125;
            // 
            // clmDongleMovment
            // 
            this.clmDongleMovment.HeaderText = "عرض الحركات";
            this.clmDongleMovment.MinimumWidth = 6;
            this.clmDongleMovment.Name = "clmDongleMovment";
            this.clmDongleMovment.ReadOnly = true;
            this.clmDongleMovment.Width = 125;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(131, 46);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(60, 17);
            this.label38.TabIndex = 26;
            this.label38.Text = "الى تاريخ";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(131, 14);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(57, 17);
            this.label39.TabIndex = 25;
            this.label39.Text = "من تاريخ";
            // 
            // btnRefresh3
            // 
            this.btnRefresh3.Location = new System.Drawing.Point(5, 73);
            this.btnRefresh3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRefresh3.Name = "btnRefresh3";
            this.btnRefresh3.Size = new System.Drawing.Size(119, 28);
            this.btnRefresh3.TabIndex = 24;
            this.btnRefresh3.Text = "تحديث البيانات";
            this.btnRefresh3.UseVisualStyleBackColor = true;
            this.btnRefresh3.Click += new System.EventHandler(this.btnRefresh3_Click);
            // 
            // dtTo3
            // 
            this.dtTo3.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTo3.Location = new System.Drawing.Point(5, 41);
            this.dtTo3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtTo3.Name = "dtTo3";
            this.dtTo3.Size = new System.Drawing.Size(118, 24);
            this.dtTo3.TabIndex = 23;
            // 
            // dtFrom3
            // 
            this.dtFrom3.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFrom3.Location = new System.Drawing.Point(5, 9);
            this.dtFrom3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtFrom3.Name = "dtFrom3";
            this.dtFrom3.Size = new System.Drawing.Size(118, 24);
            this.dtFrom3.TabIndex = 22;
            // 
            // tabPage8
            // 
            this.tabPage8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tabPage8.Controls.Add(this.label45);
            this.tabPage8.Controls.Add(this.textBox1);
            this.tabPage8.Controls.Add(this.btnExport4);
            this.tabPage8.Controls.Add(this.dg4);
            this.tabPage8.Controls.Add(this.label41);
            this.tabPage8.Controls.Add(this.label42);
            this.tabPage8.Controls.Add(this.btnRefresh4);
            this.tabPage8.Controls.Add(this.dtTo4);
            this.tabPage8.Controls.Add(this.dtFrom4);
            this.tabPage8.Location = new System.Drawing.Point(4, 25);
            this.tabPage8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage8.Size = new System.Drawing.Size(1168, 754);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "التقرير اليومي";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(932, 54);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(144, 17);
            this.label45.TabIndex = 40;
            this.label45.Text = "بحث على اسم الشركة";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(686, 74);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(375, 24);
            this.textBox1.TabIndex = 39;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btnExport4
            // 
            this.btnExport4.Location = new System.Drawing.Point(1069, 69);
            this.btnExport4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnExport4.Name = "btnExport4";
            this.btnExport4.Size = new System.Drawing.Size(75, 30);
            this.btnExport4.TabIndex = 38;
            this.btnExport4.Text = "تصدير";
            this.btnExport4.UseVisualStyleBackColor = true;
            this.btnExport4.Click += new System.EventHandler(this.btnExport4_Click);
            // 
            // dg4
            // 
            this.dg4.AllowUserToAddRows = false;
            this.dg4.AllowUserToDeleteRows = false;
            this.dg4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmUserName,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.clmCompanyDetails,
            this.clmDongleMovment2,
            this.clmKey2});
            this.dg4.Location = new System.Drawing.Point(8, 113);
            this.dg4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dg4.Name = "dg4";
            this.dg4.ReadOnly = true;
            this.dg4.RowHeadersVisible = false;
            this.dg4.RowHeadersWidth = 51;
            this.dg4.Size = new System.Drawing.Size(1149, 630);
            this.dg4.TabIndex = 37;
            this.dg4.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg4_CellClick);
            this.dg4.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dg4_RowPrePaint);
            // 
            // clmUserName
            // 
            this.clmUserName.DataPropertyName = "UserName";
            this.clmUserName.HeaderText = "أسم المستخدم";
            this.clmUserName.MinimumWidth = 6;
            this.clmUserName.Name = "clmUserName";
            this.clmUserName.ReadOnly = true;
            this.clmUserName.Width = 120;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "MachineDate";
            this.dataGridViewTextBoxColumn8.HeaderText = "التاريخ";
            this.dataGridViewTextBoxColumn8.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 120;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "CompanyName";
            this.dataGridViewTextBoxColumn11.HeaderText = "اسم الشركة";
            this.dataGridViewTextBoxColumn11.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 120;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Explained";
            this.dataGridViewTextBoxColumn12.HeaderText = "التفاصيل";
            this.dataGridViewTextBoxColumn12.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 300;
            // 
            // clmCompanyDetails
            // 
            this.clmCompanyDetails.DataPropertyName = "StrCompanyDetails";
            this.clmCompanyDetails.HeaderText = "معلومات الشركة";
            this.clmCompanyDetails.MinimumWidth = 6;
            this.clmCompanyDetails.Name = "clmCompanyDetails";
            this.clmCompanyDetails.ReadOnly = true;
            this.clmCompanyDetails.Width = 200;
            // 
            // clmDongleMovment2
            // 
            this.clmDongleMovment2.HeaderText = "--";
            this.clmDongleMovment2.Image = global::DongleManagment.Properties.Resources.grid_details;
            this.clmDongleMovment2.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.clmDongleMovment2.MinimumWidth = 6;
            this.clmDongleMovment2.Name = "clmDongleMovment2";
            this.clmDongleMovment2.ReadOnly = true;
            this.clmDongleMovment2.Width = 35;
            // 
            // clmKey2
            // 
            this.clmKey2.DataPropertyName = "DongleKey";
            this.clmKey2.HeaderText = "رقم الدنكل";
            this.clmKey2.MinimumWidth = 6;
            this.clmKey2.Name = "clmKey2";
            this.clmKey2.ReadOnly = true;
            this.clmKey2.Visible = false;
            this.clmKey2.Width = 125;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(134, 44);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(60, 17);
            this.label41.TabIndex = 36;
            this.label41.Text = "الى تاريخ";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(134, 12);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(57, 17);
            this.label42.TabIndex = 35;
            this.label42.Text = "من تاريخ";
            // 
            // btnRefresh4
            // 
            this.btnRefresh4.Location = new System.Drawing.Point(8, 71);
            this.btnRefresh4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnRefresh4.Name = "btnRefresh4";
            this.btnRefresh4.Size = new System.Drawing.Size(119, 28);
            this.btnRefresh4.TabIndex = 34;
            this.btnRefresh4.Text = "تحديث البيانات";
            this.btnRefresh4.UseVisualStyleBackColor = true;
            this.btnRefresh4.Click += new System.EventHandler(this.btnRefresh4_Click);
            // 
            // dtTo4
            // 
            this.dtTo4.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTo4.Location = new System.Drawing.Point(8, 39);
            this.dtTo4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtTo4.Name = "dtTo4";
            this.dtTo4.Size = new System.Drawing.Size(118, 24);
            this.dtTo4.TabIndex = 33;
            // 
            // dtFrom4
            // 
            this.dtFrom4.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFrom4.Location = new System.Drawing.Point(8, 7);
            this.dtFrom4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtFrom4.Name = "dtFrom4";
            this.dtFrom4.Size = new System.Drawing.Size(118, 24);
            this.dtFrom4.TabIndex = 32;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "--";
            this.dataGridViewImageColumn1.Image = global::DongleManagment.Properties.Resources.grid_details;
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn1.MinimumWidth = 6;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Width = 35;
            // 
            // clmDg1Id
            // 
            this.clmDg1Id.DataPropertyName = "Id";
            this.clmDg1Id.HeaderText = "رقم العملية";
            this.clmDg1Id.MinimumWidth = 6;
            this.clmDg1Id.Name = "clmDg1Id";
            this.clmDg1Id.ReadOnly = true;
            this.clmDg1Id.Width = 125;
            // 
            // clmDg1MachineDate
            // 
            this.clmDg1MachineDate.DataPropertyName = "MachineDate";
            this.clmDg1MachineDate.HeaderText = "التاريخ";
            this.clmDg1MachineDate.MinimumWidth = 6;
            this.clmDg1MachineDate.Name = "clmDg1MachineDate";
            this.clmDg1MachineDate.ReadOnly = true;
            this.clmDg1MachineDate.Width = 125;
            // 
            // clmDg1CostPrice
            // 
            this.clmDg1CostPrice.DataPropertyName = "CostPrice";
            this.clmDg1CostPrice.HeaderText = "السعر";
            this.clmDg1CostPrice.MinimumWidth = 6;
            this.clmDg1CostPrice.Name = "clmDg1CostPrice";
            this.clmDg1CostPrice.ReadOnly = true;
            this.clmDg1CostPrice.Width = 125;
            // 
            // clmDg1CompanyName
            // 
            this.clmDg1CompanyName.DataPropertyName = "CompanyName";
            this.clmDg1CompanyName.HeaderText = "اسم الشركة";
            this.clmDg1CompanyName.MinimumWidth = 6;
            this.clmDg1CompanyName.Name = "clmDg1CompanyName";
            this.clmDg1CompanyName.ReadOnly = true;
            this.clmDg1CompanyName.Width = 125;
            // 
            // clmDg1Explained
            // 
            this.clmDg1Explained.DataPropertyName = "Explained";
            this.clmDg1Explained.HeaderText = "التفاصيل";
            this.clmDg1Explained.MinimumWidth = 6;
            this.clmDg1Explained.Name = "clmDg1Explained";
            this.clmDg1Explained.ReadOnly = true;
            this.clmDg1Explained.Width = 125;
            // 
            // clmCompanyDetails4
            // 
            this.clmCompanyDetails4.DataPropertyName = "StrCompanyDetails";
            this.clmCompanyDetails4.HeaderText = "معلومات الشركة";
            this.clmCompanyDetails4.MinimumWidth = 6;
            this.clmCompanyDetails4.Name = "clmCompanyDetails4";
            this.clmCompanyDetails4.ReadOnly = true;
            this.clmCompanyDetails4.Width = 125;
            // 
            // clmDongleMovments
            // 
            this.clmDongleMovments.HeaderText = "--";
            this.clmDongleMovments.Image = global::DongleManagment.Properties.Resources.grid_details;
            this.clmDongleMovments.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.clmDongleMovments.MinimumWidth = 6;
            this.clmDongleMovments.Name = "clmDongleMovments";
            this.clmDongleMovments.ReadOnly = true;
            this.clmDongleMovments.Width = 40;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1176, 783);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FrmMain";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "الواجهه الرئيسية";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVersions)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFeathures)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgActiveDongleFeatures)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.pnlUser.ResumeLayout(false);
            this.pnlUser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg1)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.pnlUser2.ResumeLayout(false);
            this.pnlUser2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg2)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.pnlUser3.ResumeLayout(false);
            this.pnlUser3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg3)).EndInit();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnAddAdmin;
        private System.Windows.Forms.Button btnAddAgent;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtConfirme;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEMail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnChangePassword;
        private System.Windows.Forms.TextBox txtNewConfirme;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNewPassword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtOldPassword;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnVersionRefrish;
        private System.Windows.Forms.DataGridView dgVersions;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnSaveVersion;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtVersionName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAgentSubVersion;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtMainVersion;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSubVersion;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtAgentMainVersion;
        private System.Windows.Forms.Button btnNewVersion;
        private System.Windows.Forms.Button btnDeleteVersion;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnNewFeatures;
        private System.Windows.Forms.Button btnDeleteFeatures;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtMainFeatures;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtSubFeatures;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtAgentMainFeatures;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtAgentSubFeatures;
        private System.Windows.Forms.Button btnSaveFeatures;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtFeaturesName;
        private System.Windows.Forms.Button btnFeaturesRefrish;
        private System.Windows.Forms.DataGridView dgFeathures;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button btnAddDongle;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btnCheckDongle;
        private System.Windows.Forms.Button btnReActiveDongle;
        private System.Windows.Forms.Button btnReadData;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton rdoSub;
        private System.Windows.Forms.RadioButton rdoServer;
        private System.Windows.Forms.DataGridView DgActiveDongleFeatures;
        private System.Windows.Forms.ComboBox cmbVersion;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btnSaveActiveDongle;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmVersionName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmFeaturesName;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtCompanyNote;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtCompanyJop;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtCompanyPhones;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtCompanyAddress;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.TextBox txtCompanyGovernorate;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox cmbUser;
        private System.Windows.Forms.Button btnFetchDataAccounts;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.DataGridView dg1;
        private System.Windows.Forms.DataGridView dg2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button btnRefresh2;
        private System.Windows.Forms.DateTimePicker dtTo2;
        private System.Windows.Forms.DateTimePicker dtFrom2;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox cmbUser2;
        private System.Windows.Forms.Button btnFetchDataAccounts2;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button btnRestPassword;
        private System.Windows.Forms.TextBox txtNewConfirme2;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtNewPassword2;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtEmail2;
        private System.Windows.Forms.Button btnAccountant;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnExport2;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Button btnExport3;
        private System.Windows.Forms.DataGridView dg3;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button btnRefresh3;
        private System.Windows.Forms.DateTimePicker dtTo3;
        private System.Windows.Forms.DateTimePicker dtFrom3;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ComboBox cmbUser3;
        private System.Windows.Forms.Button btnFetchDataAccounts3;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Button btnExport4;
        private System.Windows.Forms.DataGridView dg4;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Button btnRefresh4;
        private System.Windows.Forms.DateTimePicker dtTo4;
        private System.Windows.Forms.DateTimePicker dtFrom4;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg2Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg2MachineDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg2CostPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg2CompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg2Explained;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmCompanyDetails3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmKey;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDeleted;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmCompanyDetails2;
        private System.Windows.Forms.DataGridViewImageColumn clmDongleMovment;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox txtOldVersionDetails;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label lblOldVersionKey;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label lblOldVersionState;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button btnRefrish;
        private System.Windows.Forms.CheckBox checkUpgrad;
        private System.Windows.Forms.Panel pnlUser;
        private System.Windows.Forms.Panel pnlUser2;
        private System.Windows.Forms.Panel pnlUser3;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label lblMatch;
        private System.Windows.Forms.Label lblDongleActivationInfo;
        private System.Windows.Forms.Label txtActivitionInfo;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmCompanyDetails;
        private System.Windows.Forms.DataGridViewImageColumn clmDongleMovment2;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmKey2;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmId;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmFeatureName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmPrice;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmChoose;
        private System.Windows.Forms.DateTimePicker dtpDeActivationDate;
        private System.Windows.Forms.CheckBox chkAutoDeActivate;
        private System.Windows.Forms.CheckBox chkMaster;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.ComboBox cmbcurrency;
        private System.Windows.Forms.TextBox txtMasterPassword;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg1Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg1MachineDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg1CostPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg1CompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg1Explained;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmCompanyDetails4;
        private System.Windows.Forms.DataGridViewImageColumn clmDongleMovments;
    }
}