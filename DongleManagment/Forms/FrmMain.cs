﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows.Forms;
using DongleManagment.Models;
using DongleManagment.ProjectClasses;
using Newtonsoft.Json;
using Version = DongleManagment.Models.Version;
using Morabaa6;
using System.Drawing;

using System.Drawing.Printing;
using Telerik.Reporting.Processing;

namespace DongleManagment.Forms
{
    public partial class FrmMain : Form
    {

        public class ViewData
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public decimal Price { get; set; }
            public bool Value { get; set; }
        }

        private List<Version> _versions = new List<Version>();
        private List<Features> _featureses = new List<Features>();
        private List<ViewData> ActivitionFeatures = new List<ViewData>();
        private Version _version;
        private Features _feature;
        private bool _isAgent;
        public FrmMain()
        {
            InitializeComponent();
        }
        public FrmMain(bool isagent) {
            InitializeComponent();
            _isAgent = isagent;
        }
        private void FrmMain_Load(object sender, EventArgs e) {
            cmbcurrency.SelectedIndex = 0;
            dgFeathures.AutoGenerateColumns = false;
            dgVersions.AutoGenerateColumns = false;
            if (Pv.User.Type == UserViewModel.UserType.Accountant) {
                groupBox1.Visible = false;
                groupBox12.Visible = false;
                tabControl1.TabPages.Remove(tabPage2);
                tabControl1.TabPages.Remove(tabPage3);
                tabControl1.TabPages.Remove(tabPage4);
                btnAddDongle.Visible = false;
            }
            if (Pv.User.Type == UserViewModel.UserType.Agent) {
                groupBox1.Visible = false;
                groupBox12.Visible = false;
                tabControl1.TabPages.Remove(tabPage2);
                tabControl1.TabPages.Remove(tabPage3);
                //tabControl1.TabPages.Remove(tabPage7);
                tabControl1.TabPages.Remove(tabPage8);
                pnlUser.Visible = false;
                pnlUser2.Visible = false;
                pnlUser3.Visible = false;
                btnAddDongle.Visible = false;

            }

        }

        #region Dongle
        private void btnAddDongle_Click_1(object sender, EventArgs e) {
            try {
                var dongleKey = Morabaa7.MyClass.GetId();
                if (dongleKey == "0") {
                    MessageBox.Show("Error no Dongle!");
                    return;
                }
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                var dongle = new Dongle {
                    ActiveKey = Guid.Empty,
                    DongleKey = dongleKey,

                };

                var response = client.PostAsJsonAsync("api/Dongle/AddDongle", dongle).Result;

                if (response.IsSuccessStatusCode) {
                    var value = response.Content.ReadAsAsync<Dongle.DongleResult>().Result;
                    if (value == Dongle.DongleResult.Error)
                        MessageBox.Show("حدث خطأ....");
                    else if (value == Dongle.DongleResult.Found)
                        MessageBox.Show("هذا الدنكل موجود");
                    else if (value == Dongle.DongleResult.Ok)
                        MessageBox.Show("تم الحفظ");
                    else
                        MessageBox.Show("حدث خطأ .............");

                } else {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }


            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }
        }
        private void btnCheckDongle_Click(object sender, EventArgs e) {
            try {
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                var dongleKey = Morabaa7.MyClass.GetId();
                if (dongleKey == "0") {
                    MessageBox.Show("Error no Dongle!");
                    return;
                }

                var response = client.GetAsync("api/Dongle/CheckDongle?dongleKey=" + dongleKey).Result;

                if (response.IsSuccessStatusCode) {
                    var result = response.Content.ReadAsAsync<Dongle.DongleResult>().Result;
                    if (result == Dongle.DongleResult.Ok)
                        MessageBox.Show("مفعل....");
                    else if (result == Dongle.DongleResult.NotFound)
                        MessageBox.Show("غير موجود");
                    else if (result == Dongle.DongleResult.Error)
                        MessageBox.Show("حدث خطأ");
                    else if (result == Dongle.DongleResult.NotActive)
                        MessageBox.Show("غير مفعل......");




                } else {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnReActiveDongle_Click(object sender, EventArgs e) {
            try {
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                var dongleKey = Morabaa7.MyClass.GetId();
                if (dongleKey == "0") {
                    MessageBox.Show("Error no Dongle!");
                    return;
                }

                var response = client.GetAsync("api/Dongle/ReactiveDongle?dongleKey=" + dongleKey).Result;

                if (response.IsSuccessStatusCode) {
                    var result = response.Content.ReadAsAsync<ReActiveDongleViewModel>().Result;
                    if (result == null || result.ActiveDongle == null) {
                        MessageBox.Show("هذا الدنكل غير مفعل او غير موجود");
                        return;
                    }

                    var holdData = new DongleHoldData {
                        IsMain = rdoServer.Checked,
                        VersionId = result.ActiveDongle.VersionId,
                        Features = result.ActiveDongle.FeatureList.Select(c => c.FeatureId).ToList()
                    };

                    var str = JsonConvert.SerializeObject(holdData) + "@";
                    if (Morabaa7.MyClass.WriteData(str))
                        MessageBox.Show("OK");
                    else
                        MessageBox.Show("Error");


                } else {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }
        private string getDongleData() {
            try {
                var str = Morabaa7.MyClass.ReadData();
                if (string.IsNullOrEmpty(str)) {
                    return "حصل خطأ في قراءة البيانات";
                }
                var strArray = str.Split('@');
                var num1 = strArray[0];
                var holdData = JsonConvert.DeserializeObject<DongleHoldData>(num1);
                return Pv.DongleActivitionString(holdData,"");
            } catch (Exception exception) {
                return exception.Message;
            }
        }
        private string getServerData(bool CalledFromSave = false) {
            try {
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                var dongleKey = Morabaa7.MyClass.GetId();
                if (dongleKey == "0") {
                    return "لايوجد دنكل او الدنكل غير مسجل في الشركة";
                }

                var response = client.GetAsync("api/Dongle/GetActiveDongle?dongleKey=" + dongleKey).Result;

                if (response.IsSuccessStatusCode) {
                    var result = response.Content.ReadAsAsync<ReActiveDongleViewModel>().Result;
                    if (result == null || result.ActiveDongle == null) {
                        //MessageBox.Show("هذا الدنكل غير مفعل او غير موجود");
                        if (CalledFromSave) return "null";
                        return "هذا الدنكل غير مفعل او غير موجود";
                    } else if (CalledFromSave) return "";

                    var holdData = new DongleHoldData {
                        IsMain = result.ActiveDongle.IsMain,
                        AutoDeActivate = result.ActiveDongle.AutoDeActivate,
                        DeActivationDate = result.ActiveDongle.DeActivationDate,
                        Master = result.ActiveDongle.Master,
                        VersionId = result.ActiveDongle.VersionId,
                        Features = result.ActiveDongle.FeatureList.Select(c => c.FeatureId).ToList()
                    };
                    if (!CalledFromSave) {
                        if (cmbVersion.Items.Count == 0) FetchData();
                        cmbVersion.SelectedValue = holdData.VersionId;
                        
                    }
                    if (holdData.IsMain) rdoServer.Checked = true;
                    else rdoSub.Checked = true;
                    chkAutoDeActivate.Checked = holdData.AutoDeActivate;
                    if (holdData.AutoDeActivate)
                        dtpDeActivationDate.Value = holdData.DeActivationDate;
                    GetActivitionFeatures(holdData.Features);
                    chkMaster.Checked = holdData.Master;
                    txtActivitionInfo.Text = Pv.DongleActivitionString(holdData, result.ActiveDongle.StrUpgraded);
                    txtCompanyAddress.Text = result.ActiveDongle.CompanyAddress;
                    txtCompanyJop.Text = result.ActiveDongle.CompanyJop;
                    txtPrice.Text = result.ActiveDongle.SalePrice.ToFormattedNumber();
                    txtCompanyNote.Text = result.ActiveDongle.CompanyNote;
                    txtCompanyPhones.Text = result.ActiveDongle.CompanyPhones;
                    txtCompanyGovernorate.Text = result.ActiveDongle.CompanyGovernorate;
                    txtCompanyName.Text = result.ActiveDongle.CompanyName;
                    return Pv.DongleActivitionString(holdData, result.ActiveDongle.StrUpgraded);

                } else {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                    return "حصل خطأ اثناء محاولة قراءة البيانات";
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
                return ex.Message;
            }

        }
        private void CheckDongleState() {
            Cursor = Cursors.WaitCursor;
            var serverdata = getServerData();
            var dongleData = getDongleData();
            lblDongleActivationInfo.Text = dongleData;

            if (txtActivitionInfo.Text.Split('/').First() == lblDongleActivationInfo.Text.Split('/').First()) {
                lblMatch.Text = "البيانات متطابقة";
                lblMatch.ForeColor = Color.Green;
                btnReActiveDongle.Visible = false;
            } else {
                lblMatch.Text = "البيانات غير متطابقة";
                lblMatch.ForeColor = Color.Red;
                btnReActiveDongle.Visible = true;
            }
            Cursor = Cursors.Default;
        }
        private void button2_Click(object sender, EventArgs e) {
            CheckDongleState();
            //var f = new FrmViewDongleData(dongleData, serverdata);
            //f.ShowDialog();
            //f.Dispose();
            //MessageBox.Show(getDongleData());

        }
        private void FetchData() {
            try {
                //جلب انواع النسخ
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);
                var response = client.GetAsync("api/Dongle/Versions").Result;

                if (response.IsSuccessStatusCode) {
                    _versions = response.Content.ReadAsAsync<List<Version>>().Result;

                    cmbVersion.ValueMember = "Id";
                    cmbVersion.DisplayMember = "Name";
                    cmbVersion.DataSource = _versions;
                    if (cmbVersion.DataSource != null)
                        cmbVersion.SelectedIndex = 0;

                } else {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }

                //جلب الاضافات
                client = new HttpClient { BaseAddress = Pv.BaseAddress };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);
                response = client.GetAsync("api/Dongle/Features").Result;

                if (response.IsSuccessStatusCode) {
                    _featureses = response.Content.ReadAsAsync<List<Features>>().Result;


                } else {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }


            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void SaveDongleData() {
            try {
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                var activeDongleFeatures = new List<ActiveDongleFeature>();
                foreach (var feature in ActivitionFeatures) {
                    if (feature.Value)
                        activeDongleFeatures.Add(new ActiveDongleFeature {
                            FeatureId = feature.Id
                        });
                }
                var dongleKey = Morabaa7.MyClass.GetId();
                if (dongleKey == "0") {
                    MessageBox.Show("Error no Dongle!");
                    return;
                }
                if (checkUpgrad.Checked && !obj.State) {
                    MessageBox.Show("النسخة غير مرخصة لا يمكن الاستمرار بالعملية!");
                    return;
                }
                var activition = new ActiveDongle {
                    CostPrice = Convert.ToDecimal(lblAmount.Text),
                    
                    Date = DateTime.Now,
                    AutoDeActivate = chkAutoDeActivate.Checked,
                    DeActivationDate = dtpDeActivationDate.Value,
                    Master = chkMaster.Checked,
                    Deleted = false,
                    Discount = 0,
                    SalePrice = txtPrice.Text.ToDecimal(),
                    DongleKey = dongleKey,
                    UserId = Pv.User.Id,
                    VersionId = _version.Id,
                    OfferId = 0,
                    Id = 0,
                    FeatureList = activeDongleFeatures,
                    ApplicationId = 1,
                    CompanyAddress = txtCompanyAddress.Text,
                    CompanyJop = txtCompanyJop.Text,
                    CompanyName = txtCompanyName.Text,
                    CompanyNote = txtCompanyNote.Text,
                    CompanyPhones = txtCompanyPhones.Text,
                    CompanyGovernorate = txtCompanyGovernorate.Text,
                    IsMain = rdoServer.Checked,

                };
                if (checkUpgrad.Checked && obj.State) {
                    activition.IsUpgraded = true;
                    activition.OldVersionKey = obj.Key;
                    activition.OldVersionType = obj.Version;
                    if (!string.IsNullOrEmpty(_ignoreKey)) {
                        activition.IsIgnored = true;
                        activition.IgnoreKey = _ignoreKey;
                    }
                }

                var response = client.PostAsJsonAsync("api/Dongle/ActiveDongle", activition).Result;

                if (response.IsSuccessStatusCode) {
                    var dongle = response.Content.ReadAsAsync<Dongle>().Result;
                    if (dongle == null)
                        MessageBox.Show("حدث خطأ....");
                    else {
                        if (dongle.Result == Dongle.DongleResult.Upgraded) {
                            MessageBox.Show("كود التفعيل موجود مسبقا لا يمكن الاستمرار بالعملية!");
                            return;
                        }
                        var holdData = new DongleHoldData {
                            IsMain = rdoServer.Checked,
                            AutoDeActivate = activition.AutoDeActivate,
                            Master = activition.Master,
                            DeActivationDate = activition.DeActivationDate,
                            VersionId = activition.VersionId,
                            Features = activition.FeatureList.Select(c => c.FeatureId).ToList()
                        };
                        var str = JsonConvert.SerializeObject(holdData) + "@";
                        if (Morabaa7.MyClass.WriteData(str)) {
                            if (checkUpgrad.Checked)
                                Activiation.DeleteActivationCodeFromAccountancy();
                            if (rdoServer.Checked) {
                                print(dongle.number);
                                print(dongle.number);
                            }
                            MessageBox.Show("OK");
                            Clear();
                        } else
                            MessageBox.Show("Error");
                        
                    }


                } else {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }


        private bool CheckCompanyInfo() {
            if (txtCompanyName.Text.Trim() == "") { MessageBox.Show("يجب كتابة إسم الشركة قبل الحفظ");return false; }
            if (txtCompanyPhones.Text.Trim() == "") { MessageBox.Show("يجب كتابة رقم الهاتف قبل الحفظ"); return false; }
            if (txtCompanyAddress.Text.Trim() == "") { MessageBox.Show("يجب كتابة عنوان الشركة قبل الحفظ"); return false; }

            return true;
        }
        private void btnSaveActiveDongle_Click(object sender, EventArgs e) {
            if (CheckCompanyInfo() == false) return;
            Cursor = Cursors.WaitCursor;
            var s = getServerData(true);
            if(s=="null")
                SaveDongleData();
            else
                UpdateDongleData();

            CheckDongleState();
            Cursor = Cursors.Default;
        }
        private void UpdateDongleData() {
            try {
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                var activeDongleFeatures = new List<ActiveDongleFeature>();
                foreach (var feature in ActivitionFeatures) {
                    if (feature.Value)
                        activeDongleFeatures.Add(new ActiveDongleFeature {
                            FeatureId = feature.Id
                        });
                }
                var dongleKey = Morabaa7.MyClass.GetId();
                if (dongleKey == "0") {
                    MessageBox.Show("Error no Dongle!");
                    return;
                }
                var activition = new ActiveDongle {
                    CostPrice = Convert.ToDecimal(lblAmount.Text),
                    Date = DateTime.Now,
                    AutoDeActivate = chkAutoDeActivate.Checked,
                    DeActivationDate = dtpDeActivationDate.Value,
                    Master = chkMaster.Checked,
                    Deleted = false,
                    Discount = 0,
                    SalePrice = txtPrice.Text.ToDecimal(),
                    DongleKey = dongleKey,
                    UserId = Pv.User.Id,
                    VersionId = _version.Id,
                    OfferId = 0,
                    Id = 0,
                    FeatureList = activeDongleFeatures,
                    ApplicationId = 1,
                    CompanyAddress = txtCompanyAddress.Text,
                    CompanyJop = txtCompanyJop.Text,
                    CompanyName = txtCompanyName.Text,
                    CompanyNote = txtCompanyNote.Text,
                    CompanyPhones = txtCompanyPhones.Text,
                    CompanyGovernorate = txtCompanyGovernorate.Text,
                    IsMain = rdoServer.Checked
                };

                var response = client.PostAsJsonAsync("api/Dongle/UpdateActiveDongle", activition).Result;

                if (response.IsSuccessStatusCode) {
                    var dongle = response.Content.ReadAsAsync<Dongle>().Result;
                    if (dongle == null)
                        MessageBox.Show("حدث خطأ....");
                    else {
                        var holdData = new DongleHoldData {
                            IsMain = rdoServer.Checked,
                            AutoDeActivate = activition.AutoDeActivate,
                            Master = activition.Master,
                            DeActivationDate = activition.DeActivationDate,
                            VersionId = activition.VersionId,
                            Features = activition.FeatureList.Select(c => c.FeatureId).ToList()
                        };

                        var str = JsonConvert.SerializeObject(holdData) + "@";
                        if (Morabaa7.MyClass.WriteData(str)) {
                            if (rdoServer.Checked) {
                                print(dongle.number);
                                print(dongle.number);
                            }
                            
                            MessageBox.Show("OK");
                            Clear();
                        } else
                            MessageBox.Show("Error");

                        /*
                         var holdData = new DongleHoldData {
                            IsMain = rdoServer.Checked,
                            DongleKey = dongleKey,
                            VersionId = activition.VersionId,
                            AutoDeActivate = activition.AutoDeActivate,
                            Master = activition.Master,
                            DeActivationDate = activition.DeActivationDate,
                            Features = activition.FeatureList.Select(c => c.FeatureId).ToList()
                        };
                        var str = JsonConvert.SerializeObject(holdData) + "@";
                        if (Morabaa7.MyClass.WriteData(str)) {
                            if (checkUpgrad.Checked)
                                Activiation.DeleteActivationCodeFromAccountancy();
                            MessageBox.Show("OK");
                            Clear();
                        } else
                            MessageBox.Show("Error");
                         */
                    }


                } else {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void Refrsh() {
            try {
                if (_version == null || _versions == null) return;
                if (cmbVersion.SelectedIndex == -1 ||
                    _versions.Count == 0 ||
                    _featureses.Count == 0)
                    return;
                decimal sum = 0;
                if (rdoServer.Checked) {
                    sum = _version.Prices.Single(c => c.Type == VersionPrice.VersionType.MainVersion)
                        .Price;
                } else {
                    sum = _version.Prices.Single(c => c.Type == VersionPrice.VersionType.SubVersion)
                        .Price;
                }
                foreach (var data in ActivitionFeatures) {
                    if (data.Value)
                        sum += data.Price;
                }

                lblAmount.Text = sum.ToString();
            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }
        private void button1_Click(object sender, EventArgs e) {

            //var serverdata = getServerData();
            //var f = new FrmViewDongleData(getDongleData(), serverdata);
            //f.ShowDialog();
            //f.Dispose();
        }
        private void btnRefrish_Click(object sender, EventArgs e) {
            try {
                obj = Activiation.GetInfo();


                if (obj.State) {
                    lblOldVersionState.Text = "مفعلة";
                    lblOldVersionKey.Text = obj.Key;
                    txtOldVersionDetails.Text = obj.Version;
                } else {
                    lblOldVersionState.Text = "غير مفعلة";
                    lblOldVersionKey.Text = "-----------------------";
                    txtOldVersionDetails.Text = "-----------------------";
                }
            } catch (Exception exception) {
                MessageBox.Show(exception.Message);
            }


        }
        
        

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e) {
            if (tabControl1.SelectedTab == tabPage4) {
                Refrsh();
                FetchData();
            }
        }
        #endregion




        private void btnAddAgent_Click(object sender, EventArgs e) {
            try {
                if (string.IsNullOrEmpty(txtUserName.Text) ||
                    string.IsNullOrEmpty(txtEMail.Text) ||
                    string.IsNullOrEmpty(txtPassword.Text) ||
                    string.IsNullOrEmpty(txtConfirme.Text)) {
                    MessageBox.Show("البيانات المدخلة غير صحيحة");
                    return;
                }
                if (txtPassword.Text.Trim() != txtConfirme.Text.Trim()) {
                    MessageBox.Show("كلمة المرور غير متطابقة");
                    return;
                }
                //-----------------------------------------------
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                var user = new RegisterBindingModel() {
                    UserType = UserViewModel.UserType.Agent,
                    ConfirmPassword = txtConfirme.Text.Trim(),
                    Email = txtEMail.Text.Trim(),
                    Password = txtPassword.Text.Trim(),
                    UserName = txtUserName.Text.Trim()
                };

                var response = client.PostAsJsonAsync("api/Account/RegisterAgent", user).Result;

                if (response.IsSuccessStatusCode)
                    MessageBox.Show("تم الحفظ");
                else
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);

            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnAddAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtUserName.Text) ||
                    string.IsNullOrEmpty(txtEMail.Text) ||
                    string.IsNullOrEmpty(txtPassword.Text) ||
                    string.IsNullOrEmpty(txtConfirme.Text))
                {
                    MessageBox.Show("البيانات المدخلة غير صحيحة");
                    return;
                }
                if (txtPassword.Text.Trim() != txtConfirme.Text.Trim())
                {
                    MessageBox.Show("كلمة المرور غير متطابقة");
                    return;
                }
                //-----------------------------------------------
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                var user = new RegisterBindingModel()
                {
                    UserType = UserViewModel.UserType.Admin,
                    ConfirmPassword = txtConfirme.Text.Trim(),
                    Email = txtEMail.Text.Trim(),
                    Password = txtPassword.Text.Trim(),
                    UserName = txtUserName.Text.Trim()
                };

                var response = client.PostAsJsonAsync("api/Account/RegisterAdmin", user).Result;

                if (response.IsSuccessStatusCode)
                    MessageBox.Show("تم الحفظ");
                else
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtOldPassword.Text) ||
                    string.IsNullOrEmpty(txtNewConfirme.Text) ||
                    string.IsNullOrEmpty(txtNewPassword.Text))
                {
                    MessageBox.Show("البيانات المدخلة غير صحيحة");
                    return;
                }
                if (txtNewConfirme.Text.Trim() != txtNewPassword.Text.Trim())
                {
                    MessageBox.Show("كلمة المرور غير متطابقة");
                    return;
                }
                //-----------------------------------------------
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                var newPasswoed = new ChangePasswordBindingModel()
                {
                    ConfirmPassword = txtNewConfirme.Text.Trim(),
                    NewPassword = txtNewPassword.Text.Trim(),
                    OldPassword = txtOldPassword.Text.Trim()

                };

                var response = client.PostAsJsonAsync("api/Account/ChangePassword", newPasswoed).Result;

                if (response.IsSuccessStatusCode)
                    MessageBox.Show("تم الحفظ");

                else
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnVersionRefrish_Click(object sender, EventArgs e)
        {
            try
            {
                ClearVersion();
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);
                var response = client.GetAsync("api/Dongle/Versions").Result;

                if (response.IsSuccessStatusCode)
                {
                    _versions = response.Content.ReadAsAsync<List<Version>>().Result;

                    dgVersions.DataSource = _versions;

                }
                else
                {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ClearVersion()
        {
            txtVersionName.Text = "";
            txtAgentMainVersion.Text = "";
            txtAgentSubVersion.Text = "";
            txtMainVersion.Text = "";
            txtSubVersion.Text = "";
            _version = null;
        }

        private void ClearFeatures()
        {
            txtFeaturesName.Text = "";
            txtAgentMainFeatures.Text = "";
            txtAgentSubFeatures.Text = "";
            txtMainFeatures.Text = "";
            txtSubFeatures.Text = "";
            _feature = null;
        }

        private void btnNewVersion_Click(object sender, EventArgs e)
        {
            ClearVersion();

        }

        private void btnSaveVersion_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtVersionName.Text) ||
                    string.IsNullOrEmpty(txtMainVersion.Text) ||
                    string.IsNullOrEmpty(txtSubVersion.Text) ||
                    string.IsNullOrEmpty(txtAgentMainVersion.Text) ||
                    string.IsNullOrEmpty(txtAgentSubVersion.Text))
                {
                    MessageBox.Show("البيانات المدخلة غير صحيحة");
                    return;
                }
                if (_version == null)
                {
                    _version = new Version
                    {
                        Name = txtVersionName.Text,
                        ApplicationId = 1,
                        Prices = new List<VersionPrice>
                        {
                            new VersionPrice
                            {
                                Price = Convert.ToDecimal(txtMainVersion.Text),
                                Type = VersionPrice.VersionType.MainVersion
                            },
                            new VersionPrice
                            {
                                Price = Convert.ToDecimal(txtSubVersion.Text),
                                Type = VersionPrice.VersionType.SubVersion
                            },
                            new VersionPrice
                            {
                                Price = Convert.ToDecimal(txtAgentMainVersion.Text),
                                Type = VersionPrice.VersionType.AgentMainVersion
                            },
                            new VersionPrice
                            {
                                Price = Convert.ToDecimal(txtAgentSubVersion.Text),
                                Type = VersionPrice.VersionType.AgentSubVersion
                            },
                        }
                    };
                    //-----------------------------------------------
                    var client = new HttpClient { BaseAddress = Pv.BaseAddress };
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);



                    var response = client.PostAsJsonAsync("api/Dongle/AddVersion", _version).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("تم الحفظ");
                        ClearVersion();
                    }
                    else
                    {
                        MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                        _version = null;
                    }



                }
                else
                {
                    _version.Name = txtVersionName.Text;
                    _version.ApplicationId = 1;
                    _version.Prices.Single(c => c.Type == VersionPrice.VersionType.MainVersion).Price =
                        Convert.ToDecimal(txtMainVersion.Text);

                    _version.Prices.Single(c => c.Type == VersionPrice.VersionType.SubVersion).Price =
                        Convert.ToDecimal(txtSubVersion.Text);

                    _version.Prices.Single(c => c.Type == VersionPrice.VersionType.AgentMainVersion).Price =
                        Convert.ToDecimal(txtAgentMainVersion.Text);

                    _version.Prices.Single(c => c.Type == VersionPrice.VersionType.AgentSubVersion).Price =
                        Convert.ToDecimal(txtAgentSubVersion.Text);

                    //-----------------------------------------------
                    var client = new HttpClient { BaseAddress = Pv.BaseAddress };
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                    var response = client.PutAsJsonAsync("api/Dongle/UpdateVersion", _version).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("تم الحفظ");
                        ClearVersion();
                    }
                    else
                        MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void dgVersions_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == -1) return;
                _version = _versions[e.RowIndex];
                txtVersionName.Text = _version.Name;

                txtSubVersion.Text = _version.Prices.Single(c => c.Type == VersionPrice.VersionType.SubVersion)
                    .Price.ToString();

                txtAgentMainVersion.Text = _version.Prices.Single(c => c.Type == VersionPrice.VersionType.AgentMainVersion)
                    .Price.ToString();

                txtMainVersion.Text = _version.Prices.Single(c => c.Type == VersionPrice.VersionType.MainVersion)
                    .Price.ToString();

                txtAgentSubVersion.Text = _version.Prices.Single(c => c.Type == VersionPrice.VersionType.AgentSubVersion)
                    .Price.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDeleteVersion_Click(object sender, EventArgs e)
        {
            try
            {
                if (_version != null)
                {
                    //-----------------------------------------------
                    var client = new HttpClient { BaseAddress = Pv.BaseAddress };
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                    var response = client.PutAsJsonAsync("api/Dongle/DeleteVersion", _version).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("تم الحفظ");
                        ClearVersion();
                    }
                    else
                        MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnNewFeatures_Click(object sender, EventArgs e)
        {
            ClearFeatures();
        }

        private void btnFeaturesRefrish_Click(object sender, EventArgs e)
        {
            try
            {
                ClearFeatures();
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);
                var response = client.GetAsync("api/Dongle/Features").Result;

                if (response.IsSuccessStatusCode)
                {
                    _featureses = response.Content.ReadAsAsync<List<Features>>().Result;

                    dgFeathures.DataSource = _featureses;

                }
                else
                {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDeleteFeatures_Click(object sender, EventArgs e)
        {
            try
            {
                if (_feature != null)
                {
                    //-----------------------------------------------
                    var client = new HttpClient { BaseAddress = Pv.BaseAddress };
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                    var response = client.PutAsJsonAsync("api/Dongle/DeleteFeatures", _feature).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("تم الحفظ");
                        ClearFeatures();
                    }
                    else
                        MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSaveFeatures_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtFeaturesName.Text) ||
                    string.IsNullOrEmpty(txtMainFeatures.Text) ||
                    string.IsNullOrEmpty(txtSubFeatures.Text) ||
                    string.IsNullOrEmpty(txtAgentMainFeatures.Text) ||
                    string.IsNullOrEmpty(txtAgentSubFeatures.Text))
                {
                    MessageBox.Show("البيانات المدخلة غير صحيحة");
                    return;
                }
                if (_feature == null)
                {
                    _feature = new Features
                    {
                        Name = txtFeaturesName.Text,
                        ApplicationId = 1,
                        Prices = new List<FeaturesPrice>
                        {
                            new FeaturesPrice()
                            {
                                Price = Convert.ToDecimal(txtMainFeatures.Text),
                                Type = VersionPrice.VersionType.MainVersion
                            },
                            new FeaturesPrice()
                            {
                                Price = Convert.ToDecimal(txtSubFeatures.Text),
                                Type = VersionPrice.VersionType.SubVersion
                            },
                            new FeaturesPrice()
                            {
                                Price = Convert.ToDecimal(txtAgentMainFeatures.Text),
                                Type = VersionPrice.VersionType.AgentMainVersion
                            },
                            new FeaturesPrice()
                            {
                                Price = Convert.ToDecimal(txtAgentSubFeatures.Text),
                                Type = VersionPrice.VersionType.AgentSubVersion
                            },
                        }
                    };
                    //-----------------------------------------------
                    var client = new HttpClient { BaseAddress = Pv.BaseAddress };
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);



                    var response = client.PostAsJsonAsync("api/Dongle/AddFeatures", _feature).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("تم الحفظ");
                        ClearFeatures();
                    }
                    else
                    {
                        MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                        _feature = null;
                    }



                }
                else
                {
                    _feature.Name = txtFeaturesName.Text;
                    _feature.ApplicationId = 1;
                    _feature.Prices.Single(c => c.Type == VersionPrice.VersionType.MainVersion).Price =
                        Convert.ToDecimal(txtMainFeatures.Text);

                    _feature.Prices.Single(c => c.Type == VersionPrice.VersionType.SubVersion).Price =
                        Convert.ToDecimal(txtSubFeatures.Text);

                    _feature.Prices.Single(c => c.Type == VersionPrice.VersionType.AgentMainVersion).Price =
                        Convert.ToDecimal(txtAgentMainFeatures.Text);

                    _feature.Prices.Single(c => c.Type == VersionPrice.VersionType.AgentSubVersion).Price =
                        Convert.ToDecimal(txtAgentSubFeatures.Text);

                    //-----------------------------------------------
                    var client = new HttpClient { BaseAddress = Pv.BaseAddress };
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                    var response = client.PutAsJsonAsync("api/Dongle/UpdateFeatures", _feature).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("تم الحفظ");
                        ClearFeatures();
                    }
                    else
                        MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dgFeathures_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == -1) return;
                _feature = _featureses[e.RowIndex];
                txtFeaturesName.Text = _feature.Name;

                txtSubFeatures.Text = _feature.Prices.Single(c => c.Type == VersionPrice.VersionType.SubVersion)
                    .Price.ToString();

                txtAgentMainFeatures.Text = _feature.Prices.Single(c => c.Type == VersionPrice.VersionType.AgentMainVersion)
                    .Price.ToString();

                txtMainFeatures.Text = _feature.Prices.Single(c => c.Type == VersionPrice.VersionType.MainVersion)
                    .Price.ToString();

                txtAgentSubFeatures.Text = _feature.Prices.Single(c => c.Type == VersionPrice.VersionType.AgentSubVersion)
                    .Price.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }










        private void GetActivitionFeatures(List<int> Features = null)
        {
            try
            {
                if (cmbVersion.SelectedIndex == -1 ||
                    _versions.Count == 0 ||
                    _featureses.Count == 0)
                    return;


                _version = _versions.Single(c => c.Id == (int)cmbVersion.SelectedValue);
                ActivitionFeatures = new List<ViewData>();
                foreach (var feature in _featureses)
                {
                    var viewData = new ViewData
                    {
                        Id = feature.Id,
                        Name = feature.Name,
                        Value = false

                    };
                    if (rdoServer.Checked)
                    {
                        viewData.Price = feature.Prices.Single(c => c.Type == VersionPrice.VersionType.MainVersion)
                            .Price;

                    }
                    else
                    {
                        viewData.Price = feature.Prices.Single(c => c.Type == VersionPrice.VersionType.SubVersion)
                            .Price;

                    }
                    ActivitionFeatures.Add(viewData);
                }
                if (Features != null) {
                    foreach (var a in ActivitionFeatures) {
                        var temp = Features.Where(c => c == a.Id).Count();
                        if (temp > 0) a.Value = true;
                        else a.Value = false;
                    }
                }
                DgActiveDongleFeatures.DataSource = ActivitionFeatures;

                Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmbVersion_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetActivitionFeatures();
        }

        private void rdoServer_CheckedChanged(object sender, EventArgs e)
        {
            //GetActivitionFeatures();
        }

        private void DgActiveDongleFeatures_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == -1) return;
                if (e.ColumnIndex == DgActiveDongleFeatures.Columns[clmChoose.Name].Index)
                {
                    ActivitionFeatures[e.RowIndex].Value = !ActivitionFeatures[e.RowIndex].Value;
                    DgActiveDongleFeatures.AutoGenerateColumns = false;
                    DgActiveDongleFeatures.DataSource = null;
                    DgActiveDongleFeatures.DataSource = ActivitionFeatures;
                    DgActiveDongleFeatures.Refresh();
                    Refresh();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Clear()
        {

            txtCompanyAddress.Text = "";
            txtCompanyJop.Text = "";
            txtCompanyNote.Text = "";
            txtCompanyPhones.Text = "";
            txtCompanyGovernorate.Text = "";
            txtActivitionInfo.Text = "";
            txtCompanyName.Text = "";
            chkMaster.Checked = false;
            chkAutoDeActivate.Checked = false;
        }
        
        private void btnFetchDataAccounts_Click(object sender, EventArgs e)
        {

            var client = new HttpClient { BaseAddress = Pv.BaseAddress };

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);
            var response = client.GetAsync("api/Account/GetUsers").Result;

            if (response.IsSuccessStatusCode)
            {
                var usersList = response.Content.ReadAsAsync<List<UserViewModel>>().Result;

                cmbUser.ValueMember = "Id";
                cmbUser.DisplayMember = "Name";
                cmbUser.DataSource = usersList;
                if (cmbUser.DataSource != null)
                    cmbUser.SelectedIndex = 0;

            }
            else
            {
                MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                if (_isAgent==false && cmbUser.SelectedIndex == -1) return;

                var dtFrom = new DateTime(this.dtFrom.Value.Year, this.dtFrom.Value.Month, this.dtFrom.Value.Day, 0, 0, 0);
                var dtTo = new DateTime(this.dtTo.Value.Year, this.dtTo.Value.Month, this.dtTo.Value.Day, 23, 59, 0);
                var userId = "0";
                if (_isAgent) userId = Pv.User.Id;
                else userId = cmbUser.SelectedValue.ToString();

                var client = new HttpClient { BaseAddress = Pv.BaseAddress };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);



                var response = client.GetAsync("api/Dongle/GetActiveDongleForUser?userId=" + userId
                                        + "&" + "dtFrom=" + dtFrom.ToString("yyyy-MM-dd") + "&dtTo=" + dtTo.ToString("yyyy-MM-dd")).Result;

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<List<ActiveDongle>>().Result;
                    if (result == null)
                    {
                        MessageBox.Show("لا توجد بيانات !");
                        return;
                    }

                    var data = new List<ActiveDongleViewModel>();
                    foreach (var activeDongle in result)
                    {
                        data.Add(new ActiveDongleViewModel
                        {
                            VersionId = activeDongle.VersionId,
                            Id = activeDongle.Id,
                            CompanyAddress = activeDongle.CompanyAddress,
                            IsMain = activeDongle.IsMain,
                            ApplicationId = activeDongle.ApplicationId,
                            CompanyGovernorate = activeDongle.CompanyGovernorate,
                            CompanyJop = activeDongle.CompanyJop,
                            CompanyName = activeDongle.CompanyName,
                            CompanyNote = activeDongle.CompanyNote,
                            CompanyPhones = activeDongle.CompanyPhones,
                            CostPrice = activeDongle.CostPrice,
                            Date = activeDongle.Date,
                            MachineDate = activeDongle.MachineDate,
                            Deleted = activeDongle.Deleted,
                            IsEdited = activeDongle.IsEdited,
                            Discount = activeDongle.Discount,
                            DongleKey = activeDongle.DongleKey,
                            Explained = Pv.DongleActivitionString(new DongleHoldData
                            {
                                IsMain = activeDongle.IsMain,
                                VersionId = activeDongle.VersionId,
                                ApplicationId = activeDongle.ApplicationId,
                                Features = activeDongle.FeatureList.Select(c => c.FeatureId).ToList(),
                            }, activeDongle.StrUpgraded),
                            FeatureList = activeDongle.FeatureList,
                            OfferId = activeDongle.OfferId,
                            SalePrice = activeDongle.SalePrice,
                            UserId = activeDongle.UserId,
                            UserName = activeDongle.UserName,
                            StrCompanyDetails = activeDongle.StrCompanyDetails
                        });
                    }
                    dg1.AutoGenerateColumns = false;
                    dg1.DataSource = data;

                }
                else
                {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Cursor = Cursors.Default;
        }

        private void btnFetchDataAccounts2_Click(object sender, EventArgs e)
        {
            var client = new HttpClient { BaseAddress = Pv.BaseAddress };

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);
            var response = client.GetAsync("api/Account/GetUsers").Result;

            if (response.IsSuccessStatusCode) {
                var usersList = response.Content.ReadAsAsync<List<UserViewModel>>().Result;

                cmbUser2.ValueMember = "Id";
                cmbUser2.DisplayMember = "Name";
                cmbUser2.DataSource = usersList;
                if (cmbUser2.DataSource != null)
                    cmbUser2.SelectedIndex = 0;

            } else {
                MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
        }

        private void btnRefresh2_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                if (_isAgent == false && cmbUser2.SelectedIndex == -1) return;

                var dtFrom = new DateTime(this.dtFrom2.Value.Year, this.dtFrom2.Value.Month, this.dtFrom2.Value.Day);
                var dtTo = new DateTime(this.dtTo2.Value.Year, this.dtTo2.Value.Month, this.dtTo2.Value.Day, 23, 59, 0);
                var userId = "0";
                if (_isAgent) userId = Pv.User.Id;
                else userId = cmbUser2.SelectedValue.ToString();

                var client = new HttpClient { BaseAddress = Pv.BaseAddress };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);



                var response = client.GetAsync("api/Dongle/GetDeletedActiveDongleForUser?userId=" + userId
                                               + "&" + "dtFrom=" + dtFrom.ToString("yyyy-MM-dd") + "&dtTo=" + dtTo.ToString("yyyy-MM-dd")).Result;

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<List<ActiveDongle>>().Result;
                    if (result == null)
                    {
                        MessageBox.Show("لا توجد بيانات !");
                        return;
                    }

                    var data = new List<ActiveDongleViewModel>();
                    foreach (var activeDongle in result)
                    {
                        data.Add(new ActiveDongleViewModel
                        {
                            VersionId = activeDongle.VersionId,
                            Id = activeDongle.Id,
                            CompanyAddress = activeDongle.CompanyAddress,
                            IsMain = activeDongle.IsMain,
                            ApplicationId = activeDongle.ApplicationId,
                            CompanyGovernorate = activeDongle.CompanyGovernorate,
                            CompanyJop = activeDongle.CompanyJop,
                            CompanyName = activeDongle.CompanyName,
                            CompanyNote = activeDongle.CompanyNote,
                            CompanyPhones = activeDongle.CompanyPhones,
                            CostPrice = activeDongle.CostPrice,
                            Date = activeDongle.Date,
                            MachineDate = activeDongle.MachineDate,
                            Deleted = activeDongle.Deleted,
                            Discount = activeDongle.Discount,
                            DongleKey = activeDongle.DongleKey,
                            Explained = Pv.DongleActivitionString(new DongleHoldData
                            {
                                IsMain = activeDongle.IsMain,
                                VersionId = activeDongle.VersionId,
                                ApplicationId = activeDongle.ApplicationId,
                                Features = activeDongle.FeatureList.Select(c => c.FeatureId).ToList(),
                            }, activeDongle.StrUpgraded),
                            FeatureList = activeDongle.FeatureList,
                            OfferId = activeDongle.OfferId,
                            SalePrice = activeDongle.SalePrice,
                            UserId = activeDongle.UserId,
                            UserName = activeDongle.UserName,
                            StrCompanyDetails = activeDongle.StrCompanyDetails
                        });
                    }
                    dg2.AutoGenerateColumns = false;
                    dg2.DataSource = data;

                }
                else
                {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Cursor = Cursors.Default;
        }

        private void btnRestPassword_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtEmail2.Text) ||
               string.IsNullOrEmpty(txtNewConfirme2.Text) ||
                string.IsNullOrEmpty(txtNewPassword2.Text))
            {
                MessageBox.Show("البيانات المدخلة غير صحيحة");
                return;
            }
            if (txtNewConfirme2.Text.Trim() != txtNewPassword2.Text.Trim())
            {
                MessageBox.Show("كلمة المرور غير متطابقة");
                return;
            }
            //-----------------------------------------------
            //Get Code............................
            var client = new HttpClient { BaseAddress = Pv.BaseAddress };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);
            var code = "";

            var response = client.PostAsJsonAsync("api/Account/ForgotPassword", new Pv.ForgotPasswordViewModel()
            {
                Email = txtEmail2.Text
            }).Result;

            if (response.IsSuccessStatusCode)
            {
                code = response.Content.ReadAsAsync<string>().Result;
            }
            else
            {
                MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                return;
            }
            //Reset Password.......................
            client = new HttpClient { BaseAddress = Pv.BaseAddress };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);
            response = client.PostAsJsonAsync("api/Account/ResetPassword",
                  new Pv.ResetPasswordViewModel
                  {
                      Code = code,
                      ConfirmPassword = txtNewConfirme2.Text,
                      Email = txtEmail2.Text,
                      Password = txtNewPassword2.Text
                  }).Result;

            if (response.IsSuccessStatusCode)
            {
                MessageBox.Show("Ok");
            }
            else
            {
                MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);

            }
        }

        private void btnAccountant_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtUserName.Text) ||
                    string.IsNullOrEmpty(txtEMail.Text) ||
                    string.IsNullOrEmpty(txtPassword.Text) ||
                    string.IsNullOrEmpty(txtConfirme.Text))
                {
                    MessageBox.Show("البيانات المدخلة غير صحيحة");
                    return;
                }
                if (txtPassword.Text.Trim() != txtConfirme.Text.Trim())
                {
                    MessageBox.Show("كلمة المرور غير متطابقة");
                    return;
                }
                //-----------------------------------------------
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                var user = new RegisterBindingModel()
                {
                    UserType = UserViewModel.UserType.Accountant,
                    ConfirmPassword = txtConfirme.Text.Trim(),
                    Email = txtEMail.Text.Trim(),
                    Password = txtPassword.Text.Trim(),
                    UserName = txtUserName.Text.Trim()
                };

                var response = client.PostAsJsonAsync("api/Account/RegisterAccountant", user).Result;

                if (response.IsSuccessStatusCode)
                    MessageBox.Show("تم الحفظ");
                else
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            ExportToExcel(dg1);
        }

        private void ExportToExcel(DataGridView gridView)
        {
            // Creating a Excel object.
            Microsoft.Office.Interop.Excel._Application excel = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel._Workbook workbook = excel.Workbooks.Add(Type.Missing);
            Microsoft.Office.Interop.Excel._Worksheet worksheet = null;

            try
            {

                worksheet = workbook.ActiveSheet;

                worksheet.Name = "ExportedFromDatGrid";

                int cellRowIndex = 1;
                int cellColumnIndex = 1;

                //Loop through each row and read value from each column.
                for (int i = -1; i < gridView.Rows.Count ; i++)
                {
                    for (int j = 0; j < gridView.Columns.Count; j++)
                    {
                        // Excel index starts from 1,1. As first Row would have the Column headers, adding a condition check.
                        if (cellRowIndex == 1)
                        {
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = gridView.Columns[j].HeaderText;
                        }
                        else
                        {
                            worksheet.Cells[cellRowIndex, cellColumnIndex] = gridView.Rows[i].Cells[j].FormattedValue.ToString();
                        }
                        cellColumnIndex++;
                    }
                    cellColumnIndex = 1;
                    cellRowIndex++;
                }

                //Getting the location and file name of the excel to save from user.
                SaveFileDialog saveDialog = new SaveFileDialog();
                saveDialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
                saveDialog.FilterIndex = 2;

                if (saveDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    workbook.SaveAs(saveDialog.FileName);
                    MessageBox.Show("Export Successful");
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                excel.Quit();
                workbook = null;
                excel = null;
            }

        }

        private void btnExport2_Click(object sender, EventArgs e)
        {
            ExportToExcel(dg2);
        }

        private void btnFetchDataAccounts3_Click(object sender, EventArgs e)
        {
            var client = new HttpClient { BaseAddress = Pv.BaseAddress };

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);
            var response = client.GetAsync("api/Account/GetUsers").Result;

            if (response.IsSuccessStatusCode)
            {
                var usersList = response.Content.ReadAsAsync<List<UserViewModel>>().Result;

                cmbUser3.ValueMember = "Id";
                cmbUser3.DisplayMember = "Name";
                cmbUser3.DataSource = usersList;
                if (cmbUser3.DataSource != null)
                    cmbUser3.SelectedIndex = 0;

            }
            else
            {
                MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
        }

        private void btnExport3_Click(object sender, EventArgs e)
        {
            ExportToExcel(dg3);
        }

        private void btnRefresh3_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                if (_isAgent == false && cmbUser3.SelectedIndex == -1) return;

                var dtFrom = new DateTime(this.dtFrom3.Value.Year, this.dtFrom3.Value.Month, this.dtFrom3.Value.Day);
                var dtTo = new DateTime(this.dtTo3.Value.Year, this.dtTo3.Value.Month, this.dtTo3.Value.Day, 23, 59, 0);
                var userId = "0";
                if (_isAgent) userId = Pv.User.Id;
                else userId = cmbUser3.SelectedValue.ToString();

                var client = new HttpClient { BaseAddress = Pv.BaseAddress };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);



                var response = client.GetAsync("api/Dongle/GetActiveDongleMix?userId=" + userId
                                               + "&" + "dtFrom=" + dtFrom.ToString("yyyy-MM-dd") + "&dtTo=" + dtTo.ToString("yyyy-MM-dd")).Result;

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<List<ActiveDongle>>().Result;
                    if (result == null)
                    {
                        MessageBox.Show("لا توجد بيانات !");
                        return;
                    }

                    var data = new List<ActiveDongleViewModel>();
                    foreach (var activeDongle in result)
                    {
                        data.Add(new ActiveDongleViewModel
                        {
                            VersionId = activeDongle.VersionId,
                            Id = activeDongle.Id,
                            CompanyAddress = activeDongle.CompanyAddress,
                            IsMain = activeDongle.IsMain,
                            ApplicationId = activeDongle.ApplicationId,
                            CompanyGovernorate = activeDongle.CompanyGovernorate,
                            CompanyJop = activeDongle.CompanyJop,
                            CompanyName = activeDongle.CompanyName,
                            CompanyNote = activeDongle.CompanyNote,
                            CompanyPhones = activeDongle.CompanyPhones,
                            CostPrice = activeDongle.CostPrice,
                            Date = activeDongle.Date,
                            MachineDate = activeDongle.MachineDate,
                            Deleted = activeDongle.Deleted,
                            Discount = activeDongle.Discount,
                            DongleKey = activeDongle.DongleKey,
                            
                            Explained = Pv.DongleActivitionString(new DongleHoldData
                            {
                                IsMain = activeDongle.IsMain,
                                VersionId = activeDongle.VersionId,
                                ApplicationId = activeDongle.ApplicationId,
                                Features = activeDongle.FeatureList.Select(c => c.FeatureId).ToList(),
                            }, activeDongle.StrUpgraded),
                            FeatureList = activeDongle.FeatureList,
                            OfferId = activeDongle.OfferId,
                            SalePrice = activeDongle.SalePrice,
                            UserId = activeDongle.UserId,
                            IsDeleted = activeDongle.IsDeleted,
                            UserName = activeDongle.UserName,
                            StrCompanyDetails = activeDongle.StrCompanyDetails
                        });
                    }
                    dg3.AutoGenerateColumns = false;
                    dg3.DataSource = data;

                }
                else
                {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Cursor = Cursors.Default;
        }

        private void dg3_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == -1) return;
                if (dg3.Columns[clmDongleMovment.Name].Index == e.ColumnIndex)
                {
                    var f = new FrmDongleMovment
                    {
                        DongleKey = dg3[dg3.Columns[clmKey.Name].Index, e.RowIndex].FormattedValue.ToString()
                    };
                    f.ShowDialog();
                    f.Dispose();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void btnExport4_Click(object sender, EventArgs e)
        {
            ExportToExcel(dg4);
        }
        private List<ActiveDongleViewModel> dg4data = new List<ActiveDongleViewModel>();
        private void btnRefresh4_Click(object sender, EventArgs e)
        {
            try
            {
                

                var dtFrom = new DateTime(this.dtFrom4.Value.Year, this.dtFrom4.Value.Month, this.dtFrom4.Value.Day);
                var dtTo = new DateTime(this.dtTo4.Value.Year, this.dtTo4.Value.Month, this.dtTo4.Value.Day, 23, 59, 0);
                
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);



                var response = client.GetAsync("api/Dongle/GetActiveDongleForAllUsers?dtFrom=" + dtFrom.ToString("yyyy-MM-dd") + "&dtTo=" + dtTo.ToString("yyyy-MM-dd")).Result;

                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<List<ActiveDongle>>().Result;
                    if (result == null)
                    {
                        MessageBox.Show("لا توجد بيانات !");
                        return;
                    }

                    dg4data = new List<ActiveDongleViewModel>();
                    foreach (var activeDongle in result)
                    {
                        dg4data.Add(new ActiveDongleViewModel
                        {
                            VersionId = activeDongle.VersionId,
                            Id = activeDongle.Id,
                            CompanyAddress = activeDongle.CompanyAddress,
                            IsMain = activeDongle.IsMain,
                            ApplicationId = activeDongle.ApplicationId,
                            CompanyGovernorate = activeDongle.CompanyGovernorate,
                            CompanyJop = activeDongle.CompanyJop,
                            CompanyName = activeDongle.CompanyName,
                            CompanyNote = activeDongle.CompanyNote,
                            CompanyPhones = activeDongle.CompanyPhones,
                            CostPrice = activeDongle.CostPrice,
                            Date = activeDongle.Date,
                            MachineDate = activeDongle.MachineDate,
                            Deleted = activeDongle.Deleted,
                            Discount = activeDongle.Discount,
                            DongleKey = activeDongle.DongleKey,
                            IsEdited = activeDongle.IsEdited,
                            Explained = Pv.DongleActivitionString(new DongleHoldData
                            {
                                IsMain = activeDongle.IsMain,
                                VersionId = activeDongle.VersionId,
                                ApplicationId = activeDongle.ApplicationId,
                                Features = activeDongle.FeatureList.Select(c => c.FeatureId).ToList(),
                            },  activeDongle.StrUpgraded),
                            FeatureList = activeDongle.FeatureList,
                            OfferId = activeDongle.OfferId,
                            SalePrice = activeDongle.SalePrice,
                            UserId = activeDongle.UserId,
                            IsDeleted = activeDongle.IsDeleted,
                            UserName = activeDongle.UserName,
                            StrCompanyDetails = activeDongle.StrCompanyDetails
                        });
                    }
                    dg4.AutoGenerateColumns = false;
                    dg4.DataSource = dg4data;
                    FilterDg4();
                }
                else
                {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dg4_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex == -1) return;
                if (dg4.Columns[clmDongleMovment2.Name].Index == e.ColumnIndex)
                {
                    var f = new FrmDongleMovment
                    {
                        DongleKey = dg4[dg4.Columns[clmKey2.Name].Index, e.RowIndex].FormattedValue.ToString()
                    };
                    f.ShowDialog();
                    f.Dispose();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
        private void dg1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                var i = e.RowIndex;
                if (i <0) return;
                if (dg1.Columns[clmDongleMovments.Name].Index == e.ColumnIndex)
                {
                    var f = new FrmDongleMovment
                    {
                        DongleKey = ((ActiveDongleViewModel)dg1.Rows[i].DataBoundItem).DongleKey
                    };
                    f.ShowDialog();
                    f.Dispose();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
        private  Activiation.ActiviationInfo obj=new Activiation.ActiviationInfo();

        

        private string _ignoreKey = "";

        private void label48_DoubleClick(object sender, EventArgs e)
        {
            _ignoreKey = Prompt.ShowDialog("Enter the code", "");
        }
        private void FilterDg4() {
            dg4.AutoGenerateColumns = false;
            var s = textBox1.Text.Trim();
            if (string.IsNullOrWhiteSpace(s)) dg4.DataSource = dg4data;
            else {
                dg4.DataSource = dg4data.Where(c => c.CompanyName.Contains(s)).ToList();
            }
        }
        private void textBox1_TextChanged(object sender, EventArgs e) {
            FilterDg4();
            
        }

        private void dg4_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e) {
            var i = e.RowIndex;
            if (i >= dg4data.Count) return;
            var a = dg4data[i];
            if(a.IsEdited)
                dg4.Rows[i].DefaultCellStyle.BackColor = Color.Orange;
        }
        private void dg1_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            var i = e.RowIndex;
            var a = (ActiveDongleViewModel) dg1.Rows[i].DataBoundItem;
            if (a.IsEdited)
                dg1.Rows[i].DefaultCellStyle.BackColor = Color.Orange;
        }
        private void chkAutoDeActivate_CheckedChanged(object sender, EventArgs e) {
            dtpDeActivationDate.Visible = chkAutoDeActivate.Checked;
        }

        private void label19_DoubleClick(object sender, EventArgs e) {
            
            txtMasterPassword.Visible = true;
        }

        private void btnPrint_Click(object sender, EventArgs e) {
            
        }
        public void print(int number) {
            var printerSettings = new PrinterSettings();
            var rptReport = new ContractReport();
            rptReport.txtDate.Value = DateTime.Now.ToString("yyyy/MM/dd");
            rptReport.txtNumber.Value = number.ToString();
            rptReport.txtSecondPerson.Value = txtCompanyName.Text;
            rptReport.txtSecondPerson2.Value = txtCompanyName.Text;
            var s = "يتم تجهيز الطرف الثاني برنامج المربع من إصدارات برامج الطرف الأول وبمبلغ قدره (";
            var s2 = ") و حسب البنود المدرجة أدناه : ";
            var currency = " د.ع";
            if (cmbcurrency.SelectedIndex == 1) currency = " $";

            rptReport.txtPrice.Value = s + txtPrice.Text +currency + s2;
            rptReport.txtAddress.Value = txtCompanyAddress.Text;
            rptReport.txtPhoneNumber.Value = txtCompanyPhones.Text;
            var standardPrintController = new StandardPrintController();
            var reportProcessor = new ReportProcessor();
            reportProcessor.PrintController = standardPrintController;
            ReportProcessor.Print(rptReport, printerSettings);
        }
        private void txtPrice_TextChanged(object sender, EventArgs e) {

        }

        private void txtPrice_KeyDown(object sender, KeyEventArgs e) {
            if(e.KeyCode == Keys.Enter)
                txtPrice.Text = txtPrice.Text.ToDecimal().ToFormattedNumber();
        }

        private void txtPrice_Leave(object sender, EventArgs e) {
            txtPrice.Text = txtPrice.Text.ToDecimal().ToFormattedNumber();
        }

        private void txtMasterPassword_TextChanged(object sender, EventArgs e)
        {
            if (txtMasterPassword.Text == "Morab123@a")
            {
                txtMasterPassword.Text = "";
                txtMasterPassword.Visible = false;
                chkMaster.Visible = true;
            }
        }

        
    }
}
