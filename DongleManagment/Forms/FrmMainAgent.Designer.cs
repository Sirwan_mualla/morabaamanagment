﻿namespace DongleManagment.Forms
{
    partial class FrmMainAgent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnChangePassword = new System.Windows.Forms.Button();
            this.txtNewConfirme = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNewPassword = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtOldPassword = new System.Windows.Forms.TextBox();
            this.btnCheckDongle = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.txtActivitionInfo = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.txtCompanyGovernorate = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCompanyNote = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtCompanyJop = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtCompanyPhones = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtCompanyAddress = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.btnSaveActiveDongle = new System.Windows.Forms.Button();
            this.btnFetchData = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.rdoServer = new System.Windows.Forms.RadioButton();
            this.rdoSub = new System.Windows.Forms.RadioButton();
            this.DgActiveDongleFeatures = new System.Windows.Forms.DataGridView();
            this.clmId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmFeatureName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmChoose = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label20 = new System.Windows.Forms.Label();
            this.cmbVersion = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.btnReadData = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dg1 = new System.Windows.Forms.DataGridView();
            this.clmDg1Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg1MachineDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg1CostPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg1CompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg1Explained = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnReActiveDongle = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dg2 = new System.Windows.Forms.DataGridView();
            this.clmDg2Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg2MachineDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg2CostPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg2CompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDg2Explained = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.btnRefresh2 = new System.Windows.Forms.Button();
            this.dtTo2 = new System.Windows.Forms.DateTimePicker();
            this.dtFrom2 = new System.Windows.Forms.DateTimePicker();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.txtOldVersionDetails = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.lblOldVersionKey = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.lblOldVersionState = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.btnRefrish = new System.Windows.Forms.Button();
            this.checkUpgrad = new System.Windows.Forms.CheckBox();
            this.groupBox2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgActiveDongleFeatures)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg1)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg2)).BeginInit();
            this.groupBox13.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.btnChangePassword);
            this.groupBox2.Controls.Add(this.txtNewConfirme);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtNewPassword);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtOldPassword);
            this.groupBox2.Location = new System.Drawing.Point(636, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(348, 128);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "تغيير كلمة المرور";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(225, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "تأكيد كلمة المرور الجديدة";
            // 
            // btnChangePassword
            // 
            this.btnChangePassword.Location = new System.Drawing.Point(144, 97);
            this.btnChangePassword.Name = "btnChangePassword";
            this.btnChangePassword.Size = new System.Drawing.Size(75, 23);
            this.btnChangePassword.TabIndex = 2;
            this.btnChangePassword.Text = "حفظ";
            this.btnChangePassword.UseVisualStyleBackColor = true;
            this.btnChangePassword.Click += new System.EventHandler(this.btnChangePassword_Click);
            // 
            // txtNewConfirme
            // 
            this.txtNewConfirme.Location = new System.Drawing.Point(6, 71);
            this.txtNewConfirme.Name = "txtNewConfirme";
            this.txtNewConfirme.PasswordChar = '*';
            this.txtNewConfirme.Size = new System.Drawing.Size(213, 20);
            this.txtNewConfirme.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(248, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "كلمة المرور الجديدة";
            // 
            // txtNewPassword
            // 
            this.txtNewPassword.Location = new System.Drawing.Point(6, 45);
            this.txtNewPassword.Name = "txtNewPassword";
            this.txtNewPassword.PasswordChar = '*';
            this.txtNewPassword.Size = new System.Drawing.Size(213, 20);
            this.txtNewPassword.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(248, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "كلمة المرور القديمة";
            // 
            // txtOldPassword
            // 
            this.txtOldPassword.Location = new System.Drawing.Point(6, 19);
            this.txtOldPassword.Name = "txtOldPassword";
            this.txtOldPassword.PasswordChar = '*';
            this.txtOldPassword.Size = new System.Drawing.Size(213, 20);
            this.txtOldPassword.TabIndex = 7;
            // 
            // btnCheckDongle
            // 
            this.btnCheckDongle.Location = new System.Drawing.Point(554, 10);
            this.btnCheckDongle.Name = "btnCheckDongle";
            this.btnCheckDongle.Size = new System.Drawing.Size(75, 23);
            this.btnCheckDongle.TabIndex = 7;
            this.btnCheckDongle.Text = "فحص الدنكل";
            this.btnCheckDongle.UseVisualStyleBackColor = true;
            this.btnCheckDongle.Click += new System.EventHandler(this.btnCheckDongle_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.groupBox13);
            this.groupBox8.Controls.Add(this.btnUpdate);
            this.groupBox8.Controls.Add(this.groupBox11);
            this.groupBox8.Controls.Add(this.groupBox10);
            this.groupBox8.Controls.Add(this.lblAmount);
            this.groupBox8.Controls.Add(this.label21);
            this.groupBox8.Controls.Add(this.btnSaveActiveDongle);
            this.groupBox8.Controls.Add(this.btnFetchData);
            this.groupBox8.Controls.Add(this.groupBox9);
            this.groupBox8.Controls.Add(this.DgActiveDongleFeatures);
            this.groupBox8.Controls.Add(this.label20);
            this.groupBox8.Controls.Add(this.cmbVersion);
            this.groupBox8.Controls.Add(this.label19);
            this.groupBox8.Location = new System.Drawing.Point(32, 147);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(952, 475);
            this.groupBox8.TabIndex = 8;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "تنشيط الدنكل";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(641, 423);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 17;
            this.btnUpdate.Text = "تعديل";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.txtActivitionInfo);
            this.groupBox11.Controls.Add(this.label28);
            this.groupBox11.Location = new System.Drawing.Point(6, 19);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(510, 82);
            this.groupBox11.TabIndex = 16;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "بيانات الدنكل";
            // 
            // txtActivitionInfo
            // 
            this.txtActivitionInfo.Enabled = false;
            this.txtActivitionInfo.Location = new System.Drawing.Point(48, 16);
            this.txtActivitionInfo.Multiline = true;
            this.txtActivitionInfo.Name = "txtActivitionInfo";
            this.txtActivitionInfo.Size = new System.Drawing.Size(371, 53);
            this.txtActivitionInfo.TabIndex = 11;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(425, 19);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(69, 13);
            this.label28.TabIndex = 10;
            this.label28.Text = "بيانات التفعيل";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.txtCompanyGovernorate);
            this.groupBox10.Controls.Add(this.label1);
            this.groupBox10.Controls.Add(this.txtCompanyNote);
            this.groupBox10.Controls.Add(this.label26);
            this.groupBox10.Controls.Add(this.txtCompanyJop);
            this.groupBox10.Controls.Add(this.label25);
            this.groupBox10.Controls.Add(this.txtCompanyPhones);
            this.groupBox10.Controls.Add(this.label24);
            this.groupBox10.Controls.Add(this.txtCompanyAddress);
            this.groupBox10.Controls.Add(this.label23);
            this.groupBox10.Controls.Add(this.txtCompanyName);
            this.groupBox10.Controls.Add(this.label22);
            this.groupBox10.Location = new System.Drawing.Point(6, 104);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(510, 365);
            this.groupBox10.TabIndex = 15;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "بيانات الشركة";
            // 
            // txtCompanyGovernorate
            // 
            this.txtCompanyGovernorate.Location = new System.Drawing.Point(48, 144);
            this.txtCompanyGovernorate.Name = "txtCompanyGovernorate";
            this.txtCompanyGovernorate.Size = new System.Drawing.Size(371, 20);
            this.txtCompanyGovernorate.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(425, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "المحافظة";
            // 
            // txtCompanyNote
            // 
            this.txtCompanyNote.Location = new System.Drawing.Point(48, 309);
            this.txtCompanyNote.Multiline = true;
            this.txtCompanyNote.Name = "txtCompanyNote";
            this.txtCompanyNote.Size = new System.Drawing.Size(371, 50);
            this.txtCompanyNote.TabIndex = 9;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(425, 312);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(55, 13);
            this.label26.TabIndex = 8;
            this.label26.Text = "الملاحظات";
            // 
            // txtCompanyJop
            // 
            this.txtCompanyJop.Location = new System.Drawing.Point(48, 196);
            this.txtCompanyJop.Multiline = true;
            this.txtCompanyJop.Name = "txtCompanyJop";
            this.txtCompanyJop.Size = new System.Drawing.Size(371, 107);
            this.txtCompanyJop.TabIndex = 7;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(425, 199);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(66, 13);
            this.label25.TabIndex = 6;
            this.label25.Text = "عمل الشركة";
            // 
            // txtCompanyPhones
            // 
            this.txtCompanyPhones.Location = new System.Drawing.Point(48, 170);
            this.txtCompanyPhones.Name = "txtCompanyPhones";
            this.txtCompanyPhones.Size = new System.Drawing.Size(371, 20);
            this.txtCompanyPhones.TabIndex = 5;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(425, 173);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 13);
            this.label24.TabIndex = 4;
            this.label24.Text = "رقم الهاتف";
            // 
            // txtCompanyAddress
            // 
            this.txtCompanyAddress.Location = new System.Drawing.Point(48, 57);
            this.txtCompanyAddress.Multiline = true;
            this.txtCompanyAddress.Name = "txtCompanyAddress";
            this.txtCompanyAddress.Size = new System.Drawing.Size(371, 79);
            this.txtCompanyAddress.TabIndex = 3;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(425, 60);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(38, 13);
            this.label23.TabIndex = 2;
            this.label23.Text = "العنوان";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new System.Drawing.Point(48, 31);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(371, 20);
            this.txtCompanyName.TabIndex = 1;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(425, 34);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(66, 13);
            this.label22.TabIndex = 0;
            this.label22.Text = "أسم الشركة";
            // 
            // lblAmount
            // 
            this.lblAmount.Location = new System.Drawing.Point(735, 454);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(84, 18);
            this.lblAmount.TabIndex = 13;
            this.lblAmount.Text = "0";
            this.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(825, 458);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(46, 13);
            this.label21.TabIndex = 12;
            this.label21.Text = "المجموع";
            // 
            // btnSaveActiveDongle
            // 
            this.btnSaveActiveDongle.Location = new System.Drawing.Point(722, 423);
            this.btnSaveActiveDongle.Name = "btnSaveActiveDongle";
            this.btnSaveActiveDongle.Size = new System.Drawing.Size(75, 23);
            this.btnSaveActiveDongle.TabIndex = 11;
            this.btnSaveActiveDongle.Text = "حفظ";
            this.btnSaveActiveDongle.UseVisualStyleBackColor = true;
            this.btnSaveActiveDongle.Click += new System.EventHandler(this.btnSaveActiveDongle_Click);
            // 
            // btnFetchData
            // 
            this.btnFetchData.Location = new System.Drawing.Point(803, 423);
            this.btnFetchData.Name = "btnFetchData";
            this.btnFetchData.Size = new System.Drawing.Size(75, 23);
            this.btnFetchData.TabIndex = 10;
            this.btnFetchData.Text = "جلب البيانات";
            this.btnFetchData.UseVisualStyleBackColor = true;
            this.btnFetchData.Click += new System.EventHandler(this.btnFetchData_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.rdoServer);
            this.groupBox9.Controls.Add(this.rdoSub);
            this.groupBox9.Location = new System.Drawing.Point(803, 19);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(143, 47);
            this.groupBox9.TabIndex = 8;
            this.groupBox9.TabStop = false;
            // 
            // rdoServer
            // 
            this.rdoServer.AccessibleName = "";
            this.rdoServer.AutoSize = true;
            this.rdoServer.Checked = true;
            this.rdoServer.Location = new System.Drawing.Point(73, 19);
            this.rdoServer.Name = "rdoServer";
            this.rdoServer.Size = new System.Drawing.Size(55, 17);
            this.rdoServer.TabIndex = 4;
            this.rdoServer.TabStop = true;
            this.rdoServer.Text = "سيرفر";
            this.rdoServer.UseVisualStyleBackColor = true;
            this.rdoServer.CheckedChanged += new System.EventHandler(this.rdoServer_CheckedChanged);
            // 
            // rdoSub
            // 
            this.rdoSub.AccessibleName = "";
            this.rdoSub.AutoSize = true;
            this.rdoSub.Location = new System.Drawing.Point(9, 19);
            this.rdoSub.Name = "rdoSub";
            this.rdoSub.Size = new System.Drawing.Size(56, 17);
            this.rdoSub.TabIndex = 5;
            this.rdoSub.Text = "شبكية";
            this.rdoSub.UseVisualStyleBackColor = true;
            // 
            // DgActiveDongleFeatures
            // 
            this.DgActiveDongleFeatures.AllowUserToAddRows = false;
            this.DgActiveDongleFeatures.AllowUserToDeleteRows = false;
            this.DgActiveDongleFeatures.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DgActiveDongleFeatures.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmId,
            this.clmFeatureName,
            this.clmPrice,
            this.clmChoose});
            this.DgActiveDongleFeatures.Location = new System.Drawing.Point(522, 217);
            this.DgActiveDongleFeatures.Name = "DgActiveDongleFeatures";
            this.DgActiveDongleFeatures.ReadOnly = true;
            this.DgActiveDongleFeatures.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgActiveDongleFeatures.Size = new System.Drawing.Size(356, 200);
            this.DgActiveDongleFeatures.TabIndex = 3;
            this.DgActiveDongleFeatures.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgActiveDongleFeatures_CellClick);
            // 
            // clmId
            // 
            this.clmId.DataPropertyName = "Id";
            this.clmId.HeaderText = "id";
            this.clmId.Name = "clmId";
            this.clmId.ReadOnly = true;
            this.clmId.Visible = false;
            // 
            // clmFeatureName
            // 
            this.clmFeatureName.DataPropertyName = "Name";
            this.clmFeatureName.HeaderText = "الاسم";
            this.clmFeatureName.Name = "clmFeatureName";
            this.clmFeatureName.ReadOnly = true;
            // 
            // clmPrice
            // 
            this.clmPrice.DataPropertyName = "Price";
            this.clmPrice.HeaderText = "السعر";
            this.clmPrice.Name = "clmPrice";
            this.clmPrice.ReadOnly = true;
            // 
            // clmChoose
            // 
            this.clmChoose.DataPropertyName = "Value";
            this.clmChoose.HeaderText = "أختيار";
            this.clmChoose.Name = "clmChoose";
            this.clmChoose.ReadOnly = true;
            this.clmChoose.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clmChoose.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(884, 217);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(46, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "الاضافات";
            // 
            // cmbVersion
            // 
            this.cmbVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVersion.FormattingEnabled = true;
            this.cmbVersion.Location = new System.Drawing.Point(757, 185);
            this.cmbVersion.Name = "cmbVersion";
            this.cmbVersion.Size = new System.Drawing.Size(121, 21);
            this.cmbVersion.TabIndex = 1;
            this.cmbVersion.SelectedIndexChanged += new System.EventHandler(this.cmbVersion_SelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(884, 188);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "نوع النسخة";
            // 
            // btnReadData
            // 
            this.btnReadData.Location = new System.Drawing.Point(555, 39);
            this.btnReadData.Name = "btnReadData";
            this.btnReadData.Size = new System.Drawing.Size(75, 42);
            this.btnReadData.TabIndex = 9;
            this.btnReadData.Text = "قراءة البيانات / دنكل";
            this.btnReadData.UseVisualStyleBackColor = true;
            this.btnReadData.Click += new System.EventHandler(this.btnReadData_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(554, 88);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 42);
            this.button1.TabIndex = 10;
            this.button1.Text = "قراءة البيانات / سيرفر";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1000, 654);
            this.tabControl1.TabIndex = 11;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dg1);
            this.tabPage2.Controls.Add(this.label31);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.btnRefresh);
            this.tabPage2.Controls.Add(this.dtTo);
            this.tabPage2.Controls.Add(this.dtFrom);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(992, 628);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "تقرير المبيعات";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dg1
            // 
            this.dg1.AllowUserToAddRows = false;
            this.dg1.AllowUserToDeleteRows = false;
            this.dg1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmDg1Id,
            this.clmDg1MachineDate,
            this.clmDg1CostPrice,
            this.clmDg1CompanyName,
            this.clmDg1Explained});
            this.dg1.Location = new System.Drawing.Point(8, 104);
            this.dg1.Name = "dg1";
            this.dg1.ReadOnly = true;
            this.dg1.Size = new System.Drawing.Size(978, 516);
            this.dg1.TabIndex = 17;
            // 
            // clmDg1Id
            // 
            this.clmDg1Id.DataPropertyName = "Id";
            this.clmDg1Id.HeaderText = "رقم العملية";
            this.clmDg1Id.Name = "clmDg1Id";
            this.clmDg1Id.ReadOnly = true;
            // 
            // clmDg1MachineDate
            // 
            this.clmDg1MachineDate.DataPropertyName = "MachineDate";
            this.clmDg1MachineDate.HeaderText = "التاريخ";
            this.clmDg1MachineDate.Name = "clmDg1MachineDate";
            this.clmDg1MachineDate.ReadOnly = true;
            // 
            // clmDg1CostPrice
            // 
            this.clmDg1CostPrice.DataPropertyName = "CostPrice";
            this.clmDg1CostPrice.HeaderText = "السعر";
            this.clmDg1CostPrice.Name = "clmDg1CostPrice";
            this.clmDg1CostPrice.ReadOnly = true;
            // 
            // clmDg1CompanyName
            // 
            this.clmDg1CompanyName.DataPropertyName = "CompanyName";
            this.clmDg1CompanyName.HeaderText = "اسم الشركة";
            this.clmDg1CompanyName.Name = "clmDg1CompanyName";
            this.clmDg1CompanyName.ReadOnly = true;
            // 
            // clmDg1Explained
            // 
            this.clmDg1Explained.DataPropertyName = "Explained";
            this.clmDg1Explained.HeaderText = "التفاصيل";
            this.clmDg1Explained.Name = "clmDg1Explained";
            this.clmDg1Explained.ReadOnly = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(116, 47);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(48, 13);
            this.label31.TabIndex = 16;
            this.label31.Text = "الى تاريخ";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(116, 21);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(45, 13);
            this.label30.TabIndex = 15;
            this.label30.Text = "من تاريخ";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(8, 70);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(102, 23);
            this.btnRefresh.TabIndex = 14;
            this.btnRefresh.Text = "تحديث البيانات";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTo.Location = new System.Drawing.Point(8, 44);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(102, 20);
            this.dtTo.TabIndex = 13;
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFrom.Location = new System.Drawing.Point(8, 18);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(102, 20);
            this.dtFrom.TabIndex = 12;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnReActiveDongle);
            this.tabPage1.Controls.Add(this.groupBox8);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.btnReadData);
            this.tabPage1.Controls.Add(this.btnCheckDongle);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(992, 628);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "الرئيسية";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnReActiveDongle
            // 
            this.btnReActiveDongle.Location = new System.Drawing.Point(473, 10);
            this.btnReActiveDongle.Name = "btnReActiveDongle";
            this.btnReActiveDongle.Size = new System.Drawing.Size(75, 23);
            this.btnReActiveDongle.TabIndex = 11;
            this.btnReActiveDongle.Text = "أعادة تنشيط الدنكل";
            this.btnReActiveDongle.UseVisualStyleBackColor = true;
            this.btnReActiveDongle.Click += new System.EventHandler(this.btnReActiveDongle_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dg2);
            this.tabPage3.Controls.Add(this.label32);
            this.tabPage3.Controls.Add(this.label33);
            this.tabPage3.Controls.Add(this.btnRefresh2);
            this.tabPage3.Controls.Add(this.dtTo2);
            this.tabPage3.Controls.Add(this.dtFrom2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(992, 628);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "تقرير المحذوفات";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dg2
            // 
            this.dg2.AllowUserToAddRows = false;
            this.dg2.AllowUserToDeleteRows = false;
            this.dg2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmDg2Id,
            this.clmDg2MachineDate,
            this.clmDg2CostPrice,
            this.clmDg2CompanyName,
            this.clmDg2Explained});
            this.dg2.Location = new System.Drawing.Point(8, 102);
            this.dg2.Name = "dg2";
            this.dg2.ReadOnly = true;
            this.dg2.Size = new System.Drawing.Size(976, 518);
            this.dg2.TabIndex = 26;
            // 
            // clmDg2Id
            // 
            this.clmDg2Id.DataPropertyName = "Id";
            this.clmDg2Id.HeaderText = "رقم العملية";
            this.clmDg2Id.Name = "clmDg2Id";
            this.clmDg2Id.ReadOnly = true;
            // 
            // clmDg2MachineDate
            // 
            this.clmDg2MachineDate.DataPropertyName = "MachineDate";
            this.clmDg2MachineDate.HeaderText = "التاريخ";
            this.clmDg2MachineDate.Name = "clmDg2MachineDate";
            this.clmDg2MachineDate.ReadOnly = true;
            // 
            // clmDg2CostPrice
            // 
            this.clmDg2CostPrice.DataPropertyName = "CostPrice";
            this.clmDg2CostPrice.HeaderText = "السعر";
            this.clmDg2CostPrice.Name = "clmDg2CostPrice";
            this.clmDg2CostPrice.ReadOnly = true;
            // 
            // clmDg2CompanyName
            // 
            this.clmDg2CompanyName.DataPropertyName = "CompanyName";
            this.clmDg2CompanyName.HeaderText = "اسم الشركة";
            this.clmDg2CompanyName.Name = "clmDg2CompanyName";
            this.clmDg2CompanyName.ReadOnly = true;
            // 
            // clmDg2Explained
            // 
            this.clmDg2Explained.DataPropertyName = "Explained";
            this.clmDg2Explained.HeaderText = "التفاصيل";
            this.clmDg2Explained.Name = "clmDg2Explained";
            this.clmDg2Explained.ReadOnly = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(116, 45);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(48, 13);
            this.label32.TabIndex = 25;
            this.label32.Text = "الى تاريخ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(116, 19);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(45, 13);
            this.label33.TabIndex = 24;
            this.label33.Text = "من تاريخ";
            // 
            // btnRefresh2
            // 
            this.btnRefresh2.Location = new System.Drawing.Point(8, 68);
            this.btnRefresh2.Name = "btnRefresh2";
            this.btnRefresh2.Size = new System.Drawing.Size(102, 23);
            this.btnRefresh2.TabIndex = 23;
            this.btnRefresh2.Text = "تحديث البيانات";
            this.btnRefresh2.UseVisualStyleBackColor = true;
            this.btnRefresh2.Click += new System.EventHandler(this.btnRefresh2_Click);
            // 
            // dtTo2
            // 
            this.dtTo2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTo2.Location = new System.Drawing.Point(8, 42);
            this.dtTo2.Name = "dtTo2";
            this.dtTo2.Size = new System.Drawing.Size(102, 20);
            this.dtTo2.TabIndex = 22;
            // 
            // dtFrom2
            // 
            this.dtFrom2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFrom2.Location = new System.Drawing.Point(8, 16);
            this.dtFrom2.Name = "dtFrom2";
            this.dtFrom2.Size = new System.Drawing.Size(102, 20);
            this.dtFrom2.TabIndex = 21;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.txtOldVersionDetails);
            this.groupBox13.Controls.Add(this.label48);
            this.groupBox13.Controls.Add(this.lblOldVersionKey);
            this.groupBox13.Controls.Add(this.label46);
            this.groupBox13.Controls.Add(this.lblOldVersionState);
            this.groupBox13.Controls.Add(this.label43);
            this.groupBox13.Controls.Add(this.btnRefrish);
            this.groupBox13.Controls.Add(this.checkUpgrad);
            this.groupBox13.Location = new System.Drawing.Point(523, 72);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(424, 105);
            this.groupBox13.TabIndex = 18;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "بيانات الترقية";
            // 
            // txtOldVersionDetails
            // 
            this.txtOldVersionDetails.Enabled = false;
            this.txtOldVersionDetails.Location = new System.Drawing.Point(6, 52);
            this.txtOldVersionDetails.Multiline = true;
            this.txtOldVersionDetails.Name = "txtOldVersionDetails";
            this.txtOldVersionDetails.Size = new System.Drawing.Size(135, 46);
            this.txtOldVersionDetails.TabIndex = 23;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(147, 52);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(70, 13);
            this.label48.TabIndex = 22;
            this.label48.Text = "بيانات النسخة";
            this.label48.DoubleClick += new System.EventHandler(this.label48_DoubleClick);
            // 
            // lblOldVersionKey
            // 
            this.lblOldVersionKey.Location = new System.Drawing.Point(32, 25);
            this.lblOldVersionKey.Name = "lblOldVersionKey";
            this.lblOldVersionKey.Size = new System.Drawing.Size(111, 13);
            this.lblOldVersionKey.TabIndex = 21;
            this.lblOldVersionKey.Text = "-----------------------";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(147, 25);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(59, 13);
            this.label46.TabIndex = 20;
            this.label46.Text = "كود التفعيل";
            // 
            // lblOldVersionState
            // 
            this.lblOldVersionState.Location = new System.Drawing.Point(238, 52);
            this.lblOldVersionState.Name = "lblOldVersionState";
            this.lblOldVersionState.Size = new System.Drawing.Size(111, 13);
            this.lblOldVersionState.TabIndex = 19;
            this.lblOldVersionState.Text = "-----------------------";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(354, 52);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(64, 13);
            this.label43.TabIndex = 18;
            this.label43.Text = "حالة النسخة";
            // 
            // btnRefrish
            // 
            this.btnRefrish.Location = new System.Drawing.Point(344, 75);
            this.btnRefrish.Name = "btnRefrish";
            this.btnRefrish.Size = new System.Drawing.Size(75, 23);
            this.btnRefrish.TabIndex = 17;
            this.btnRefrish.Text = "تحديث";
            this.btnRefrish.UseVisualStyleBackColor = true;
            this.btnRefrish.Click += new System.EventHandler(this.btnRefrish_Click);
            // 
            // checkUpgrad
            // 
            this.checkUpgrad.AutoSize = true;
            this.checkUpgrad.Location = new System.Drawing.Point(366, 25);
            this.checkUpgrad.Name = "checkUpgrad";
            this.checkUpgrad.Size = new System.Drawing.Size(49, 17);
            this.checkUpgrad.TabIndex = 0;
            this.checkUpgrad.Text = "ترقية";
            this.checkUpgrad.UseVisualStyleBackColor = true;
            // 
            // FrmMainAgent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1000, 654);
            this.Controls.Add(this.tabControl1);
            this.Name = "FrmMainAgent";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "الواجهه الرئيسية";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgActiveDongleFeatures)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg1)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg2)).EndInit();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnChangePassword;
        private System.Windows.Forms.TextBox txtNewConfirme;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNewPassword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtOldPassword;
        private System.Windows.Forms.Button btnCheckDongle;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btnSaveActiveDongle;
        private System.Windows.Forms.Button btnFetchData;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.RadioButton rdoServer;
        private System.Windows.Forms.RadioButton rdoSub;
        private System.Windows.Forms.DataGridView DgActiveDongleFeatures;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmId;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmFeatureName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmPrice;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmChoose;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cmbVersion;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox txtCompanyNote;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtCompanyJop;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtCompanyPhones;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtCompanyAddress;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtCompanyGovernorate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnReadData;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox txtActivitionInfo;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dg1;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg1Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg1MachineDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg1CostPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg1CompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg1Explained;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.DataGridView dg2;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg2Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg2MachineDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg2CostPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg2CompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDg2Explained;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button btnRefresh2;
        private System.Windows.Forms.DateTimePicker dtTo2;
        private System.Windows.Forms.DateTimePicker dtFrom2;
        private System.Windows.Forms.Button btnReActiveDongle;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox txtOldVersionDetails;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label lblOldVersionKey;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label lblOldVersionState;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button btnRefrish;
        private System.Windows.Forms.CheckBox checkUpgrad;
    }
}