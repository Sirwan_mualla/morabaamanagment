﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DongleManagment.Models;
using DongleManagment.ProjectClasses;
using Morabaa6;
using Newtonsoft.Json;
using Version = DongleManagment.Models.Version;

namespace DongleManagment.Forms
{
   public partial class FrmMainAgent : Form
   {
      private List<Version> _versions = new List<Version>();
      private List<Features> _featureses = new List<Features>();
      private Version _version;
      private Features _feature;
      private List<FrmMain.ViewData> ActivitionFeatures = new List<FrmMain.ViewData>();
      public FrmMainAgent()
      {
         InitializeComponent();
      }

      private void btnChangePassword_Click(object sender,EventArgs e)
      {
         try
         {
            if(string.IsNullOrEmpty(txtOldPassword.Text) ||
                string.IsNullOrEmpty(txtNewConfirme.Text) ||
                string.IsNullOrEmpty(txtNewPassword.Text))
            {
               MessageBox.Show("البيانات المدخلة غير صحيحة");
               return;
            }
            if(txtNewConfirme.Text.Trim() != txtNewPassword.Text.Trim())
            {
               MessageBox.Show("كلمة المرور غير متطابقة");
               return;
            }
            //-----------------------------------------------
            var client = new HttpClient { BaseAddress = Pv.BaseAddress };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",Pv.User.Token);

            var newPasswoed = new ChangePasswordBindingModel()
            {
               ConfirmPassword = txtNewConfirme.Text.Trim(),
               NewPassword = txtNewPassword.Text.Trim(),
               OldPassword = txtOldPassword.Text.Trim()

            };

            var response = client.PostAsJsonAsync("api/Account/ChangePassword",newPasswoed).Result;

            if(response.IsSuccessStatusCode)
               MessageBox.Show("تم الحفظ");

            else
               MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
         }
         catch(Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }

      private void btnCheckDongle_Click(object sender,EventArgs e)
      {
         try
         {
            var client = new HttpClient { BaseAddress = Pv.BaseAddress };

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",Pv.User.Token);

            var dongleKey = Morabaa7.MyClass.GetId();
            if(dongleKey == "0")
            {
               MessageBox.Show("Error no Dongle!");
               return;
            }

            var response = client.GetAsync("api/Dongle/CheckDongle?dongleKey=" + dongleKey).Result;

            if(response.IsSuccessStatusCode)
            {
               var result = response.Content.ReadAsAsync<Dongle.DongleResult>().Result;
               if(result == Dongle.DongleResult.Ok)
                  MessageBox.Show("مفعل....");
               else if(result == Dongle.DongleResult.NotFound)
                  MessageBox.Show("غير موجود");
               else if(result == Dongle.DongleResult.Error)
                  MessageBox.Show("حدث خطأ");
               else if(result == Dongle.DongleResult.NotActive)
                  MessageBox.Show("غير مفعل......");




            }
            else
            {
               MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
         }
         catch(Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }
      private void GetActivitionFeatures()
      {
         try
         {
            if(cmbVersion.SelectedIndex == -1 ||
                _versions.Count == 0 ||
                _featureses.Count == 0)
               return;


            _version = _versions.Single(c => c.Id == (int)cmbVersion.SelectedValue);
            ActivitionFeatures = new List<FrmMain.ViewData>();
            foreach(var feature in _featureses)
            {
               var viewData = new FrmMain.ViewData
               {
                  Id = feature.Id,
                  Name = feature.Name,
                  Value = false

               };
               if(rdoServer.Checked)
               {
                  viewData.Price = feature.Prices.Single(c => c.Type == VersionPrice.VersionType.AgentMainVersion)
                      .Price;

               }
               else
               {
                  viewData.Price = feature.Prices.Single(c => c.Type == VersionPrice.VersionType.AgentSubVersion)
                      .Price;

               }
               ActivitionFeatures.Add(viewData);
            }
            DgActiveDongleFeatures.DataSource = ActivitionFeatures;
            Refresh();
         }
         catch(Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }
      private void rdoServer_CheckedChanged(object sender,EventArgs e)
      {
         GetActivitionFeatures();
      }

      private void cmbVersion_SelectedIndexChanged(object sender,EventArgs e)
      {
         GetActivitionFeatures();
      }

      private void DgActiveDongleFeatures_CellClick(object sender,DataGridViewCellEventArgs e)
      {
         try
         {
            if(e.RowIndex == -1) return;
            if(e.ColumnIndex == DgActiveDongleFeatures.Columns[clmChoose.Name].Index)
               if(e.ColumnIndex == DgActiveDongleFeatures.Columns[clmChoose.Name].Index)
               {
                  ActivitionFeatures[e.RowIndex].Value = !ActivitionFeatures[e.RowIndex].Value;
                  DgActiveDongleFeatures.AutoGenerateColumns = false;
                  DgActiveDongleFeatures.DataSource = null;
                  DgActiveDongleFeatures.DataSource = ActivitionFeatures;
                  DgActiveDongleFeatures.Refresh();
                  Refresh();
               }
         }
         catch(Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }

      private void btnFetchData_Click(object sender,EventArgs e)
      {
         try
         {
            //جلب انواع النسخ
            var client = new HttpClient { BaseAddress = Pv.BaseAddress };

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",Pv.User.Token);
            var response = client.GetAsync("api/Dongle/Versions").Result;

            if(response.IsSuccessStatusCode)
            {
               _versions = response.Content.ReadAsAsync<List<Version>>().Result;

               cmbVersion.ValueMember = "Id";
               cmbVersion.DisplayMember = "Name";
               cmbVersion.DataSource = _versions;
               cmbVersion.DataSource = _versions;
               if(cmbVersion.DataSource != null)
                  cmbVersion.SelectedIndex = 0;

            }
            else
            {
               MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }

            //جلب الاضافات
            client = new HttpClient { BaseAddress = Pv.BaseAddress };

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",Pv.User.Token);
            response = client.GetAsync("api/Dongle/Features").Result;

            if(response.IsSuccessStatusCode)
            {
               _featureses = response.Content.ReadAsAsync<List<Features>>().Result;


            }
            else
            {
               MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }


         }
         catch(Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }

      private void Refresh()
      {
         try
         {

            if(cmbVersion.SelectedIndex == -1 ||
                _versions.Count == 0 ||
                _featureses.Count == 0)
               return;
            decimal sum = 0;
            if(rdoServer.Checked)
            {
               sum = _version.Prices.Single(c => c.Type == VersionPrice.VersionType.AgentMainVersion)
                   .Price;

            }
            else
            {
               sum = _version.Prices.Single(c => c.Type == VersionPrice.VersionType.AgentSubVersion)
                   .Price;

            }
            foreach(var data in ActivitionFeatures)
            {
               if(data.Value)
                  sum += data.Price;
            }

            lblAmount.Text = sum.ToString();
         }
         catch(Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }

      private void btnSaveActiveDongle_Click(object sender,EventArgs e)
      {
            #region MyRegion

            //try
            //{
            //   var client = new HttpClient { BaseAddress = Pv.BaseAddress };
            //   client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //   client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",Pv.User.Token);

            //   var activeDongleFeatures = new List<ActiveDongleFeature>();
            //   foreach(var feature in ActivitionFeatures)
            //   {
            //      activeDongleFeatures.Add(new ActiveDongleFeature
            //      {
            //         FeatureId = feature.Id
            //      });
            //   }

            //   var dongleKey = Morabaa7.MyClass.GetId();
            //   if(dongleKey == "0")
            //   {
            //      MessageBox.Show("Error no Dongle!");
            //      return;
            //   }


            //   var activition = new ActiveDongle
            //   {
            //      CostPrice = Convert.ToDecimal(lblAmount.Text),
            //      Date = DateTime.Now,
            //      Deleted = false,
            //      Discount = 0,
            //      SalePrice = 0,
            //      DongleKey = dongleKey,
            //      UserId = Pv.User.Id,
            //      VersionId = _version.Id,
            //      OfferId = 0,
            //      Id = 0,
            //      FeatureList = activeDongleFeatures,
            //      ApplicationId = 1,
            //      CompanyAddress = txtCompanyAddress.Text,
            //      CompanyJop = txtCompanyJop.Text,
            //      CompanyName = txtCompanyName.Text,
            //      CompanyNote = txtCompanyNote.Text,
            //      CompanyPhones = txtCompanyPhones.Text,
            //      CompanyGovernorate = txtCompanyGovernorate.Text,
            //      IsMain = rdoServer.Checked
            //   };





            //   var response = client.PostAsJsonAsync("api/Dongle/ActiveDongle",activition).Result;

            //   if(response.IsSuccessStatusCode)
            //   {
            //      var dongle = response.Content.ReadAsAsync<Dongle>().Result;
            //      if(dongle == null)
            //         MessageBox.Show("حدث خطأ....");
            //      else
            //      {
            //         var holdData = new DongleHoldData
            //         {
            //            IsMain = rdoServer.Checked,
            //            VersionId = activition.VersionId,
            //            Features = activition.FeatureList.Select(c => c.FeatureId).ToList()
            //         };

            //         var str = JsonConvert.SerializeObject(holdData) + "@";
            //         if(Morabaa7.MyClass.WriteData(str))
            //         {
            //            MessageBox.Show("OK");
            //            Clear();
            //         }
            //         else
            //            MessageBox.Show("Error");
            //      }


            //   }
            //   else
            //   {
            //      MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            //   }
            //}
            //catch(Exception ex)
            //{
            //   MessageBox.Show(ex.Message);
            //}

            #endregion


            try
            {
                var client = new HttpClient { BaseAddress = Pv.BaseAddress };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Pv.User.Token);

                var activeDongleFeatures = new List<ActiveDongleFeature>();
                foreach (var feature in ActivitionFeatures)
                {
                    if (feature.Value)
                        activeDongleFeatures.Add(new ActiveDongleFeature
                        {
                            FeatureId = feature.Id
                        });
                }
                var dongleKey = Morabaa7.MyClass.GetId();
                if (dongleKey == "0")
                {
                    MessageBox.Show("Error no Dongle!");
                    return;
                }
                if (checkUpgrad.Checked && !obj.State)
                {
                    MessageBox.Show("النسخة غير مرخصة لا يبمكن الاستمرار بالعملية!");
                    return;
                }
                var activition = new ActiveDongle
                {
                    CostPrice = Convert.ToDecimal(lblAmount.Text),
                    Date = DateTime.Now,
                    Deleted = false,
                    Discount = 0,
                    SalePrice = 0,
                    DongleKey = dongleKey,
                    UserId = Pv.User.Id,
                    VersionId = _version.Id,
                    OfferId = 0,
                    Id = 0,
                    FeatureList = activeDongleFeatures,
                    ApplicationId = 1,
                    CompanyAddress = txtCompanyAddress.Text,
                    CompanyJop = txtCompanyJop.Text,
                    CompanyName = txtCompanyName.Text,
                    CompanyNote = txtCompanyNote.Text,
                    CompanyPhones = txtCompanyPhones.Text,
                    CompanyGovernorate = txtCompanyGovernorate.Text,
                    IsMain = rdoServer.Checked,

                };
                if (checkUpgrad.Checked && obj.State)
                {
                    activition.IsUpgraded = true;
                    activition.OldVersionKey = obj.Key;
                    activition.OldVersionType = obj.Version;
                    if (!string.IsNullOrEmpty(_ignoreKey))
                    {
                        activition.IsIgnored = true;
                        activition.IgnoreKey = _ignoreKey;
                    }
                }

                var response = client.PostAsJsonAsync("api/Dongle/ActiveDongle", activition).Result;

                if (response.IsSuccessStatusCode)
                {
                    var dongle = response.Content.ReadAsAsync<Dongle>().Result;
                    if (dongle == null)
                        MessageBox.Show("حدث خطأ....");
                    else
                    {
                        if (dongle.Result == Dongle.DongleResult.Upgraded)
                        {
                            MessageBox.Show("كود التفعيل موجود مسبقا لا يمكن الاستمرار بالعملية!");
                            return;
                        }
                        var holdData = new DongleHoldData
                        {
                            IsMain = rdoServer.Checked,
                            VersionId = activition.VersionId,
                            Features = activition.FeatureList.Select(c => c.FeatureId).ToList()
                        };

                        var str = JsonConvert.SerializeObject(holdData) + "@";
                        if (Morabaa7.MyClass.WriteData(str))
                        {
                            MessageBox.Show("OK");
                            Clear();
                        }
                        else
                            MessageBox.Show("Error");
                    }


                }
                else
                {
                    MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

      private void btnReadData_Click(object sender,EventArgs e)
      {
         try
         {
            var str = Morabaa7.MyClass.ReadData();
            if(string.IsNullOrEmpty(str))
            {
               MessageBox.Show("Error");
               return;
            }
            var strArray = str.Split('@');
            var num1 = strArray[0];
            var holdData = JsonConvert.DeserializeObject<DongleHoldData>(num1);
            MessageBox.Show(Pv.DongleActivitionString(holdData,""));
         }
         catch(Exception exception)
         {
            MessageBox.Show(exception.Message);
         }
      }

      private void button1_Click(object sender,EventArgs e)
      {
         try
         {
            var client = new HttpClient { BaseAddress = Pv.BaseAddress };

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",Pv.User.Token);

            var dongleKey = Morabaa7.MyClass.GetId();
            if(dongleKey == "0")
            {
               MessageBox.Show("Error no Dongle!");
               return;
            }

            var response = client.GetAsync("api/Dongle/GetActiveDongle?dongleKey=" + dongleKey).Result;

            if(response.IsSuccessStatusCode)
            {
               var result = response.Content.ReadAsAsync<ReActiveDongleViewModel>().Result;
               if(result == null || result.ActiveDongle == null)
               {
                  MessageBox.Show("هذا الدنكل غير مفعل او غير موجود");
                  return;
               }

               var holdData = new DongleHoldData
               {
                  IsMain = rdoServer.Checked,
                  VersionId = result.ActiveDongle.VersionId,
                  Features = result.ActiveDongle.FeatureList.Select(c => c.FeatureId).ToList()
               };

               txtActivitionInfo.Text = Pv.DongleActivitionString(holdData, result.ActiveDongle.StrUpgraded);
               txtCompanyAddress.Text = result.ActiveDongle.CompanyAddress;
               txtCompanyJop.Text = result.ActiveDongle.CompanyJop;
               txtCompanyNote.Text = result.ActiveDongle.CompanyNote;
               txtCompanyPhones.Text = result.ActiveDongle.CompanyPhones;
               txtCompanyGovernorate.Text = result.ActiveDongle.CompanyGovernorate;
               txtCompanyName.Text = result.ActiveDongle.CompanyName;

            }
            else
            {
               MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
         }
         catch(Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }

      private void Clear()
      {

         txtCompanyAddress.Text = "";
         txtCompanyJop.Text = "";
         txtCompanyNote.Text = "";
         txtCompanyPhones.Text = "";
         txtCompanyGovernorate.Text = "";
         txtActivitionInfo.Text = "";
         txtCompanyName.Text = "";
      }

      private void btnUpdate_Click(object sender,EventArgs e)
      {
         try
         {
            var client = new HttpClient { BaseAddress = Pv.BaseAddress };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",Pv.User.Token);

            var activeDongleFeatures = new List<ActiveDongleFeature>();
            foreach(var feature in ActivitionFeatures)
            {
               if(feature.Value)
                  activeDongleFeatures.Add(new ActiveDongleFeature
                  {
                     FeatureId = feature.Id
                  });
            }
            var dongleKey = Morabaa7.MyClass.GetId();
            if(dongleKey == "0")
            {
               MessageBox.Show("Error no Dongle!");
               return;
            }
            var activition = new ActiveDongle
            {
               CostPrice = Convert.ToDecimal(lblAmount.Text),
               Date = DateTime.Now,
               Deleted = false,
               Discount = 0,
               SalePrice = 0,
               DongleKey = dongleKey,
               UserId = Pv.User.Id,
               VersionId = _version.Id,
               OfferId = 0,
               Id = 0,
               FeatureList = activeDongleFeatures,
               ApplicationId = 1,
               CompanyAddress = txtCompanyAddress.Text,
               CompanyJop = txtCompanyJop.Text,
               CompanyName = txtCompanyName.Text,
               CompanyNote = txtCompanyNote.Text,
               CompanyPhones = txtCompanyPhones.Text,
               CompanyGovernorate = txtCompanyGovernorate.Text,
               IsMain = rdoServer.Checked
            };




            var response = client.PostAsJsonAsync("api/Dongle/UpdateActiveDongle",activition).Result;

            if(response.IsSuccessStatusCode)
            {
               var dongle = response.Content.ReadAsAsync<Dongle>().Result;
               if(dongle == null)
                  MessageBox.Show("حدث خطأ....");
               else
               {
                  var holdData = new DongleHoldData
                  {
                     IsMain = rdoServer.Checked,
                     VersionId = activition.VersionId,
                     Features = activition.FeatureList.Select(c => c.FeatureId).ToList()
                  };

                  var str = JsonConvert.SerializeObject(holdData) + "@";
                  if(Morabaa7.MyClass.WriteData(str))
                  {
                     MessageBox.Show("OK");
                     Clear();
                  }
                  else
                     MessageBox.Show("Error");
               }


            }
            else
            {
               MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
         }
         catch(Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }

      private void btnRefresh2_Click(object sender,EventArgs e)
      {
         try
         {


            var dtFrom = new DateTime(this.dtFrom2.Value.Year,this.dtFrom2.Value.Month,this.dtFrom2.Value.Day);
            var dtTo = new DateTime(this.dtTo2.Value.Year,this.dtTo2.Value.Month,this.dtTo2.Value.Day,23,59,0);
            var userId = Pv.User.Id;

            var client = new HttpClient { BaseAddress = Pv.BaseAddress };

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",Pv.User.Token);



            var response = client.GetAsync("api/Dongle/GetDeletedActiveDongleForUser?userId=" + userId
                                           + "&" + "dtFrom=" + dtFrom.ToString("yyyy-MM-dd") + "&dtTo=" + dtTo.ToString("yyyy-MM-dd")).Result;

            if(response.IsSuccessStatusCode)
            {
               var result = response.Content.ReadAsAsync<List<ActiveDongle>>().Result;
               if(result == null)
               {
                  MessageBox.Show("لا توجد بيانات !");
                  return;
               }

               var data = new List<ActiveDongleViewModel>();
               foreach(var activeDongle in result)
               {
                  data.Add(new ActiveDongleViewModel
                  {
                     VersionId = activeDongle.VersionId,
                     Id = activeDongle.Id,
                     CompanyAddress = activeDongle.CompanyAddress,
                     IsMain = activeDongle.IsMain,
                     ApplicationId = activeDongle.ApplicationId,
                     CompanyGovernorate = activeDongle.CompanyGovernorate,
                     CompanyJop = activeDongle.CompanyJop,
                     CompanyName = activeDongle.CompanyName,
                     CompanyNote = activeDongle.CompanyNote,
                     CompanyPhones = activeDongle.CompanyPhones,
                     CostPrice = activeDongle.CostPrice,
                     Date = activeDongle.Date,
                     MachineDate = activeDongle.MachineDate,
                     Deleted = activeDongle.Deleted,
                     Discount = activeDongle.Discount,
                     DongleKey = activeDongle.DongleKey,
                     Explained = Pv.DongleActivitionString(new DongleHoldData
                     {
                        IsMain = activeDongle.IsMain,
                        VersionId = activeDongle.VersionId,
                        ApplicationId = activeDongle.ApplicationId,
                        Features = activeDongle.FeatureList.Select(c => c.FeatureId).ToList(),
                     }, activeDongle.StrUpgraded),
                     FeatureList = activeDongle.FeatureList,
                     OfferId = activeDongle.OfferId,
                     SalePrice = activeDongle.SalePrice,
                     UserId = activeDongle.UserId
                  });
               }
               dg2.AutoGenerateColumns = false;
               dg2.DataSource = data;

            }
            else
            {
               MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
         }
         catch(Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }

      private void btnRefresh_Click(object sender,EventArgs e)
      {
         try
         {


            var dtFrom = new DateTime(this.dtFrom.Value.Year,this.dtFrom.Value.Month,this.dtFrom.Value.Day);
            var dtTo = new DateTime(this.dtTo.Value.Year,this.dtTo.Value.Month,this.dtTo.Value.Day,23,59,0);
            var userId = Pv.User.Id;

            var client = new HttpClient { BaseAddress = Pv.BaseAddress };

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",Pv.User.Token);



            var response = client.GetAsync("api/Dongle/GetActiveDongleForUser?userId=" + userId
                                           + "&" + "dtFrom=" + dtFrom.ToString("yyyy-MM-dd") + "&dtTo=" + dtTo.ToString("yyyy-MM-dd")).Result;

            if(response.IsSuccessStatusCode)
            {
               var result = response.Content.ReadAsAsync<List<ActiveDongle>>().Result;
               if(result == null)
               {
                  MessageBox.Show("لا توجد بيانات !");
                  return;
               }

               var data = new List<ActiveDongleViewModel>();
               foreach(var activeDongle in result)
               {
                  data.Add(new ActiveDongleViewModel
                  {
                     VersionId = activeDongle.VersionId,
                     Id = activeDongle.Id,
                     CompanyAddress = activeDongle.CompanyAddress,
                     IsMain = activeDongle.IsMain,
                     ApplicationId = activeDongle.ApplicationId,
                     CompanyGovernorate = activeDongle.CompanyGovernorate,
                     CompanyJop = activeDongle.CompanyJop,
                     CompanyName = activeDongle.CompanyName,
                     CompanyNote = activeDongle.CompanyNote,
                     CompanyPhones = activeDongle.CompanyPhones,
                     CostPrice = activeDongle.CostPrice,
                     Date = activeDongle.Date,
                     MachineDate = activeDongle.MachineDate,
                     Deleted = activeDongle.Deleted,
                     Discount = activeDongle.Discount,
                     DongleKey = activeDongle.DongleKey,
                     Explained = Pv.DongleActivitionString(new DongleHoldData
                     {
                        IsMain = activeDongle.IsMain,
                        VersionId = activeDongle.VersionId,
                        ApplicationId = activeDongle.ApplicationId,
                        Features = activeDongle.FeatureList.Select(c => c.FeatureId).ToList(),
                     }, activeDongle.StrUpgraded),
                     FeatureList = activeDongle.FeatureList,
                     OfferId = activeDongle.OfferId,
                     SalePrice = activeDongle.SalePrice,
                     UserId = activeDongle.UserId
                  });
               }
               dg1.AutoGenerateColumns = false;
               dg1.DataSource = data;

            }
            else
            {
               MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
         }
         catch(Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }

      private void btnReActiveDongle_Click(object sender,EventArgs e)
      {
         try
         {
            var client = new HttpClient { BaseAddress = Pv.BaseAddress };

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",Pv.User.Token);

            var dongleKey = Morabaa7.MyClass.GetId();
            if(dongleKey == "0")
            {
               MessageBox.Show("Error no Dongle!");
               return;
            }

            var response = client.GetAsync("api/Dongle/ReactiveDongle?dongleKey=" + dongleKey).Result;

            if(response.IsSuccessStatusCode)
            {
               var result = response.Content.ReadAsAsync<ReActiveDongleViewModel>().Result;
               if(result == null || result.ActiveDongle == null)
               {
                  MessageBox.Show("هذا الدنكل غير مفعل او غير موجود");
                  return;
               }

               var holdData = new DongleHoldData
               {
                  IsMain = rdoServer.Checked,
                  VersionId = result.ActiveDongle.VersionId,
                  Features = result.ActiveDongle.FeatureList.Select(c => c.FeatureId).ToList()
               };

               var str = JsonConvert.SerializeObject(holdData) + "@";
               if(Morabaa7.MyClass.WriteData(str))
                  MessageBox.Show("OK");
               else
                  MessageBox.Show("Error");


            }
            else
            {
               MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
         }
         catch(Exception ex)
         {
            MessageBox.Show(ex.Message);
         }
      }
       private Activiation.ActiviationInfo obj = new Activiation.ActiviationInfo();
        private void btnRefrish_Click(object sender, EventArgs e)
        {
            try
            {
                obj = Activiation.GetInfo();


                if (obj.State)
                {
                    lblOldVersionState.Text = "مفعلة";
                    lblOldVersionKey.Text = obj.Key;
                    txtOldVersionDetails.Text = obj.Version;
                }
                else
                {
                    lblOldVersionState.Text = "غير مفعلة";
                    lblOldVersionKey.Text = "-----------------------";
                    txtOldVersionDetails.Text = "-----------------------";
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
       private string _ignoreKey = "";

       private void label48_DoubleClick(object sender, EventArgs e)
       {
           _ignoreKey = Prompt.ShowDialog("Enter the code", "");
       }
   }
}
