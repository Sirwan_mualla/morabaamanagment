﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DongleManagment.Forms {
    public partial class FrmViewDongleData : Form {
        public FrmViewDongleData() {
            InitializeComponent();
        }

        public FrmViewDongleData(string dongleData,string serverData) {
            InitializeComponent();
            lblDongleData.Text = dongleData;
            lblServerData.Text = serverData;
        }

    }
}
