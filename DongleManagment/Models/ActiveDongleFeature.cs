﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DongleManagment.Models
{
    public class ActiveDongleFeature
    {
        public int Id { get; set; }
        public int FeatureId { get; set; }
        public int ActiveDongleId { get; set; }
        public ActiveDongle ActiveDongle { get; set; }
        public bool Deleted { get; set; }
    }
}
