﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DongleManagment.Models
{
    public class ActiveDongleViewModel
    {

        public int Id { get; set; }
        public string DongleKey { get; set; }
        public string UserId { get; set; }
        public DateTime Date { get; set; }
        public DateTime MachineDate { get; set; }
        public bool Deleted { get; set; }
        public int OfferId { get; set; }
        public decimal CostPrice { get; set; }  //هذا الي اعتمد علية
        public decimal Discount { get; set; }
        public decimal SalePrice { get; set; }//خراع خضره
        public List<ActiveDongleFeature> FeatureList { get; set; }
        public int VersionId { get; set; }
        public int ApplicationId { get; set; }
        public bool IsMain { get; set; }
        public bool IsEdited { get; set; }
        public string CompanyName { get; set; }//اسم الشركة
        public string CompanyAddress { get; set; }//عنوان الشركة 
        public string CompanyPhones { get; set; }//رقم الهاتف الشركة 
        public string CompanyJop { get; set; }//عمل الشركة  
        public string CompanyNote { get; set; }
        public string CompanyGovernorate { get; set; }
        public string Explained { get; set; }
        public string IsDeleted { get; set; }
        public string UserName { get; set; }
        public string StrCompanyDetails { get; set; }
        
    }
}
