﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DongleManagment.Models
{
    public class Dongle
    {

        public enum DongleResult
        {
            Ok = 0,
            NotFound = 1,
            Registered = 2,
            Error = 3,
            Found = 4,
            NotActive=5,
            Upgraded = 6,//كود التفعيل الي اجه وية الدنكل مكرر
        }



        public enum DongleState
        {
            Active = 0,//شغال
            Lost = 1,//ضائع
            Damaged = 2,//تالف
            Sold = 3,//مبيوع
        }

        public int number { get; set; }
        public int Id { get; set; }
        public string DongleKey { get; set; }
        public Guid ActiveKey { get; set; }
        public DongleState State { get; set; }
        public bool Deleted { get; set; }
        public string UserId { get; set; }
        public DongleResult Result { get; set; }
    }
}
