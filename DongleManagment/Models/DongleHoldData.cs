﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DongleManagment.Models
{
    public class DongleHoldData
    {
        public bool IsMain { get; set; }
        public int VersionId { get; set; }
        public List<int> Features { get; set; }
        //public string DataBaseName { get; set; } = "MorabaaDB";
        public int ApplicationId { get; set; } = 1;
        public bool AutoDeActivate { get; set; }
        public DateTime DeActivationDate { get; set; }
        public bool Master { get; set; }
        public string DongleKey { get; set; }
    }
}
