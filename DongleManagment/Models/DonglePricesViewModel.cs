﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DongleManagment.Models
{
   public class DonglePricesViewModel
    {
        public List<Version> Versions { get; set; }
        public List<Features> Featureses { get; set; }
    }
}
