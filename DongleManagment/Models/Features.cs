﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DongleManagment.Models
{
    public class Features
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Deleted { get; set; }
        public List<FeaturesPrice> Prices { get; set; }
        public int ApplicationId { get; set; }
    }
}
