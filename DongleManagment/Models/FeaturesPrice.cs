﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DongleManagment.Models
{
   public class FeaturesPrice
    {
        public int Id { get; set; }
        public int FeaturesId { get; set; }
        public VersionPrice.VersionType Type { get; set; }
        public DateTime CreateDate { get; set; } = DateTime.Now;
        public DateTime DeleteDate { get; set; } = DateTime.Now;
        public bool Deleted { get; set; }
        public decimal Price { get; set; }
    }
}
