﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DongleManagment.Models
{
   public class ReActiveDongleViewModel
    {
        public Dongle Dongle { get; set; }
        public ActiveDongle ActiveDongle { get; set; }
    }
}
