﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DongleManagment.Models
{
    public class RegisterBindingModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public  UserViewModel.UserType UserType { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
