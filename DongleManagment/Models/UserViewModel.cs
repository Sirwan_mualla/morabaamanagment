﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DongleManagment.Models
{
    public class UserViewModel
    {
        public enum UserType
        {
            Admin = 0,
            Agent = 1,
            User = 2,
            MorabaaUser = 3,
            Accountant=4,
        }


        public string Id { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
        public string Address { get; set; }
        public UserType Type { get; set; }
        public string Phone { get; set; }
    }
}
