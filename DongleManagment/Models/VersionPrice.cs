﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DongleManagment.Models
{
   public class VersionPrice
    {
        public enum VersionType
        {
            MainVersion = 0,
            SubVersion = 1,
            AgentMainVersion = 2,
            AgentSubVersion = 3

        }

        //-------------------------------
        public int Id { get; set; }
        public int VersionId { get; set; }
        public VersionType Type { get; set; }
        public DateTime CreateDate { get; set; } = DateTime.Now;
        public DateTime DeleteDate { get; set; } = DateTime.Now;
        public bool Deleted { get; set; }
        public decimal Price { get; set; }
    }
}
