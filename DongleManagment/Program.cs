﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DongleManagment.Forms;
using DongleManagment.Models;
using Application = System.Windows.Forms.Application;

namespace DongleManagment
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
          
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run( new FrmLogin());
        }
    }
}
 