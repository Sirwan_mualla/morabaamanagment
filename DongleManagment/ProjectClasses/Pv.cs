﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DongleManagment.Models;
using Newtonsoft.Json;
using Version = DongleManagment.Models.Version;

namespace DongleManagment
{
    public static class Pv
    {
        
        public class ForgotPasswordViewModel
        {
            public string Email { get; set; }
        }

        public class ResetPasswordViewModel
        {
            public string Code { get; set; }
            public string ConfirmPassword { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
        }


        public static Uri BaseAddress = new Uri("https://dongles.morabaa.com/");
        //public static Uri BaseAddress = new Uri("http://www.morabaawork.site/");
        //public static Uri BaseAddress = new Uri("http://localhost:50401/");
        public static UserViewModel User = new UserViewModel();
        public static List<Version> Versions;
        public static List<Features> Featureses;
        public static int CurrentVersionNo = 7;

        public static string DongleActivitionString(DongleHoldData data,string upgrad)
        {
            try
            {
                var activitionString = "";

                if (data.Master) {
                    return "---------Master---------";
                }
                
                if (data.IsMain)
                    activitionString = "رئيسية ; ";
                else
                    activitionString = "فرعية ; ";
                
                
                    var temp = Versions.SingleOrDefault(c => c.Id == data.VersionId);
                if (temp != null)
                    activitionString += temp.Name ;
                if (data.AutoDeActivate)
                    activitionString += " ينتهي في " + data.DeActivationDate.ToString("yyyy/MM/dd")  ;
                activitionString += ": ";
                foreach (var id in data.Features)
                {
                    var temp2 = Featureses.SingleOrDefault(c => c.Id == id);
                    if (temp2 != null)
                        activitionString += temp2.Name + " , ";
                }
                activitionString += "// " + upgrad;

                return activitionString;


            }
            catch (Exception e)
            {
                return e.Message;
            }

        }
    }
}
