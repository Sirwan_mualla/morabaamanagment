﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace DongleManagment
{
    public class Token
    {
        public string AccessToken { get; set; }


        public async Task<string> GetToken(string username, string password)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = Pv.BaseAddress;
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, new Uri(Pv.BaseAddress, "/token")))
                {
                    try
                    {
                        request.Content = new FormUrlEncodedContent(new Dictionary<string, string>()
                        {
                            {"grant_type","password" } ,
                            {"username",username } ,
                            {"password",password }
                        });

                        HttpResponseMessage response = await httpClient.SendAsync(request);
                        if (response.IsSuccessStatusCode)
                        {
                            string result = await response.Content.ReadAsStringAsync();
                            JObject jObject = JObject.Parse(result);
                            return (string)jObject["access_token"];
                        }
                        else if (response.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            MessageBox.Show("خطأ في اسم المستخدم او كلمة المرور");
                            return null;
                        }
                        else
                        {
                            MessageBox.Show("خطأ ...............");
                            return null;
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message);
                        return null;
                    }
                    
                   
                }
            }
        }
    }
}
