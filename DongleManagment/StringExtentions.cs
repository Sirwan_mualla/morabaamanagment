﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DongleManagment {
    public static class StringExtentions {
        private static string CleanForDecimal(String str) {
            if (String.IsNullOrEmpty(str)) return "";
            int i = 0;
            while (!char.IsDigit(str[i]) && str[i] != '-') {
                str = str.Remove(i, 1);
                if (String.IsNullOrEmpty(str)) return "";
            }
            while (!char.IsDigit(str[str.Length - 1]) && str[str.Length - 1] != '-') {
                str = str.Remove(str.Length - 1);
            }
            while (i < str.Length - 1) {
                if (!char.IsDigit(str[i]) && str[i] != '-' && str[i] != '.')
                    str = str.Remove(i, 1);
                else i++;
            }
            return str;
        }
        public static decimal ToDecimal(this string str) {
            str = CleanForDecimal(str);
            if (String.IsNullOrEmpty(str)) return 0;
            decimal d = 0;
            Decimal.TryParse(str, out d);
            return d;
        }
        private static string CleanForInt(String str) {
            if (String.IsNullOrEmpty(str)) return "";
            int i = 0;
            while (!char.IsDigit(str[i]) && str[i] != '-') {
                str = str.Remove(i, 1);
                if (String.IsNullOrEmpty(str)) return "";
            }
            while (!char.IsDigit(str[str.Length - 1]) && str[str.Length - 1] != '-') {
                str = str.Remove(str.Length - 1);
            }
            while (i < str.Length - 1) {
                if (!char.IsDigit(str[i]) && str[i] != '-')
                    str = str.Remove(i, 1);
                else i++;
            }
            return str;
        }
        public static int ToInt(this String str) {
            str = CleanForInt(str);
            if (String.IsNullOrEmpty(str)) return 0;
            int t = 0;
            int.TryParse(str, out t);
            return t;
        }
        public static bool IsNotEmpty(this String str) {
            return !String.IsNullOrWhiteSpace(str);

        }
        public static bool IsEmpty(this String str) {
            return String.IsNullOrWhiteSpace(str);

        }
    }
    public static class DecimalrExtentions {
        public static string ToFormattedNumber(this decimal d, string format = "#,##0.##") {
            return d.ToString(format);
        }
        public static string ToFormattedPrice(this decimal d, string format = "#,##0.##") {
            return d.ToString(format) + " د.ع";
        }
    }
}
