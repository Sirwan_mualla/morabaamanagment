﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Management
Imports System.Net.NetworkInformation
Imports System.Windows.Forms

Module Module1
    Public application_type As Integer
    Public accaunt6 As New DataTable() 'key pc
    Public daAdabtr As New SqlDataAdapter("", "")
    Public con As New SqlConnection("")
    Public applicationType As String = ""
    Public CodetoCompare As String = "-----------------------"

    Public Function RunMeFirst() As Boolean
        getConnectionString()
        daAdabtr = New SqlDataAdapter("", con.ConnectionString)
        application_type = GetApplicationType()
        applicationType = GetApplicationTypeAsString(application_type)

        CodetoCompare = GetTheCodeMustBeFoundInTheAccountancyDataBase(GetPcId())
        Return checkIfActivated()
    End Function

    Private Sub getConnectionString()
        Dim x = "."
        If File.Exists("ServerName.txt") Then
            x = File.ReadAllText("ServerName.txt")
        End If
        Dim conx = "Data Source='" & x & "';Initial Catalog=accountancy;Persist Security Info=True;User ID=sa;Password=123@a"
        con = New SqlConnection(conx)
    End Sub


#Region "CompeletedWork"
    Public Function GetPcId() As String
        Try
            '''''''''''' bios id
            Dim query As New SelectQuery("Win32_bios")
            Dim search As New ManagementObjectSearcher(query)
            Dim info As ManagementObject
            Dim s As String = ""
            For Each info In search.Get()
                s = info("serialnumber").ToString().Trim
            Next
            If s.Contains(" ") Then
                s = GetMACAddress()
            End If
            Dim s2 As String = s
            s2.Replace(" ", "9")
            If s2.Length <= 0 Then s2 = "9479461059441870286"
            Return s2
        Catch ex As Exception
            Return "9479461059441870286"
        End Try
    End Function
    Private Function GetMACAddress() As String
        Dim nics As NetworkInterface() = NetworkInterface.GetAllNetworkInterfaces()
        Dim sMacAddress As [String] = String.Empty
        For Each adapter As NetworkInterface In nics
            If sMacAddress = [String].Empty Then
                ' only return MAC Address from first card  
                Dim properties As IPInterfaceProperties = adapter.GetIPProperties()
                sMacAddress = adapter.GetPhysicalAddress().ToString()
            End If
        Next
        Return sMacAddress
    End Function
    Public Function GetApplicationType() As Integer
        Dim x As Integer
        Try
            Dim mycon As New SqlConnection(con.ConnectionString)
            Dim mycmd As New SqlCommand("", mycon)

            If mycon.State = ConnectionState.Closed Then mycon.Open()
            mycmd.CommandText = "select t9 from satting  where branch_id =1 and set_id=1"
            mycmd.Parameters.Clear()
            If Not IsDBNull(mycmd.ExecuteScalar) Then
                x = mycmd.ExecuteScalar()
            Else
                x = 0
            End If
            mycon.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return (x)
        application_type = 1
    End Function
    Function Bin2Dec(ByVal ans As String) As Double
        Dim dig As String, p As Integer
        Dim dec, B, d As Double
        p = 0
        For x As Integer = ans.Length - 1 To 0 Step -1
            dig = ans.Substring(x, 1)
            If Not (dig = "0" Or dig = "1") Then
                dec = 0
                '  MessageBox.Show("Incorrect entry.  ")
                Exit For
            End If
            Double.TryParse(dig, B)
            d = B * (2 ^ p)
            dec = dec + d
            p = p + 1
        Next x
        Return dec
    End Function
#End Region

    Public Function GetTheCodeMustBeFoundInTheAccountancyDataBase(ByVal PCId As String) As String
        Dim hash(14) As String
        hash(0) = "NATURLIFEHUMANANIMAL"
        hash(1) = "ACCOUNTANCYEXCHANGE"
        hash(2) = "SARDARPHARMACYSALES"
        hash(3) = "SUPERMARKETWITHBARCODE"
        hash(4) = "CERAMICANDGRANYTE"
        hash(5) = "FILLINGCONSTRUCTION"
        hash(6) = "MZKHRANDSCIENCEOFFICE"
        hash(7) = "RESTURANTANDCOFFESHOP"
        hash(8) = "FACTORYWITHMAKE"
        hash(9) = "CONTRACTORENTREPRENUR"
        hash(10) = "GENERALAPPNUMBERONE"
        hash(11) = "MYAPPLICATIONSECOND"
        hash(12) = "OUTSYSTEMTHIRDEDITION"
        hash(13) = "FORTHEDITIONFORMYAPP"
        hash(14) = "FIFTHGENERATIONAPPLICATION"
        For myindex = 0 To 14
            Dim s As String = PCId '.Substring(0, 6)
            hash(myindex) += hash(myindex)
            If hash(myindex).Length > 20 Then hash(myindex) = hash(myindex).Substring(0, 20)
            If s.Length < 20 Then
                Dim add As String = hash(myindex).Substring(s.Length - 1, 20 - s.Length)
                s += add
            End If
            If s.Length > 20 Then s = s.Substring(0, 20)
            Dim ss As String = ""
            Dim sindex As Integer = 0
            For Each a In s
                Dim t As Integer = Asc(a)
                If t >= 65 And t <= 90 Then
                    t += 13
                    While t > 90
                        t -= 7
                    End While
                ElseIf t >= 48 And t <= 57 Then
                    t += 7
                    While t > 57
                        t -= 3
                    End While
                End If
                ss = ss.Insert(sindex, Chr(t))
                sindex += 1
            Next
            Dim strapptype As String = (myindex + 1).ToString
            If strapptype.Length = 1 Then strapptype = strapptype.Insert(0, "0")
            ss = ss.Remove(0, 2)
            ss = ss.Insert(0, strapptype)
            s = ss
            Dim biosasciiarray(s.Length - 1) As Integer
            Dim hashasciiarray(s.Length - 1) As Integer
            Dim biosbinarystring(s.Length - 1) As String
            Dim hashbinarystring(s.Length - 1) As String
            Dim biosbitarray As New BitArray((s.Length - 1) * 7)
            Dim hashbitarray As New BitArray((s.Length - 1) * 7)
            Dim finalarray As New BitArray((s.Length - 1) * 7)

            Dim hash2array As New BitArray(7)
            Dim h2 As String = "01000111"
            For i = 0 To 6
                hash2array(i) = (h2.Chars(i) = "1"c)
            Next
            For i = 0 To s.Length - 1
                biosasciiarray(i) = Asc(s.Chars(i)) + 5
                hashasciiarray(i) = Asc(hash(myindex).Chars(i)) + 5
                biosbinarystring(i) = Convert.ToString(biosasciiarray(i), 2)
                hashbinarystring(i) = Convert.ToString(hashasciiarray(i), 2)
                If biosbinarystring(i).Length = 6 Then biosbinarystring(i) = "0" & biosbinarystring(i)
                If hashbinarystring(i).Length = 6 Then hashbinarystring(i) = "0" & hashbinarystring(i)
            Next
            Dim k As Integer = 0
            For i = 0 To s.Length - 2
                For j = 0 To 6
                    biosbitarray(k) = (biosbinarystring(i).Chars(j) = "1"c) Xor hash2array(j)
                    hashbitarray(k) = (hashbinarystring(i).Chars(j) = "1"c)
                    k += 1
                Next
            Next

            For i = 0 To (s.Length - 2) * 7
                finalarray(i) = biosbitarray(i) Xor hashbitarray(i)
            Next
            Dim finalcode As String = ""
            k = 0
            For i = 0 To s.Length - 2
                Dim temp As String = ""
                For j = 0 To 6
                    If finalarray(k) Then
                        temp = temp.Insert(j, "1")
                    Else
                        temp = temp.Insert(j, "0")
                    End If
                    k += 1
                Next
                finalcode += Bin2Dec(temp).ToString
            Next
            Dim str1 As String = Convert.ToString(Int64.Parse(finalcode.Substring(0, 8)) Mod 9991)
            Dim str2 As String = Convert.ToString(Int64.Parse(finalcode.Substring(8, 8)) Mod 9821)
            Dim str3 As String = Convert.ToString(Int64.Parse(finalcode.Substring(16, 8)) Mod 9456)
            Dim str4 As String = Convert.ToString(Int64.Parse(finalcode.Substring(24, 8)) Mod 9538)
            Dim str5 As String = Convert.ToString(Int64.Parse(finalcode.Substring(32, finalcode.Length - 33)) Mod 9538)
            Dim myprime As Integer = 233
            While str1.Length < 4
                str1 = Val(str1) + myprime
                myprime += myprime
            End While
            While str2.Length < 4
                str2 = Val(str2) + myprime
                myprime += myprime
            End While
            While str3.Length < 4
                str3 = Val(str3) + myprime
                myprime += myprime
            End While
            While str4.Length < 4
                str4 = Val(str4) + myprime
                myprime += myprime
            End While
            While str5.Length < 4
                str5 = Val(str5) + myprime
                myprime += myprime
            End While
            Dim strfinal As String = str1 & str2 & str3 & str4 & str5

            If application_type = (myindex + 1) Then Return strfinal

        Next

    End Function
    Public Function DeleteActivation() As Boolean
        Dim cmd As New SqlCommand("delete from Productactivation", con)
        Try
            If con.State = ConnectionState.Closed Then con.Open()
            cmd.ExecuteNonQuery()
            MessageBox.Show("يرجى تحديد مسار الاصدار السادس")
            Dim o As New FolderBrowserDialog()
            If o.ShowDialog() = DialogResult.OK Then
                Dim filepath = o.SelectedPath & "\ActivationCode.txt"
                If (File.Exists(filepath)) Then
                    File.Delete(filepath)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("حصل خطأ اثناء محاولة الغاء تفعيل الاصدار السادس يرجى القيام بالعملية يدوياً")
            con.Close()
            Return False
        End Try
        con.Close()
        Return True

    End Function

    Public Sub FillActivationCodesFromAccountancyDataBase()
        accaunt6.Clear()
        daAdabtr.SelectCommand.CommandText = "select KeyPC FROM Productactivation"
        daAdabtr.Fill(accaunt6)

    End Sub

    Public Function checkIfActivated() As Boolean
        FillActivationCodesFromAccountancyDataBase()
        For i = 0 To accaunt6.Rows.Count - 1
            If accaunt6.Rows(i).Item("keyPc").ToString() = CodetoCompare Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Function GetApplicationTypeAsString(ByVal x As Integer) As String
        Select Case x
            Case 1
                Return "مبيعات"
            Case 2
                Return "صيرفة"
            Case 3
                Return "صيدلية"
            Case 4
                Return "سوبر ماركت"
            Case 5
                Return "سيراميك"
            Case 6
                Return "تعئبة"
            Case 7
                Return "مذخر"
            Case 8
                Return "مطعم"
            Case Else
                Return "يوجد خطا"
        End Select
    End Function

End Module
